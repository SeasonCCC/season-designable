import { createBehavior } from '@season-designable/core';
import { RootSchema } from './RootSchema';
import { RootLocales } from './RootLocales';
import { CSSStyle } from '../../schemas/CSSStyle';

export const RootBehavior = createBehavior({
  name: 'Root',
  selector: (node) => node.componentName === 'Root',
  designerProps(node) {
    return {
      draggable: !node.isRoot,
      cloneable: !node.isRoot,
      deletable: !node.isRoot,
      droppable: true,
      propsSchema: {
        type: 'object',
        properties: {
          ...RootSchema.properties,
          style: CSSStyle,
        },
      },
      defaultProps: {
        labelCol: 6,
        wrapperCol: 12,
        feedbackLayout: 'loose',
        size: 'default',
        layout: 'horizontal',
        tooltipLayout: 'icon',
        labelAlign: 'right',
        wrapperAlign: 'left',
        shallow: true,
        bordered: true,
      },
    };
  },
  designerLocales: RootLocales,
});
