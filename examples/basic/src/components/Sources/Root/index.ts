import { RootBehavior } from './behavior/behavior';
import RootComponent from './Root.vue';

export const Root = {
  Component: RootComponent,
  Behavior: RootBehavior,
};
