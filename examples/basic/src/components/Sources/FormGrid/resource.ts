import { createResource } from '@season-designable/core';
import { h } from 'vue';
import { Icon } from '@iconify/vue';

export const FormGridResource = createResource({
  icon: h(Icon, { icon: 'mdi:grid', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'FormGrid',
        type: 'void',
        'x-component': 'FormGrid',
      },
      children: [
        {
          componentName: 'Field',
          props: {
            type: 'void',
            'x-component': 'FormGridColumn',
          },
        },
        {
          componentName: 'Field',
          props: {
            type: 'void',
            'x-component': 'FormGridColumn',
          },
        },
        {
          componentName: 'Field',
          props: {
            type: 'void',
            'x-component': 'FormGridColumn',
          },
        },
      ],
    },
  ],
});
