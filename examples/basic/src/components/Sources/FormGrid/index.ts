import FormGridComponent from './FormGrid.vue';
// import { FormGrid as FormGridComponent } from '@formily/element-plus';
import FormGridColumnComponent from './FormGridColumn.vue';

import { FormGridResource } from './resource';
import { FormGridBehavior, FormGridColumnBehavior } from './behavior/behavior';

export const FormGrid = {
  Component: FormGridComponent,
  Resource: FormGridResource,
  Behavior: FormGridBehavior,
};

export const FormGridColumn = {
  Component: FormGridColumnComponent,
  Behavior: FormGridColumnBehavior,
};
