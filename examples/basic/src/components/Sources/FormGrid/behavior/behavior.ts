import { createBehavior } from '@season-designable/core';
import { createFieldSchema } from '../../Field/createComponentSchema';
import { FormGridLocales, FormGridColumnLocales } from './Locales';
import { FormGridSchema, FormGridColumnSchema } from './Schema';

export const FormGridBehavior = createBehavior({
  name: 'FormGrid',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'FormGrid',
  designerProps: {
    droppable: true,
    allowDrop: (node) => node.props.value?.['x-component'] !== 'FormGrid',
    propsSchema: createFieldSchema(FormGridSchema),
    defaultProps: {
      'x-component-props': {
        // minColumns: 3,
        // maxColumns: 6,
        // columnGap: 8,
      },
    },
  },
  designerLocales: FormGridLocales,
});

export const FormGridColumnBehavior = createBehavior({
  name: 'FormGridColumn',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'FormGridColumn',
  designerProps: {
    droppable: true,
    allowDrop: (node) => node.props.value?.['x-component'] !== 'FormGrid',
    propsSchema: createFieldSchema(FormGridColumnSchema),
  },
  designerLocales: FormGridColumnLocales,
});
