import { createBehavior } from '@season-designable/core';
import { createFieldSchema } from '../../Field/createComponentSchema';
import { InputNumberLocales } from './InputNumberLocales';
import { InputNumberSchema } from './InputNumberSchema';

export const InputNumberBehavior = createBehavior({
  name: 'InputNumber',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'InputNumber',
  designerProps: {
    propsSchema: createFieldSchema(InputNumberSchema),
  },
  designerLocales: InputNumberLocales,
});
