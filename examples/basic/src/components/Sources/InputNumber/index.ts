import { InputNumber as InputNumberComponent } from '@formily/element-plus';
import { InputNumberResource } from './resource';
import { InputNumberBehavior } from './behavior/behavior';

export const InputNumber = {
  Component: InputNumberComponent,
  Resource: InputNumberResource,
  Behavior: InputNumberBehavior,
};
