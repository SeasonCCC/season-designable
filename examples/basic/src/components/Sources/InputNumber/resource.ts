import { createResource } from '@season-designable/core';
import { h } from 'vue';
import { Icon } from '@iconify/vue';

export const InputNumberResource = createResource({
  icon: h(Icon, { icon: 'mdi:counter', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'InputNumber',
        type: 'string',
        'x-decorator': 'FormItem',
        'x-component': 'InputNumber',
      },
    },
  ],
});
