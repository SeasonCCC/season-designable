import { createResource } from '@season-designable/core';
import { h } from 'vue';
import { Icon } from '@iconify/vue';

export const CardResource = createResource({
  icon: h(Icon, { icon: 'mdi:card-outline', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'Card',
        type: 'string',
        'x-component': 'Card',
      },
    },
  ],
});
