export const CardLocales = {
  'zh-CN': {
    title: '卡片',
    settings: {
      'x-component-props': {
        type: '类型',
        title: '标题',
        extra: '右侧扩展',
        shadow: '阴影',
        cardTypes: [
          { label: '内置', value: 'inner' },
          { label: '默认', value: '' },
        ],
      },
    },
  },
  'en-US': {
    title: 'Card',
    settings: {
      'x-component-props': {
        type: 'Type',
        title: 'Title',
        extra: 'Extra',
        shadow: 'Shadow',
        cardTypes: [
          { label: 'Inner', value: 'inner' },
          { label: 'Default', value: '' },
        ],
      },
    },
  },
};
