export const CardSchema = {
  type: 'object',
  properties: {
    title: {
      type: 'string',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
    },
    extra: {
      type: 'string',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
    },
    shadow: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      enum: ['always', 'never', 'hover'],
      default: 'always',
    },
  },
};
