import { createBehavior } from '@season-designable/core';
import { createVoidFieldSchema } from '../../Field/createComponentSchema';
import { CardLocales } from './CardLocales';
import { CardSchema } from './CardSchema';

export const CardBehavior = createBehavior({
  name: 'Card',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'Card',
  designerProps: {
    propsSchema: createVoidFieldSchema(CardSchema),
  },
  designerLocales: CardLocales,
});
