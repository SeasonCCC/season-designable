import CardComponent from './Card.vue';
import { CardResource } from './resource';
import { CardBehavior } from './behavior/behavior';

export const Card = {
  Component: CardComponent,
  Resource: CardResource,
  Behavior: CardBehavior,
};
