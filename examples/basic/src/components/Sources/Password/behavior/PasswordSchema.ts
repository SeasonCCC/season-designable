import { InputSchema } from '../../Input/behavior/InputSchema';

export const PasswordSchema = {
  type: 'object',
  properties: {
    ...InputSchema.properties,
  },
};
