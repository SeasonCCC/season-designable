import { createBehavior } from '@season-designable/core';
import { createFieldSchema } from '../../Field/createComponentSchema';
import { PasswordLocales } from './PasswordLocales';
import { PasswordSchema } from './PasswordSchema';

export const PasswordBehavior = createBehavior({
  name: 'Password',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'Password',
  designerProps: {
    propsSchema: createFieldSchema(PasswordSchema),
  },
  designerLocales: PasswordLocales,
});
