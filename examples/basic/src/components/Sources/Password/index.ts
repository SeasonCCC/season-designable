import { Password as PasswordComponent } from '@formily/element-plus';
import { PasswordResource } from './resource';
import { PasswordBehavior } from './behavior/behavior';

export const Password = {
  Component: PasswordComponent,
  Resource: PasswordResource,
  Behavior: PasswordBehavior,
};
