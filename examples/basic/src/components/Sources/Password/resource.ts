import { createResource } from '@season-designable/core';
import { h } from 'vue';
import { Icon } from '@iconify/vue';

export const PasswordResource = createResource({
  icon: h(Icon, { icon: 'mdi:form-textbox-password', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'Password',
        type: 'string',
        'x-decorator': 'FormItem',
        'x-component': 'Password',
      },
    },
  ],
});
