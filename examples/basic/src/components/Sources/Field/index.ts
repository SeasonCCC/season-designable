import FieldComponent from './Field.vue';
import { FieldBehavior } from './behavior/behavior';

export const Field = {
  Component: FieldComponent,
  Behavior: FieldBehavior,
};
