import { createBehavior } from '@season-designable/core';
import { FieldLocales } from './FieldLocales';

export const FieldBehavior = createBehavior({
  name: 'Field',
  selector: 'Field',
  designerLocales: FieldLocales,
});
