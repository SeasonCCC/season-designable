import { ISchema } from '@formily/vue';
import { CSSStyle as CSSStyleSchema } from '../schemas/CSSStyle';
import { FormItem as FormItemSchema } from '../schemas/FormItem';

export const createComponentSchema = (
  component: ISchema,
  decorator: ISchema
): any => {
  return {
    'component-group': component && {
      type: 'void',
      'x-component': 'CollapseItem',
      'x-reactions': {
        fulfill: {
          state: {
            visible: '{{!!$form.values["x-component"]}}',
          },
        },
      },
      properties: {
        'x-component-props': component,
      },
    },
    'decorator-group': decorator && {
      type: 'void',
      'x-component': 'CollapseItem',
      'x-component-props': { defaultExpand: false },
      'x-reactions': {
        fulfill: {
          state: {
            visible: '{{!!$form.values["x-decorator"]}}',
          },
        },
      },
      properties: {
        'x-decorator-props': decorator,
      },
    },
    'component-style-group': {
      type: 'void',
      'x-component': 'CollapseItem',
      'x-component-props': { defaultExpand: false },
      'x-reactions': {
        fulfill: {
          state: {
            visible: '{{!!$form.values["x-component"]}}',
          },
        },
      },
      properties: {
        'x-component-props.style': CSSStyleSchema,
      },
    },
    'decorator-style-group': {
      type: 'void',
      'x-component': 'CollapseItem',
      'x-component-props': { defaultExpand: false },
      'x-reactions': {
        fulfill: {
          state: {
            visible: '{{!!$form.values["x-decorator"]}}',
          },
        },
      },
      properties: {
        'x-decorator-props.style': CSSStyleSchema,
      },
    },
  };
};

export const createFieldSchema = (
  component: ISchema,
  defaultValueSchema: ISchema = {
    'x-decorator': 'FormItem',
    'x-component': 'Input',
  },
  decorator: ISchema = FormItemSchema
): ISchema => {
  return {
    type: 'object',
    properties: {
      'field-group': {
        type: 'void',
        'x-component': 'CollapseItem',
        properties: {
          name: {
            type: 'string',
            'x-decorator': 'FormItem',
            'x-component': 'Input',
            'x-pattern': 'disabled',
          },
          title: {
            type: 'string',
            'x-decorator': 'FormItem',
            'x-component': 'Input',
          },
          description: {
            type: 'string',
            'x-decorator': 'FormItem',
            'x-component': 'Input.TextArea',
          },
          'x-display': {
            type: 'string',
            enum: ['visible', 'hidden', 'none', ''],
            'x-decorator': 'FormItem',
            'x-component': 'Select',
            default: 'visible',
            'x-component-props': {
              teleported: false,
            },
          },
          'x-pattern': {
            type: 'string',
            enum: ['editable', 'disabled', 'readOnly', 'readPretty', ''],
            'x-decorator': 'FormItem',
            'x-component': 'Select',
            default: 'editable',
            'x-component-props': {
              teleported: false,
            },
          },
          default: defaultValueSchema,
          // enum: {
          //   'x-decorator': 'FormItem',
          //   'x-component': DataSourceSetter,
          // },
          // 'x-reactions': {
          //   'x-decorator': 'FormItem',
          //   'x-component': ReactionsSetter,
          // },
          // 'x-validator': {
          //   type: 'array',
          //   'x-component': ValidatorSetter,
          // },
          required: {
            type: 'boolean',
            'x-decorator': 'FormItem',
            'x-component': 'Switch',
          },
        },
      },
      ...createComponentSchema(component, decorator),
    },
  };
};

export const createVoidFieldSchema = (
  component: ISchema,
  decorator: ISchema = FormItemSchema
): ISchema => {
  return {
    type: 'object',
    properties: {
      'field-group': {
        type: 'void',
        'x-component': 'CollapseItem',
        properties: {
          name: {
            type: 'string',
            'x-decorator': 'FormItem',
            'x-component': 'Input',
            'x-pattern': 'disabled',
          },
          'x-display': {
            type: 'string',
            enum: ['visible', 'hidden', 'none', ''],
            'x-decorator': 'FormItem',
            'x-component': 'Select',
            default: 'visible',
            'x-component-props': {
              teleported: false,
            },
          },
          'x-pattern': {
            type: 'string',
            enum: ['editable', 'disabled', 'readOnly', 'readPretty', ''],
            'x-decorator': 'FormItem',
            'x-component': 'Select',
            default: 'editable',
            'x-component-props': {
              teleported: false,
            },
          },
        },
      },
      ...createComponentSchema(component, decorator),
    },
  };
};
