import { createResource } from '@season-designable/core';
import { h } from 'vue';
import { Icon } from '@iconify/vue';

export const InputResource = createResource({
  icon: h(Icon, { icon: 'mdi:form-textbox', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'Input',
        type: 'string',
        'x-decorator': 'FormItem',
        'x-component': 'Input',
      },
    },
  ],
});

export const TextAreaResource = createResource({
  icon: h(Icon, { icon: 'mdi:form-textarea', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'TextArea',
        type: 'string',
        'x-decorator': 'FormItem',
        'x-component': 'TextArea',
      },
    },
  ],
});
