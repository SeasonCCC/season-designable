import { Input as InputComponent } from '@formily/element-plus';
import { InputResource, TextAreaResource } from './resource';
import { InputBehavior, TextAreaBehavior } from './behavior/behavior';

export const Input = {
  Component: InputComponent,
  Resource: InputResource,
  Behavior: InputBehavior,
};

export const TextArea = {
  Component: InputComponent.TextArea,
  Resource: TextAreaResource,
  Behavior: TextAreaBehavior,
};
