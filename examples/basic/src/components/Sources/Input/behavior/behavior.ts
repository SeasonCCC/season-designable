import { createBehavior } from '@season-designable/core';
import { createFieldSchema } from '../../Field/createComponentSchema';
import { InputLocales, TextAreaLocales } from './InputLocales';
import { InputSchema, TextAreaSchema } from './InputSchema';

export const InputBehavior = createBehavior({
  name: 'Input',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'Input',
  designerProps: {
    propsSchema: createFieldSchema(InputSchema),
  },
  designerLocales: InputLocales,
});

export const TextAreaBehavior = createBehavior({
  name: 'Input.TextArea',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'TextArea',
  designerProps: {
    propsSchema: createFieldSchema(TextAreaSchema),
  },
  designerLocales: TextAreaLocales,
});
