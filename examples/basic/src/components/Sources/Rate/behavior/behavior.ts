import { createBehavior } from '@season-designable/core';
import { createFieldSchema } from '../../Field/createComponentSchema';
import { RateLocales } from './RateLocales';
import { RateSchema, RateValueSchema } from './RateSchema';

export const RateBehavior = createBehavior({
  name: 'Rate',
  extends: ['Field'],
  selector: (node) => node.props.value?.['x-component'] === 'Rate',
  designerProps: {
    propsSchema: createFieldSchema(RateSchema, RateValueSchema),
  },
  designerLocales: RateLocales,
});
