export const RateLocales = {
  'zh-CN': {
    title: '评分器',
    settings: {
      'x-component-props': {
        max: '最大分值',
        allowHalf: '允许半选',
        lowThreshold: '低中界限值',
        highThreshold: '中高界限值',
        tooltips: { title: '提示信息', tooltip: '格式：string[]' },
        showScore: '展示分数',
        scoreTemplate: '分数模板',
        voidIcon: '未被选中图标',
      },
    },
  },
  'en-US': {
    title: 'Rate',
    settings: {
      'x-component-props': {
        max: 'Max',
        allowHalf: 'Allow Half',
        lowThreshold: 'Low Threshold',
        highThreshold: 'High Threshold',
        tooltips: { title: 'Tooltips', tooltip: 'Format：string[]' },
        showScore: 'Show Score',
        scoreTemplate: 'Score Template',
        voidIcon: 'Void Icon'
      },
    },
  },
};
