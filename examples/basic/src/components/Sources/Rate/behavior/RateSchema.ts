import { Star } from '@element-plus/icons-vue';
import { Icon } from '@iconify/vue';

const Component = defineComponent({
  name: 'mdi:menu',
  setup() {
    return () =>
      h(Icon, {
        icon: 'mdi:menu',
      });
  },
});

export const RateSchema = {
  type: 'object',
  properties: {
    clearable: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    max: {
      type: 'number',
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
      default: 5,
    },
    size: {
      type: 'string',
      enum: ['large', 'small', 'default'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'default',
    },
    allowHalf: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    lowThreshold: {
      type: 'number',
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    highThreshold: {
      type: 'number',
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    showScore: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    scoreTemplate: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
      default: '{value}',
    },
    voidIcon: {
      type: 'object',
      'x-component': 'IconSetter',
      default: Star,
    },
  },
};

export const RateValueSchema = {
  'x-decorator': 'FormItem',
  'x-component': 'InputNumber',
};
