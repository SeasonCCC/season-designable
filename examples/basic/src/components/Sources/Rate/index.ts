import { RateResource } from './resource';
import { RateBehavior } from './behavior/behavior';
import { Rate as RateComponent } from '../../SettingsForm/components/Rate';

export const Rate = {
  Component: RateComponent,
  Resource: RateResource,
  Behavior: RateBehavior,
};
