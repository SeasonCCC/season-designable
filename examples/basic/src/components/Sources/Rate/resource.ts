import { createResource } from '@season-designable/core';
import { h } from 'vue';
import { Icon } from '@iconify/vue';

export const RateResource = createResource({
  icon: h(Icon, { icon: 'mdi:star', width: 30 }),
  elements: [
    {
      componentName: 'Field',
      props: {
        title: 'Rate',
        type: 'string',
        'x-decorator': 'FormItem',
        'x-component': 'Rate',
      },
    },
  ],
});
