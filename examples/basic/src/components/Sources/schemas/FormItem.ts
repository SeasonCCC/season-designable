import { ISchema } from '@formily/vue';

export const FormItem: ISchema = {
  type: 'object',
  properties: {
    tooltip: {
      type: 'string',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
    },
    addonBefore: {
      type: 'string',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
    },
    addonAfter: {
      type: 'string',
      'x-decorator': 'FormItem',
      'x-component': 'Input',
    },
    labelCol: {
      type: 'number',
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    wrapperCol: {
      type: 'number',
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    labelWidth: {
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    wrapperWidth: {
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    colon: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
      default: true,
      'x-component-props': {
        teleported: false,
      },
    },
    asterisk: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    gridSpan: {
      type: 'number',
      'x-decorator': 'FormItem',
      'x-component': 'InputNumber',
    },
    feedbackLayout: {
      type: 'string',
      enum: ['loose', 'terse', 'popover', 'none'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'loose',
      'x-component-props': {
        teleported: false,
      },
    },
    size: {
      type: 'string',
      enum: ['large', 'small', 'default'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'default',
      'x-component-props': {
        teleported: false,
      },
    },
    layout: {
      type: 'string',
      enum: ['vertical', 'horizontal', 'inline'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'horizontal',
      'x-component-props': {
        teleported: false,
      },
    },
    tooltipLayout: {
      type: 'string',
      enum: ['icon', 'text'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'icon',
      'x-component-props': {
        teleported: false,
      },
    },
    labelAlign: {
      type: 'string',
      enum: ['left', 'right'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'right',
      'x-component-props': {
        teleported: false,
      },
    },
    wrapperAlign: {
      type: 'string',
      enum: ['left', 'right'],
      'x-decorator': 'FormItem',
      'x-component': 'Select',
      default: 'left',
      'x-component-props': {
        teleported: false,
      },
    },
    labelWrap: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    wrapperWrap: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    fullness: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    inset: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
    bordered: {
      type: 'boolean',
      'x-decorator': 'FormItem',
      'x-component': 'Switch',
    },
  },
};
