import { transformComponent } from '@formily/element-plus/lib/__builtins__/shared';
import { connect, mapProps } from '@formily/vue';
import { ElRate } from 'element-plus';

export type RateProps = typeof ElRate;

const TransformRate = transformComponent<RateProps>(ElRate, {
  change: 'update:modelValue',
});

export const Rate = connect(
  TransformRate,
  mapProps({
    value: 'modelValue',
    readOnly: 'readonly',
  })
);

export default Rate;
