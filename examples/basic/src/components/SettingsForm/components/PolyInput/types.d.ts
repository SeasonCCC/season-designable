export interface IInput {
  style?: CSSProperties;
  className?: string;
  value: any;
  onChange: (value: any) => void;
  exclude?: string[];
  include?: string[];
}

export interface PolyType {
  type: string;
  title?: string;
  icon?: string;
  component?: any;
  checker: (value: any) => boolean;
  toInputValue?: (value: any) => any;
  toChangeValue?: (value: any) => any;
}

export type PolyTypes = PolyType[];
