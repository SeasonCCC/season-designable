export const getIconList = (iconDataList: string[], icon?: string) => {
  const list = getRandomArrayElements(iconDataList, 20).map((iconData) => ({
    label: iconData,
    value: iconData,
  }));

  if (icon && !list.find((item) => item.label === icon)) {
    list.unshift({
      label: icon,
      value: icon,
    });
  }

  return list;
};

export const getRandomArrayElements = (arr: any[], count: number) => {
  const newArr = [];
  for (var i = 0; i < count; i++) {
    const index = Math.floor(Math.random() * arr.length);
    const item = arr[index];
    newArr.push(item);
    arr.splice(index, 1);
  }
  return newArr;
};
