import { transformComponent } from '@formily/element-plus/lib/__builtins__/shared';
import { connect, mapProps } from '@formily/vue';
import { ElSlider } from 'element-plus';

export type SliderProps = typeof ElSlider;

const TransformSlider = transformComponent<SliderProps>(ElSlider, {
  change: 'update:modelValue',
});

export const Slider = connect(
  TransformSlider,
  mapProps({
    value: 'modelValue',
    readOnly: 'readonly',
  })
  // mapReadPretty(PreviewText.Input)
);

export default Slider;
