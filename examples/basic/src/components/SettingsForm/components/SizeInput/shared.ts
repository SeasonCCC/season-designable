import UnitType from './UnitType.vue';

export const takeNumber = (value: any) => {
  const num = String(value)
    .trim()
    .replace(/[^\d\.]+/, '');
  if (num === '') return;
  return Number(num);
};

export const createUnitType = (type: string) => ({
  type,
  component: UnitType,
  checker(value: any) {
    return String(value).includes(type);
  },
  toInputValue(value: any) {
    return takeNumber(value);
  },
  toChangeValue(value: any) {
    return `${value || 0}${type}`;
  },
});

export const createSpecialSizeOption = (type: string) => ({
  type,
  checker(value: any) {
    if (value === type) return true;
    return false;
  },
  toChangeValue() {
    return type;
  },
});

export const createSimpleInputNumber = (title: string) => ({
  title,
  component: UnitType,
  toInputValue(value: any) {
    return takeNumber(value);
  },
  toChangeValue(value: any) {
    return value;
  },
});
