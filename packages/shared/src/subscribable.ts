import { Subject } from 'rxjs';
import { CustomEvent } from './event';

// const UNSUBSCRIBE_ID_SYMBOL = Symbol('UNSUBSCRIBE_ID_SYMBOL');

export type Subscriber<Payload = any> = {
  (payload: Payload): void;
};

// export type SubscribeResp = {
//   unsubscribe: () => void;
//   [key: symbol]: number;
// };

export class Subscribable<E = CustomEvent> {
  subscribers = new Map<string, Subject<any>>();

  dispatch(event: E extends CustomEvent ? CustomEvent : any, context?: any) {
    const subject = this.subscribers.get(event.constructor.name);
    if (!subject) {
      return false;
    }

    event['context'] = context;

    subject.next(event);
    return true;
    // let interrupted = false;

    // this.subscribers.forEach((subscriber) => {
    //   if (isFunction(subscriber)) {
    //     if (subscriber(event) === false) {
    //       interrupted = true;
    //     }
    //   }
    // });

    // return !interrupted;
  }

  subscribe(type: string, subscriber: Subscriber) {
    let subject = this.subscribers.get(type);
    if (!subject) {
      subject = new Subject();
      this.subscribers.set(type, subject);
    }

    return { [type]: subject.subscribe(subscriber) };

    // let id: number = this.subscribers.size + 1;
    // if (isFunction(subscriber)) {
    //   this.subscribers.set(id, subscriber);
    // }
    // return {
    //   unsubscribe: () => {
    //     this.unsubscribe(id);
    //   },
    //   [UNSUBSCRIBE_ID_SYMBOL]: id,
    // };
  }

  // unsubscribe(id?: number | string | SubscribeResp) {
  // if (isNull(id) || isUndefined(id)) {
  //   this.subscribers.forEach((value, key) => {
  //     this.unsubscribe(key);
  //   });
  //   return;
  // }
  // if (isNumber(id)) {
  //   this.subscribers.delete(id);
  // } else if (isString(id)) {
  //   this.subscribers.delete(parseInt(id, 10));
  // } else {
  //   this.subscribers.delete(id[UNSUBSCRIBE_ID_SYMBOL]);
  // }
  // }
}
