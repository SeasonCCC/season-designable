export const getGlobalThis = (): any => {
  return typeof globalThis !== 'undefined'
    ? globalThis
    : typeof self !== 'undefined'
    ? self
    : typeof window !== 'undefined'
    ? window
    : typeof global !== 'undefined'
    ? global
    : {};
};

export const globalThisPolyfill: Window = getGlobalThis();

export const getUuid = () => {
  const blobUrl = URL.createObjectURL(new Blob());
  URL.revokeObjectURL(blobUrl);
  return blobUrl.slice(blobUrl.lastIndexOf('/') + 1);
};

const isType =
  <T>(type: string | string[]) =>
  (obj: unknown): obj is T =>
    obj != null &&
    (Array.isArray(type) ? type : [type]).some(
      (t) => getType(obj) === `[object ${t}]`
    );

export const getType = (obj: any) =>
  Object.prototype.toString.call(obj);

export const isWindow = isType<Window>('Window');

export const isPlainObj = isType<object>('Object');

export const isHTMLElement = (obj: any): obj is HTMLElement => {
  return obj?.['nodeName'] || obj?.['tagName'];
};
