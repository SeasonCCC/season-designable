import { isArray, isString } from 'lodash-es';
import { Subscribable, Subscriber } from './subscribable';
import { globalThisPolyfill, isWindow } from './utils';

export const ATTACHED_SYMBOL = Symbol('ATTACHED_SYMBOL');
const EVENTS_SYMBOL = Symbol('__EVENTS_SYMBOL__');
const EVENTS_ONCE_SYMBOL = Symbol('EVENTS_ONCE_SYMBOL');
const EVENTS_BATCH_SYMBOL = Symbol('EVENTS_BATCH_SYMBOL');
const DRIVER_INSTANCES_SYMBOL = Symbol('DRIVER_INSTANCES_SYMBOL');

export type EventDriverContainer = HTMLElement | Document;

export type EventContainer = (Window | HTMLElement | Document) & {
  [ATTACHED_SYMBOL]?: IEventDriver[];
  [EVENTS_SYMBOL]?: Map<string, Map<any, boolean>>;
  [EVENTS_ONCE_SYMBOL]?: Map<string, any>;
  [EVENTS_BATCH_SYMBOL]?: Map<string, any>;
};

export interface CustomEvent<EventData = any, EventContext = any> {
  data?: EventData;
  context?: EventContext;
  // type: symbol;
}

export interface CustomEventClass {
  new (...args: any[]): any;
}

export type EventOptions =
  | boolean
  | (AddEventListenerOptions &
      EventListenerOptions & {
        mode?: 'onlyOne' | 'onlyParent' | 'onlyChild';
      });

export interface IEventDriver {
  container: EventDriverContainer;
  contentWindow: Window;
  attach(container: EventDriverContainer): void;
  detach(container: EventDriverContainer): void;
  dispatch<T extends CustomEvent<any> = any>(event: T): void | boolean;
  subscribe<T extends CustomEvent<any> = any>(
    type: string,
    subscriber: Subscriber<T>
  ): void;
  addEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: boolean | EventOptions
  ): void;
  addEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventOptions
  ): void;
  addEventListener(type: any, listener: any, options: any): void;
  removeEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: boolean | EventOptions
  ): void;
  removeEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventOptions
  ): void;
  removeEventListener(type: any, listener: any, options?: any): void;
  batchAddEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: boolean | EventOptions
  ): void;
  batchAddEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventOptions
  ): void;
  batchAddEventListener(type: any, listener: any, options?: any): void;
  batchRemoveEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: boolean | EventOptions
  ): void;
  batchRemoveEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventOptions
  ): void;
  batchRemoveEventListener(type: any, listener: any, options: any): void;
  eventTarget(type: string): EventContainer;
}

export interface EventEffect<T> {
  (engine: T): void;
}

export type EventDriverClass<T> = {
  new (engine: T, context?: any): IEventDriver;
};

export interface EventProps<T = any> {
  effects?: EventEffect<T>[];
  drivers?: EventDriverClass<T>[];
}

const isOnlyMode = (mode: string) =>
  mode === 'onlyOne' || mode === 'onlyChild' || mode === 'onlyParent';

export class EventDriver<Engine extends Event = Event, Context = any>
  implements IEventDriver
{
  engine: Engine;

  container: EventDriverContainer = document;

  contentWindow: Window = globalThisPolyfill;

  context!: Context;

  constructor(engine: Engine, context?: Context) {
    this.engine = engine;

    if (context) {
      this.context = context;
    }
  }

  ventTarget(type: string) {
    throw new Error('Method not implemented.');
  }

  attach(container: EventDriverContainer): void {
    throw new Error('Method not implemented.');
  }
  detach(container: EventDriverContainer): void {
    throw new Error('Method not implemented.');
  }

  dispatch<T extends CustomEvent<any, any> = any>(event: T): boolean | void {
    return this.engine.dispatch(event, this.context);
  }

  subscribe<T extends CustomEvent<any, any> = any>(
    type: string,
    subscriber: Subscriber<T>
  ) {
    return this.engine.subscribe(type, subscriber);
  }

  // subscribeTo<T extends CustomEventClass>(
  //   type: T,
  //   subscriber: Subscriber<InstanceType<T>>
  // ) {
  //   return this.engine.subscribeTo(type, subscriber);
  // }

  subscribeWith<T extends CustomEvent = CustomEvent>(
    type: string | string[],
    subscriber: Subscriber<T>
  ) {
    return this.engine.subscribeWith(type, subscriber);
  }

  eventTarget(type: string): EventContainer {
    if (type === 'resize' || type === 'scroll') {
      if (this.container === this.contentWindow?.document) {
        return this.contentWindow;
      }
    }
    return this.container;
  }

  addEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: EventOptions | undefined
  ): void;

  addEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: EventOptions | undefined
  ): void;

  addEventListener(type: any, listener: any, options?: any): void {
    const target = this.eventTarget(type);
    if (isOnlyMode(options?.mode)) {
      const eventsOnce = target[EVENTS_ONCE_SYMBOL] || new Map();
      const handler = eventsOnce.get(type);

      if (!handler) {
        target.addEventListener(type, listener, options);
        eventsOnce.set(type, listener);
      }
    } else {
      target.addEventListener(type, listener, options);
      const events = target[EVENTS_SYMBOL] || new Map();
      if (!events.get(type)) {
        events.set(type, new Map());
      }
      events.get(type).set(listener, true);
    }
  }

  removeEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: EventOptions | undefined
  ): void;
  removeEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: EventOptions | undefined
  ): void;
  removeEventListener(type: any, listener: any, options?: any): void {
    this.engine[DRIVER_INSTANCES_SYMBOL] =
      this.engine[DRIVER_INSTANCES_SYMBOL] || [];

    const target = this.eventTarget(type);

    const eventsBatch = target[EVENTS_ONCE_SYMBOL] || new Map();
    if (eventsBatch.get(type)) {
      eventsBatch.delete(type);
    }

    target.removeEventListener(type, listener, options);
  }

  batchAddEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: EventOptions | undefined
  ): void;
  batchAddEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: EventOptions | undefined
  ): void;
  batchAddEventListener(type: any, listener: any, options?: any) {
    this.engine[DRIVER_INSTANCES_SYMBOL] =
      this.engine[DRIVER_INSTANCES_SYMBOL] || [];

    if (!this.engine[DRIVER_INSTANCES_SYMBOL].includes(this)) {
      this.engine[DRIVER_INSTANCES_SYMBOL].push(this);
    }

    this.engine[DRIVER_INSTANCES_SYMBOL].forEach((driver) => {
      const target = driver.eventTarget(type);

      const eventsBatch = target[EVENTS_BATCH_SYMBOL] || new Map();

      if (!eventsBatch.get(type)) {
        // this.observable = fromEvent(target, 'mousedown');
        // this.observable.subscribe(listener);
        target.addEventListener(type, listener, options);
        eventsBatch.set(type, listener);
      }
    });
  }

  batchRemoveEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: EventOptions | undefined
  ): void;

  batchRemoveEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: EventOptions | undefined
  ): void;
  batchRemoveEventListener(type: any, listener: any, options: any): void {
    this.engine[DRIVER_INSTANCES_SYMBOL] =
      this.engine[DRIVER_INSTANCES_SYMBOL] || [];

    this.engine[DRIVER_INSTANCES_SYMBOL].forEach((driver) => {
      const target = driver.eventTarget(type);
      const eventsBatch = target[EVENTS_BATCH_SYMBOL] || new Map();
      if (eventsBatch.get(type)) {
        eventsBatch.delete(type);
      }
      target.removeEventListener(type, listener, options);
    });
  }
}

export class Event extends Subscribable<CustomEvent> {
  private drivers: EventDriverClass<any>[] = [];
  private containers: EventContainer[] = [];
  private [DRIVER_INSTANCES_SYMBOL]: IEventDriver[] = [];

  constructor(props?: EventProps) {
    super();

    if (props) {
      const { drivers, effects } = props;
      if (isArray(drivers)) {
        this.drivers = drivers;
      }

      if (isArray(effects)) {
        effects.forEach((plugin) => {
          plugin(this);
        });
      }
    }
  }

  subscribeTo<T extends CustomEventClass>(
    clazz: T,
    subscriber: Subscriber<InstanceType<T>>
  ) {
    if (!clazz) {
      return;
    }
    return this.subscribe(clazz.name, subscriber);
    // const effect = this[EFFECT_INSTANCES_SYMBOL].get(type);
    // if (!effect) {
    //   this[EFFECT_INSTANCES_SYMBOL].set(type, [subscriber]);
    // } else {
    //   this[EFFECT_INSTANCES_SYMBOL].set(type, [...effect, subscriber]);
    // }
    // this[DRIVER_INSTANCES_SYMBOL].forEach((driver) => {
    //   if (driver instanceof type) {
    //     driver.observable.subscribe((e) => subscriber(e));
    //   }
    // });
    // return this.subscribe((event) => {
    //   if (type && event instanceof type) {
    //     return subscriber(event);
    //   }
    // });
  }

  subscribeWith<T extends CustomEvent = CustomEvent>(
    type: string | string[],
    subscriber: Subscriber<T>
  ) {
    if (isString(type)) {
      return this.subscribe(type, subscriber);
    }

    if (isArray(type)) {
      return type.map((item) => this.subscribe(item, subscriber));
    }

    // return this.subscribe(type, (event) => {
    //   if (isArray(type) && type.includes(event?.type)) {
    //     return subscriber(event);
    //   } else if (isString(type) === event?.type) {
    //     return subscriber(event);
    //   }
    // });
  }

  attachEvents(
    container: EventContainer,
    contentWindow: Window = globalThisPolyfill,
    context?: any
  ): void {
    if (!container) return;
    if (isWindow(container)) {
      return this.attachEvents(container.document, container, context);
    }

    if (container[ATTACHED_SYMBOL]) return;

    container[ATTACHED_SYMBOL] = this.drivers.map((EventDriver) => {
      const driver = new EventDriver(this, context);
      driver.contentWindow = contentWindow;
      driver.container = container;
      driver.attach(container);
      return driver;
    });

    if (!this.containers.includes(container)) {
      this.containers.push(container);
    }
  }

  detachEvents(container?: EventContainer): void {
    if (!container) {
      this.containers.forEach((container) => {
        this.detachEvents(container);
      });
      return;
    }

    if (isWindow(container)) {
      return this.detachEvents(container.document);
    }

    if (!container[ATTACHED_SYMBOL]) return;

    container[ATTACHED_SYMBOL].forEach((driver) => {
      driver.detach(container);
    });

    this[DRIVER_INSTANCES_SYMBOL] = this[DRIVER_INSTANCES_SYMBOL].reduce(
      (drivers: IEventDriver[], driver) => {
        if (driver.container === container) {
          driver.detach(container);
          return drivers;
        }
        return drivers.concat(driver);
      },
      []
    );

    this.containers = this.containers.filter((item) => item !== container);
    delete container[ATTACHED_SYMBOL];
    // delete container[EVENTS_SYMBOL];
    // delete container[EVENTS_ONCE_SYMBOL];
    // delete container[EVENTS_BATCH_SYMBOL];
  }
}
