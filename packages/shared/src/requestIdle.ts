import { globalThisPolyfill } from './utils';

export interface IdleDeadline {
  didTimeout: boolean;
  timeRemaining: () => DOMHighResTimeStamp;
}

export interface IdleCallbackOptions {
  timeout?: number;
}

export const requestIdle = (
  callback: (params: IdleDeadline) => void,
  options?: IdleCallbackOptions
): number => {
  return globalThisPolyfill['requestIdleCallback'](callback, options);
};

export const cancelIdle = (id: number) => {
  globalThisPolyfill['cancelIdleCallback'](id);
};
