import { WorkbenchTypes } from '@season-designable/core';
import { CSSProperties, PropType, defineComponent, ref } from 'vue';
import { useWorkbench } from '../../hooks/useWorkbench';
import { Code } from '../../icons/Code';
import { Design } from '../../icons/Design';
import { Json } from '../../icons/Json';
import { Play } from '../../icons/Play';
import { Svg } from '../../icons/Svg';
import { IconWidget } from '../IconWidget';
import { useBehaviorSubject } from '../../hooks';

export const ViewToolsWidget = defineComponent({
  props: {
    className: String,
    style: Object as PropType<CSSProperties>,
    use: {
      type: Array as PropType<Array<WorkbenchTypes>>,
      default: ['DESIGNABLE', 'JSONTREE', 'MARKUP', 'PREVIEW'],
    },
  },
  setup(props) {
    const workbench = useWorkbench();
    const workbenchType = useBehaviorSubject(workbench?.type);

    // const workbenchType = ref(workbench?.type.value);

    // workbench?.type.subscribe({
    //   next: (v) => (workbenchType.value = v),
    // });

    return () => (
      <>
        <div class="btn-group" style={{ marginRight: 20 }}>
          {props.use.includes('DESIGNABLE') && (
            <button
              class="btn"
              disabled={workbenchType.value === 'DESIGNABLE'}
              onClick={() => {
                workbench?.setWorkbenchType('DESIGNABLE');
              }}
            >
              <IconWidget>
                <Svg viewBox="0 0 1260 1024">
                  <Design />
                </Svg>
              </IconWidget>
            </button>
          )}
          {props.use.includes('JSONTREE') && (
            <button
              class="btn"
              disabled={workbenchType.value === 'JSONTREE'}
              onClick={() => {
                workbench?.setWorkbenchType('JSONTREE');
              }}
            >
              <IconWidget>
                <Svg>
                  <Json />
                </Svg>
              </IconWidget>
            </button>
          )}
          {props.use.includes('MARKUP') && (
            <button
              class="btn"
              disabled={workbenchType.value === 'MARKUP'}
              onClick={() => {
                workbench?.setWorkbenchType('MARKUP');
              }}
            >
              <IconWidget>
                <Svg>
                  <Code />
                </Svg>
              </IconWidget>
            </button>
          )}
          {props.use.includes('PREVIEW') && (
            <button
              class="btn"
              disabled={workbenchType.value === 'PREVIEW'}
              onClick={() => {
                workbench?.setWorkbenchType('PREVIEW');
              }}
            >
              <IconWidget>
                <Svg>
                  <Play />
                </Svg>
              </IconWidget>
            </button>
          )}
        </div>
      </>
    );
  },
});
