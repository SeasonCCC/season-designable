import { TreeNode } from '@season-designable/core';
import { PropType, defineComponent } from 'vue';

export const NodeTitleWidget = defineComponent({
  props: {
    node: {
      type: Object as PropType<TreeNode>,
      required: true,
    },
  },
  setup(props) {
    const takeNode = () => {
      const { node } = props;
      if (node.componentName === '$$ResourceNode$$') {
        return node.children.value[0];
      }
      return node;
    };

    const node = takeNode();
    return () => <>{node.getMessage('title') || node.componentName}</>;
  },
});
