import { TreeNode } from '@season-designable/core';
import {
  CSSProperties,
  computed,
  defineComponent,
  nextTick,
  ref,
  watch,
} from 'vue';
import { useValidNodeOffsetRect, useViewport } from '../../hooks';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { useCursor } from '../../hooks/useCursor';
import { useDesigner } from '../../hooks/useDesigner';
import { useMoveHelper } from '../../hooks/useMoveHelper';
import { usePrefix } from '../../hooks/usePrefix';
import { useSelection } from '../../hooks/useSelection';
import { useTree } from '../../hooks/useTree';
import { Helpers } from './Helpers';
import { Rect } from '@season-designable/shared';

export const SelectionBox = defineComponent({
  props: {
    node: { type: TreeNode, required: true },
    showHelpers: Boolean,
  },
  setup(props) {
    const designer = useDesigner();
    const viewport = useViewport();
    const prefix = usePrefix('aux-selection-box');
    const innerPrefix = usePrefix('aux-selection-box-inner');
    const children = useBehaviorSubject(props.node.children);
    const nodeProps = useBehaviorSubject(props.node.props);
    const viewportElement = useBehaviorSubject(viewport?.viewportElement);
    const viewportWidth = useBehaviorSubject(viewport?.width);
    const viewportHeight = useBehaviorSubject(viewport?.height);

    const nodeRect = ref<Rect>();

    watch(
      [nodeProps, viewportElement, viewportWidth, viewportHeight, children],
      async () => {
        await nextTick();
        let rect = useValidNodeOffsetRect(props.node);

        if (props.node.isRoot) {
          let viewportRect = viewportElement.value?.getBoundingClientRect();
          if (rect && viewportRect) {
            if (rect.width < viewportRect.width) {
              rect.width = viewportRect.width;
            }

            if (rect.height < viewportRect.height) {
              rect.height = viewportRect.height;
            }
          }
        }

        nodeRect.value = rect;
      },
      {
        immediate: true,
      }
    );

    // const nodeRect = computed(() => {
    //   if (
    //     nodeProps.value &&
    //     viewportElement.value &&
    //     viewportWidth.value &&
    //     viewportHeight.value &&
    //     children.value
    //   ) {
    //     let rect = useValidNodeOffsetRect(props.node);

    //     if (props.node.isRoot) {
    //       let viewportRect = viewportElement.value?.getBoundingClientRect();
    //       if (rect) {
    //         if (rect.width < viewportRect.width) {
    //           rect.width = viewportRect.width;
    //         }

    //         if (rect.height < viewportRect.height) {
    //           rect.height = viewportRect.height;
    //         }
    //       }
    //     }

    //     return rect;
    //   }
    // });

    const style = computed(() => {
      const baseStyle: CSSProperties = {
        position: 'absolute',
        top: 0,
        left: 0,
        boxSizing: 'border-box',
        zIndex: 4,
      };

      if (viewportElement.value) {
        if (nodeRect.value) {
          baseStyle.transform = `perspective(1px) translate3d(${nodeRect.value.x}px,${nodeRect.value.y}px,0)`;
          baseStyle.height = nodeRect.value.height + 'px';
          baseStyle.width = nodeRect.value.width + 'px';
        }
      }

      return baseStyle;
    });

    const nodeSelectionIdAttrName = designer?.props.nodeSelectionIdAttrName;

    const selectionId = nodeSelectionIdAttrName && {
      [nodeSelectionIdAttrName]: props.node?.id,
    };

    return () => (
      <div {...selectionId} class={prefix} style={style.value}>
        <div class={innerPrefix}></div>
        {props.showHelpers && (
          <Helpers {...props} node={props.node} nodeRect={nodeRect.value} />
        )}
      </div>
    );
  },
});

export const Selection = defineComponent({
  setup() {
    const tree = useTree();

    const cursor = useCursor();
    const status = useBehaviorSubject(cursor?.status);

    const selection = useSelection();
    const selected = useBehaviorSubject(selection?.selected);

    const viewportMoveHelper = useMoveHelper();
    const touchNode = useBehaviorSubject(viewportMoveHelper?.touchNode);

    return () => {
      if (status.value !== 'NORMAL' && touchNode.value) {
        return null;
      }

      return selected.value.map((id) => {
        const node = tree?.findById(id);
        if (!node) return null;
        if (node.hidden) return null;
        return (
          <SelectionBox
            key={id}
            node={node}
            showHelpers={selected.value.length === 1}
          />
        );
      });
    };
  },
});
