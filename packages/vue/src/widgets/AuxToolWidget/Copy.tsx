import { TreeNode } from '@season-designable/core';
import { defineComponent } from 'vue';
import { usePrefix } from '../../hooks/usePrefix';
import { Clone } from '../../icons/Clone';
import { Svg } from '../../icons/Svg';
import { IconWidget } from '../IconWidget';

export const Copy = defineComponent({
  props: {
    node: { type: TreeNode, required: true },
  },
  setup(props) {
    const prefix = usePrefix('aux-copy');

    if (props.node.isRoot) return () => null;

    return () => (
      <button
        class={prefix}
        onClick={() => {
          TreeNode.clone([props.node]);
        }}
      >
        <IconWidget>
          <Svg>
            <Clone />
          </Svg>
        </IconWidget>
      </button>
    );
  },
});
