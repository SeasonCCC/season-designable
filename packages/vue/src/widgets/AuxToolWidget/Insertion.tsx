import { CSSProperties, computed, defineComponent } from 'vue';
import { usePrefix } from '../../hooks/usePrefix';
import { useMoveHelper } from '../../hooks/useMoveHelper';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { ClosestPosition } from '@season-designable/core';

export const Insertion = defineComponent({
  setup(props) {
    const prefix = usePrefix('aux-insertion');
    const moveHelper = useMoveHelper();
    const closestDirection = useBehaviorSubject(
      moveHelper?.viewportClosestDirection
    );
    const closestRect = useBehaviorSubject(
      moveHelper?.viewportClosestOffsetRect
    );

    const createInsertionStyle = computed(() => {
      const isInlineLayout =
        moveHelper?.closestNode.value?.moveLayout === 'horizontal';

      const baseStyle: CSSProperties = {
        position: 'absolute',
        transform: 'perspective(1px) translate3d(0,0,0)',
        top: 0,
        left: 0,
      };

      if (!closestRect.value) return baseStyle;

      if (
        closestDirection.value === ClosestPosition.Before ||
        closestDirection.value === ClosestPosition.ForbidBefore
      ) {
        baseStyle.width = closestRect.value.width + 'px';
        baseStyle.height = '2px';
        baseStyle.transform = `perspective(1px) translate3d(${closestRect.value.x}px,${closestRect.value.y}px,0)`;
      } else if (
        closestDirection.value === ClosestPosition.After ||
        closestDirection.value === ClosestPosition.ForbidAfter
      ) {
        baseStyle.width = '2px';
        baseStyle.height = closestRect.value.height + 'px';
        baseStyle.transform = `perspective(1px) translate3d(${
          closestRect.value.x + closestRect.value.width - 2
        }px,${closestRect.value.y}px,0)`;
      } else if (
        closestDirection.value === ClosestPosition.Upper ||
        closestDirection.value === ClosestPosition.ForbidUpper
      ) {
        baseStyle.width = closestRect.value.width + 'px';
        baseStyle.height = '2px';
        baseStyle.transform = `perspective(1px) translate3d(${
          closestRect.value.x - 2
        }px,${closestRect.value.y}px,0)`;
      } else if (
        closestDirection.value === ClosestPosition.Under ||
        closestDirection.value === ClosestPosition.ForbidUnder
      ) {
        baseStyle.width = closestRect.value.width + 'px';
        baseStyle.height = '2px';
        baseStyle.transform = `perspective(1px) translate3d(${
          closestRect.value.x - 2
        }px,${closestRect.value.y + closestRect.value.height}px,0)`;
      }

      return baseStyle;
    });

    return () => <div class={prefix} style={createInsertionStyle.value}></div>;
  },
});
