import {
  ClosestPosition,
  CursorStatus,
  TreeNode,
} from '@season-designable/core';
import cls from 'classnames';
import { CSSProperties, computed, defineComponent } from 'vue';
import { useCursor, useValidNodeOffsetRect, useViewport } from '../../hooks';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { useMoveHelper } from '../../hooks/useMoveHelper';
import { usePrefix } from '../../hooks/usePrefix';

export const CoverRect = defineComponent({
  props: {
    dragging: Boolean,
    dropping: Boolean,
    node: { type: TreeNode, required: true },
  },
  setup(props) {
    const prefix = usePrefix('aux-cover-rect');
    const rect = useValidNodeOffsetRect(props.node);

    const createCoverStyle = computed(() => {
      const baseStyle: CSSProperties = {
        position: 'absolute',
        top: 0,
        left: 0,
        pointerEvents: 'none',
      };

      if (rect) {
        baseStyle.transform = `perspective(1px) translate3d(${rect.x}px,${rect.y}px,0)`;
        baseStyle.height = rect.height + 'px';
        baseStyle.width = rect.width + 'px';
      }

      return baseStyle;
    });

    return () => (
      <div
        class={cls(prefix, {
          dragging: props.dragging,
          dropping: props.dropping,
        })}
        style={createCoverStyle.value}
      ></div>
    );
  },
});

export const Cover = defineComponent({
  setup() {
    const viewport = useViewport();
    const cursor = useCursor();
    const moveHelper = useMoveHelper();

    const status = useBehaviorSubject(cursor?.status);
    const closestNode = useBehaviorSubject(moveHelper?.closestNode);
    const dragNodes = useBehaviorSubject(moveHelper?.dragNodes);
    const viewportClosestDirection = useBehaviorSubject(
      moveHelper?.viewportClosestDirection
    );

    const renderDropCover = () => {
      if (
        !closestNode.value ||
        !closestNode.value?.allowAppend(dragNodes.value) ||
        viewportClosestDirection.value !== ClosestPosition.Inner
      ) {
        return null;
      }

      if (closestNode.value) {
        return <CoverRect node={closestNode.value} dropping />;
      }
    };

    return () =>
      status.value === CursorStatus.Dragging ? (
        <>
          {moveHelper?.dragNodes.value.map((node) => {
            if (!node) return;
            if (!viewport?.findElementById(node.id)) return;

            return <CoverRect key={node.id} node={node} dragging />;
          })}
          {renderDropCover()}
        </>
      ) : null;
  },
});
