import { TreeNode } from '@season-designable/core';
import { Rect } from '@season-designable/shared';
import cls from 'classnames';
import { defineComponent, ref } from 'vue';
import { usePrefix } from '../../hooks/usePrefix';
import { Svg } from '../../icons/Svg';
import { IconWidget } from '../IconWidget';
import { Delete as DeleteIcon } from '../../icons/Delete';

export const Delete = defineComponent({
  props: {
    node: { type: TreeNode, required: true },
  },
  setup(props) {
    const prefix = usePrefix('aux-delete');

    if (props.node.isRoot) return () => null;

    return () => (
      <button
        class={prefix}
        onClick={() => {
          TreeNode.remove([props.node]);
        }}
      >
        <IconWidget>
          <Svg>
            <DeleteIcon />
          </Svg>
        </IconWidget>
      </button>
    );
  },
});
