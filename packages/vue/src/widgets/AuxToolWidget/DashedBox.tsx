import { CSSProperties, computed, defineComponent, watch } from 'vue';
import { useSelection, useValidNodeOffsetRect, useViewport } from '../../hooks';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { useHover } from '../../hooks/useHover';
import { usePrefix } from '../../hooks/usePrefix';

export const DashedBox = defineComponent({
  setup() {
    const hover = useHover();
    const prefix = usePrefix('aux-dashed-box');
    const selection = useSelection();

    const selected = useBehaviorSubject(selection?.selected);
    const hoverNode = useBehaviorSubject(hover?.node);

    const viewport = useViewport();
    const viewportElement = useBehaviorSubject(viewport?.viewportElement);
    const viewportWidth = useBehaviorSubject(viewport?.width);
    const viewportHeight = useBehaviorSubject(viewport?.height);

    const nodeRect = computed(() => {
      if (
        viewportElement.value &&
        viewportWidth.value &&
        viewportHeight.value &&
        hoverNode.value
      ) {
        return useValidNodeOffsetRect(hoverNode.value);
      }
    });

    const createTipsStyle = () => {
      const baseStyle: CSSProperties = {
        top: 0,
        left: 0,
        pointerEvents: 'none',
        boxSizing: 'border-box',
        visibility: 'hidden',
        zIndex: 2,
      };

      if (nodeRect.value) {
        baseStyle.transform = `perspective(1px) translate3d(${nodeRect.value.x}px,${nodeRect.value.y}px,0)`;
        baseStyle.height = nodeRect.value.height + 'px';
        baseStyle.width = nodeRect.value.width + 'px';
        baseStyle.visibility = 'visible';
      }

      return baseStyle;
    };

    return () => {
      if (!hoverNode.value) return null;
      if (hoverNode.value.hidden) return null;
      if (selected.value.includes(hoverNode.value.id)) return null;

      return (
        <div class={prefix} style={createTipsStyle()}>
          <span
            class={prefix + '-title'}
            style={{
              position: 'absolute',
              bottom: '100%',
              left: 0,
              fontSize: 12,
              userSelect: 'none',
              fontWeight: 'lighter',
              whiteSpace: 'nowrap',
            }}
          >
            {hoverNode.value.getMessage('title')}
          </span>
        </div>
      );
    };
  },
});
