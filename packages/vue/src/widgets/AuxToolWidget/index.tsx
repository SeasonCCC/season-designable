import {
  CSSProperties,
  PropType,
  defineComponent,
  ref,
  toRaw,
  watch,
} from 'vue';
import { useCursor } from '../../hooks/useCursor';
import { useHistory } from '../../hooks/useHistory';
import { usePrefix } from '../../hooks/usePrefix';
import { useScreen } from '../../hooks/useScreen';
import { useWorkbench } from '../../hooks/useWorkbench';
import './style.scss';
import { useDesigner } from '../../hooks/useDesigner';
import { useViewport } from '../../hooks/useViewport';
import { Insertion } from './Insertion';
import { Cover } from './Cover';
import { Selection } from './Selection';
import { DashedBox } from './DashedBox';

export const AuxWidget = defineComponent({
  setup(props) {
    const engine = useDesigner();
    const viewport = useViewport();
    const prefix = usePrefix('auxtool');

    const elementRef = ref<HTMLDivElement | null>(null);

    if (!viewport) return null;

    return () => (
      <div ref={elementRef} class={prefix}>
        <Insertion />
        <Cover />
        <Selection />
        <DashedBox />
      </div>
    );
  },
});
