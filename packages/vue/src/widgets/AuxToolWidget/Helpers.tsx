import { TreeNode } from '@season-designable/core';
import { Rect } from '@season-designable/shared';
import cls from 'classnames';
import { Selector } from './Selector';
import { PropType, defineComponent, ref } from 'vue';
import { useViewport } from '../../hooks';
import { usePrefix } from '../../hooks/usePrefix';
import { Copy } from './Copy';
import { Delete } from './Delete';
import { DragHandler } from './DragHandler';

export const Helpers = defineComponent({
  props: {
    node: { type: TreeNode, required: true },
    nodeRect: Object as PropType<Rect>,
  },
  setup(props) {
    const prefix = usePrefix('aux-helpers');
    const viewport = useViewport();
    const element = ref<HTMLDivElement | null>(null);
    const position = ref('top-right');

    return () => (
      <div
        class={cls(prefix, {
          [position.value]: true,
        })}
        ref={ref}
      >
        <div class={`${prefix}-content`}>
          <Selector node={props.node}></Selector>
          {props.node.allowClone() && <Copy node={props.node} />}
          {props.node.allowDrag() === false ? null : (
            <DragHandler node={props.node} />
          )}
          {props.node.allowDelete() === false ? null : (
            <Delete node={props.node} />
          )}
        </div>
      </div>
    );
  },
});
