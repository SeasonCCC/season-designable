import { TreeNode } from '@season-designable/core';
import { defineComponent } from 'vue';
import { usePrefix } from '../../hooks/usePrefix';
import { Move } from '../../icons/Move';
import { Svg } from '../../icons/Svg';
import { IconWidget } from '../IconWidget';
import { useDesigner } from '../../hooks';

export const DragHandler = defineComponent({
  props: {
    node: { type: TreeNode, required: true },
  },
  setup(props) {
    const designer = useDesigner();

    const prefix = usePrefix('aux-drag-handler');

    if (props.node.isRoot) return () => null;

    const handlerProps = designer && {
      [designer.props.nodeDragHandlerAttrName]: 'true',
    };

    return () => (
      <button class={prefix} {...handlerProps}>
        <IconWidget>
          <Svg>
            <Move />
          </Svg>
        </IconWidget>
      </button>
    );
  },
});
