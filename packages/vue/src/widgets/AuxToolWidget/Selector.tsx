import { TreeNode } from '@season-designable/core';
import { defineComponent, ref } from 'vue';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { useHover } from '../../hooks/useHover';
import { usePrefix } from '../../hooks/usePrefix';
import { useSelection } from '../../hooks/useSelection';
import { Svg } from '../../icons/Svg';
import { IconWidget } from '../IconWidget';
import { NodeTitleWidget } from '../NodeTitleWidget';
import { Component as ComponentIcon } from '../../icons/Component';

export const Selector = defineComponent({
  props: {
    node: { type: TreeNode, required: true },
  },
  setup(props) {
    const prefix = usePrefix('aux-selector');

    const selection = useSelection();
    const hover = useHover();
    const hoverNode = useBehaviorSubject(hover?.node);

    const expand = ref(false);
    const element = ref<HTMLDivElement | null>(null);

    return () => (
      <div ref={element} class={prefix}>
        <button class={prefix}>
          <IconWidget>
            <Svg>
              <ComponentIcon />
            </Svg>
          </IconWidget>
          <span>
            <NodeTitleWidget node={props.node} />
          </span>
        </button>
      </div>
    );
  },
});
