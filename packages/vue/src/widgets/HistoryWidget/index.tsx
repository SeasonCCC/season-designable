import { defineComponent } from 'vue';
import cls from 'classnames';
import { useBehaviorSubject, useWorkbench } from '../../hooks';
import { usePrefix } from '../../hooks/usePrefix';
import './style.scss';
import { TextWidget } from '../TextWidget';

const format = (timestamp: number) => {
  const date = new Date(timestamp);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  let formattedDate = `${year}-${month < 10 ? '0' + month : month}-${
    day < 10 ? '0' + day : day
  } ${hours}:${minutes < 10 ? '0' + minutes : minutes}:${
    seconds < 10 ? '0' + seconds : seconds
  }`;

  return formattedDate;
};

const humpToUnderline = (str: string) => {
  let temp = str.replace(/[A-Z]/g, (match) => {
    return '_' + match.toLowerCase();
  });

  if (temp.slice(0, 1) === '_') {
    temp = temp.slice(1);
  }
  return temp;
};

export const HistoryWidget = defineComponent({
  setup() {
    const prefix = usePrefix('history');
    const workbench = useWorkbench();
    const currentWorkspace =
      workbench?.activeWorkspace || workbench?.currentWorkspace;

    const history = useBehaviorSubject(currentWorkspace?.history.list());
    const current = useBehaviorSubject(currentWorkspace?.history.current);

    return () => (
      <div class={prefix}>
        {history.value.map((item, index) => {
          const type = item.type || 'default_state';
          const token = humpToUnderline(type.replace('Event', ''));
          return (
            <div
              class={cls(prefix + '-item', {
                active: current.value === index,
              })}
              key={item.timestamp}
              onClick={() => {
                currentWorkspace?.history.goTo(index);
              }}
            >
              <span class={prefix + '-item-title'}>
                <TextWidget token={`operations.${token}`}></TextWidget>
              </span>
              <span class={prefix + '-item-timestamp'}>
                {' '}
                {format(item.timestamp)}
              </span>
            </div>
          );
        })}
      </div>
    );
  },
});
