import {
  VNode,
  computed,
  defineComponent,
  PropType,
  CSSProperties,
  isVNode,
  cloneVNode,
  renderSlot,
} from 'vue';
import cls from 'classnames';
import './style.scss';
import { useTheme } from '../../hooks/useTheme';
import { usePrefix } from '../../hooks/usePrefix';
import { isFunction, isObject, isString } from 'lodash-es';
import { useRegistry } from '../../hooks/useRegistry';
import { EngineContext, IconSymbol } from '../../context';

export interface IconProviderProps {
  tooltip?: boolean;
}

export interface ShadowSVGProps {
  content?: string;
  width?: number | string;
  height?: number | string;
}

export interface IconWidgetProps extends HTMLElement {
  tooltip?: VNode;
  infer: VNode | { shadow: string };
  size?: number | string;
}

export const IconWidget = defineComponent({
  props: {
    icon: String,
  },
  emits: ['click'],
  setup(props, { slots, emit, attrs }) {
    // const theme = useTheme();
    // const registry = useRegistry();
    // const iconContext = EngineContext[IconSymbol];

    const prefix = usePrefix('icon');
    const style = attrs.style as CSSProperties;
    const className = attrs.class as string;

    const renderIcon = () => {
      if (slots['default']) {
        return renderSlot(slots, 'default');
      } else {
        return <img src={props.icon} />;
      }
    };

    return () => (
      <div
        class={cls(prefix, className)}
        style={style}
        onClick={() => emit('click')}
      >
        {renderIcon()}
      </div>
    );
  },
});
