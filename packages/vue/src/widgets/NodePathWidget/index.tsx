import cls from 'classnames';
import { computed, defineComponent } from 'vue';
import { useBehaviorSubject, useSelection, useTree } from '../../hooks';
import { useHover } from '../../hooks/useHover';
import { usePrefix } from '../../hooks/usePrefix';
import { Position } from '../../icons/Position';
import { Svg } from '../../icons/Svg';
import { IconWidget } from '../IconWidget';
import { NodeTitleWidget } from '../NodeTitleWidget';
import './style.scss';

export const NodePathWidget = defineComponent({
  props: {
    workspaceId: String,
    maxItems: Number,
  },
  setup(props) {
    const prefix = usePrefix('node-path');
    const selection = useSelection(props.workspaceId);
    const hover = useHover(props.workspaceId);
    const tree = useTree(props.workspaceId);
    const selected = useBehaviorSubject(selection?.selected);
    const selectedNode = computed(() => tree?.findById(selected.value[0]));
    const maxItems = props.maxItems ?? 3;

    const nodes = computed(() =>
      selectedNode.value
        ?.getParents()
        .slice(0, maxItems - 1)
        .reverse()
        .concat(selectedNode.value)
    );

    return () => (
      <nav class={prefix}>
        <ol>
          {nodes.value?.map((node, index) => (
            <li>
              <span>
                {index === 0 && (
                  <IconWidget style={{ marginRight: 3 }}>
                    <Svg>
                      <Position />
                    </Svg>
                  </IconWidget>
                )}
                <a
                  onMouseenter={() => {
                    hover?.setHover(node);
                  }}
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    selection?.select(node);
                  }}
                >
                  <NodeTitleWidget node={node} />
                </a>
              </span>
              <span class={prefix + '-breadcrumb-separator'}>/</span>
            </li>
          ))}
        </ol>
      </nav>
    );
  },
});
