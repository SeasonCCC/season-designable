import { TreeNode } from '@season-designable/core';
import { PropType, computed, defineComponent, renderSlot } from 'vue';
import { useBehaviorSubject, useDesigner, useTreeNode } from '../../hooks';
import { usePrefix } from '../../hooks/usePrefix';
import './style.scss';
import { NodeTitleWidget } from '../NodeTitleWidget';

export const DroppableWidget = defineComponent({
  props: {
    node: { type: Object as PropType<TreeNode> },
    placeholder: { type: Boolean, default: () => true },
  },
  setup(props, { slots }) {
    const designer = useDesigner();
    const currentNode = useTreeNode();
    const prefix = usePrefix('droppable-placeholder');
    const target = props.node ?? currentNode;
    const children = useBehaviorSubject(target?.children);

    const nodeId = designer
      ? {
          [designer.props.nodeIdAttrName]: props.node
            ? props.node.id
            : target?.id,
        }
      : {};

    return () =>
      children.value.length > 0
        ? renderSlot(slots, 'default')
        : target && (
            <div class={prefix}>
              <NodeTitleWidget node={target} />
            </div>
          );
  },
});
