import {
  DesignerMiniLocales,
  GlobalRegistry,
  getISOCode,
} from '@season-designable/core';
import { isObject, isString } from 'lodash-es';
import { computed, defineComponent, watch } from 'vue';
import { useBehaviorSubject } from '../../hooks';

export const TextWidget = defineComponent({
  props: {
    componentName: String,
    sourceName: String,
    token: [String, Object],
    defaultMessage: String,
  },
  setup(props, { slots }) {
    const langRef = useBehaviorSubject(
      GlobalRegistry.getDesignerLanguageStore()
    );

    // const takeLocale = (message: string | DesignerMiniLocales) => {
    //   if (isString(message)) return message;
    //   if (isObject(message)) {
    //     const lang = GlobalRegistry.getDesignerLanguage();
    //     for (let key in message) {
    //       if (key.toLocaleLowerCase() === lang) return message[key];
    //     }
    //     return;
    //   }

    //   return message;
    // };

    // const takeMessage = (token: any) => {
    //   if (!token) return;

    //   const message = isString(token)
    //     ? GlobalRegistry.getDesignerMessage(token)
    //     : token;

    //   if (message) return takeLocale(message);

    //   return token;
    // };

    const takeMessage = computed(() => {
      const lang = getISOCode(langRef.value);
      let token;

      if (slots.default?.()?.[0].children) {
        token = slots.default?.()?.[0].children;
      } else if (props.token) {
        token = props.token;
      } else if (props.defaultMessage) {
        token = props.defaultMessage;
      }

      if (!token) return;

      const message: string | Record<string, string> = isString(token)
        ? GlobalRegistry.getDesignerMessage(token)
        : token;

      if (message) {
        if (isString(message)) return message;
        if (isObject(message)) {
          for (let key in message) {
            if (key.toLocaleLowerCase() === lang) return message[key];
          }
          return;
        }

        return message;
      }

      return token;
    });

    return () => <>{takeMessage.value}</>;
  },
});
