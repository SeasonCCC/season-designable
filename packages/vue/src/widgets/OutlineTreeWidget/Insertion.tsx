import { ClosestPosition } from '@season-designable/core';
import { CSSProperties, computed, defineComponent } from 'vue';
import { useBehaviorSubject, useMoveHelper, usePrefix } from '../../hooks';

export const Insertion = defineComponent({
  props: {
    workspaceId: { type: String, required: true },
  },
  setup(props) {
    const prefix = usePrefix('outline-tree-insertion');
    const moveHelper = useMoveHelper(props.workspaceId);
    const closestDirection = useBehaviorSubject(
      moveHelper?.outlineClosestDirection
    );
    const closestRect = useBehaviorSubject(
      moveHelper?.outlineClosestOffsetRect
    );

    const createInsertionStyle = computed(() => {
      const baseStyle: CSSProperties = {
        position: 'absolute',
        transform: 'perspective(1px) translate3d(0,0,0)',
        top: 0,
        left: 0,
      };

      if (!closestRect.value) return baseStyle;

      if (
        closestDirection.value === ClosestPosition.Before ||
        closestDirection.value === ClosestPosition.InnerBefore ||
        closestDirection.value === ClosestPosition.ForbidBefore ||
        closestDirection.value === ClosestPosition.Upper ||
        closestDirection.value === ClosestPosition.ForbidInnerBefore ||
        closestDirection.value === ClosestPosition.ForbidUpper
      ) {
        baseStyle.width = closestRect.value.width + 'px';
        baseStyle.height = '2px';
        baseStyle.transform = `perspective(1px) translate3d(${closestRect.value.x}px,${closestRect.value.y}px,0)`;
      } else if (
        closestDirection.value === ClosestPosition.After ||
        closestDirection.value === ClosestPosition.ForbidAfter ||
        closestDirection.value === ClosestPosition.InnerAfter ||
        closestDirection.value === ClosestPosition.Under ||
        closestDirection.value === ClosestPosition.ForbidUnder ||
        closestDirection.value === ClosestPosition.ForbidInnerAfter
      ) {
        baseStyle.width = closestRect.value.width + 'px';
        baseStyle.height = '2px';
        baseStyle.transform = `perspective(1px) translate3d(${
          closestRect.value.x
        }px,${closestRect.value.y + closestRect.value.height - 2}px,0)`;
      }

      return baseStyle;
    });

    return () => <div class={prefix} style={createInsertionStyle.value}></div>;
  },
});
