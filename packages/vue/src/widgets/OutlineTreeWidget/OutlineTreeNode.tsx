import cls from 'classnames';
import { defineComponent, onMounted, ref } from 'vue';
import { useBehaviorSubject, useSelection, useWorkbench } from '../../hooks';
import { usePrefix } from '../../hooks/usePrefix';
import { TreeNode } from '@season-designable/core';
import { NodeTitleWidget } from '../NodeTitleWidget';
import { IconWidget } from '../IconWidget';
import { Expand, Svg } from '../../icons';
import { Design } from '../../icons/Design';
import { Component } from '../../icons/Component';

export const OutlineTreeNode = defineComponent({
  props: {
    node: {
      type: TreeNode,
      required: true,
    },
    workspaceId: { type: String, required: true },
  },
  setup(props) {
    const prefix = usePrefix('outline-tree-node');
    const workbench = useWorkbench();
    const expand = ref(true);
    const children = useBehaviorSubject(props.node.children);
    const selection = useSelection(props.workspaceId);
    const selected = useBehaviorSubject(selection?.selected);

    const renderIcon = (node: TreeNode) => {
      if (node === node.root) {
        return (
          <IconWidget>
            <Svg>
              <Design />
            </Svg>
          </IconWidget>
        );
      }

      return (
        <IconWidget>
          <Svg>
            <Component />
          </Svg>
        </IconWidget>
      );
    };

    const renderTitle = (node: TreeNode) => {
      return (
        <span>
          <NodeTitleWidget node={node} />
        </span>
      );
    };

    return () => (
      <div
        class={cls(prefix, {
          expanded: expand.value,
          selected: selected.value.includes(props.node.id),
        })}
        data-designer-outline-node-id={props.node.id}
      >
        <div class={prefix + '-header'}>
          <div
            class={prefix + '-header-head'}
            style={{
              left: -props.node.depth * 16 + 'px',
              width: props.node.depth * 16 + 'px',
            }}
          ></div>
          <div class={prefix + '-header-content'}>
            <div class={prefix + '-header-base'}>
              {(children.value.length > 0 ||
                props.node === props.node.root) && (
                <div
                  class={prefix + '-expand'}
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    expand.value = !expand.value;
                  }}
                >
                  <IconWidget>
                    <Svg width="10" height="10" viewBox="0 0 20 20">
                      <Expand />
                    </Svg>
                  </IconWidget>
                </div>
              )}

              <div class={prefix + '-icon'}>{renderIcon(props.node)}</div>
              <div class={prefix + '-title'}>{renderTitle(props.node)}</div>
            </div>
          </div>
        </div>
        <div class={prefix + '-children'}>
          {children.value.map((child) => {
            return (
              <OutlineTreeNode
                node={child}
                key={child.id}
                workspaceId={props.workspaceId}
              />
            );
          })}
        </div>
      </div>
    );
  },
});
