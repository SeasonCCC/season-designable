import { globalThisPolyfill } from '@season-designable/shared';
import { defineComponent, onMounted, onUnmounted, ref } from 'vue';
import { useOutline, useWorkbench } from '../../hooks';
import { usePrefix } from '../../hooks/usePrefix';
import { useTree } from '../../hooks/useTree';
import { Insertion } from './Insertion';
import { OutlineTreeNode } from './OutlineTreeNode';
import './style.scss';

export const OutlineTreeWidget = defineComponent({
  setup(props) {
    const prefix = usePrefix('outline-tree');
    const workbench = useWorkbench();
    const current = workbench?.activeWorkspace || workbench?.currentWorkspace;
    const workspaceId = current?.id;
    const tree = useTree();
    const outline = useOutline(workspaceId);
    const elementRef = ref<HTMLDivElement | null>(null);

    onMounted(() => {
      if (elementRef.value) {
        outline?.onMount(elementRef.value, globalThisPolyfill);
      }
    });

    onUnmounted(() => {
      outline?.onUnmount();
    });

    return () => (
      <div class={prefix + '-container'}>
        <div class={prefix + '-content'} ref={elementRef}>
          {tree && workspaceId && (
            <OutlineTreeNode
              node={tree}
              workspaceId={workspaceId}
            ></OutlineTreeNode>
          )}
          <div class={prefix + '-aux'}>
            {workspaceId && <Insertion workspaceId={workspaceId} />}
          </div>
        </div>
      </div>
    );
  },
});
