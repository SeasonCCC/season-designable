import {
  GlobalRegistry,
  Resource,
  ResourceLike,
  isResourceHost,
  isResourceList,
} from '@season-designable/core';
import cls from 'classnames';
import { isFunction } from 'lodash-es';
import { VNode, defineComponent, isVNode, ref, renderSlot } from 'vue';
import { usePrefix } from '../../hooks/usePrefix';
import { IconWidget } from '../IconWidget';
import { TextWidget } from '../TextWidget';
import './style.scss';
import { Expand, Svg } from '../../icons';
import { useBehaviorSubject } from '../../hooks';

export interface IResourceWidgetProps {
  title: VNode;
  sources?: ResourceLike[];
  className?: string;
  defaultExpand?: boolean;
  children?: VNode[];
}

export const ResourceWidget = defineComponent({
  props: {
    defaultExpand: { type: Boolean, default: true },
    sources: { type: Array, default: () => [] },
    className: String,
    title: String,
  },
  setup(props, { slots }) {
    const prefix = usePrefix('resource');
    const expand = ref(props.defaultExpand);
    const langRef = useBehaviorSubject(
      GlobalRegistry.getDesignerLanguageStore()
    );

    const renderNode = (source: Resource) => {
      const { node, icon, title, thumb, span } = source;
      return (
        langRef.value && (
          <div
            class={`${prefix}-item`}
            style={{ gridColumnStart: `span ${span || 1}` }}
            key={node?.id}
            data-designer-source-id={node?.id}
          >
            {thumb && <img class={`${prefix}-item-thumb`} src={thumb} />}
            {icon && (
              <IconWidget
                class={`${prefix}-item-icon`}
                style={{ width: 150, height: 40 }}
              >
                {isVNode(source.icon) ? (
                  source.icon
                ) : (
                  <img src={source.icon} style={{ width: '60px' }} />
                )}
              </IconWidget>
            )}

            <span class={`${prefix}-item-text`}>
              {/* <TextWidget>
              {title || node?.children.value[0]?.getMessage('title')}
            </TextWidget> */}
              <TextWidget
                token={title || node?.children.value[0]?.getMessage('title')}
              ></TextWidget>
            </span>
          </div>
        )
      );
    };

    const sources = props.sources.reduce<Resource[]>((buf, source) => {
      if (isResourceList(source)) {
        return [...buf, ...source];
      } else if (isResourceHost(source) && source.Resource) {
        return [...buf, ...source.Resource];
      }
      return buf;
    }, []);

    return () => (
      <div
        class={cls(prefix, props.className, {
          expand: expand.value,
        })}
      >
        <div
          class={prefix + '-header'}
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            expand.value = !expand.value;
          }}
        >
          <div class={`${prefix}-header-expand`}>
            <IconWidget>
              <Svg width="10" height="10" viewBox="0 0 20 20">
                <Expand></Expand>
              </Svg>
            </IconWidget>
          </div>
          <div class={`${prefix}-header-content`}>
            <TextWidget token={props.title}></TextWidget>

            {/* <TextWidget token={props.title}></TextWidget> */}
          </div>
        </div>
        <div class={`${prefix}-content-wrapper`}>
          <div class={`${prefix}-content`}>
            {sources.map((source) =>
              isFunction(slots.default)
                ? renderSlot(slots, 'default')
                : renderNode(source)
            )}
          </div>
        </div>
      </div>
    );
  },
});
