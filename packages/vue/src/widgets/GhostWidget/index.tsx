import { CursorStatus } from '@season-designable/core';
import { computed, defineComponent, ref } from 'vue';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { useCursor } from '../../hooks/useCursor';
import { useDesigner } from '../../hooks/useDesigner';
import { usePrefix } from '../../hooks/usePrefix';
import { NodeTitleWidget } from '../NodeTitleWidget';
import './style.scss';
import { isNumber } from 'lodash-es';

export const GhostWidget = defineComponent({
  setup() {
    const containerRef = ref<HTMLDivElement>();
    const prefix = usePrefix('ghost');

    const designer = useDesigner();
    const cursor = useCursor();

    const status = useBehaviorSubject(cursor?.status);
    const position = useBehaviorSubject(cursor?.position);
    const movingNodes = useBehaviorSubject(designer?.findMovingNodes());

    // const firstNode = computed(() =>
    //   movingNodes.value.length !== 0 ? movingNodes.value[0] : null
    // );

    const transform = computed(() => {
      if (
        isNumber(position.value.topClientX) &&
        isNumber(position.value.topClientY)
      ) {
        return `perspective(1px) translate3d(${
          position.value.topClientX - 18
        }px,${position.value.topClientY - 12}px,0) scale(0.8)`;
      }
    });

    // if (!movingNodes.value[0]) return () => null;

    return () =>
      movingNodes.value.length > 0 && status.value === CursorStatus.Dragging ? (
        <div
          ref={containerRef}
          class={prefix}
          style={{ transform: transform.value }}
        >
          <span
            style={{
              whiteSpace: 'nowrap',
            }}
          >
            <NodeTitleWidget node={movingNodes.value[0]} />
          </span>
        </div>
      ) : null;
  },
});
