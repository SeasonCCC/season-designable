import {
  PropType,
  CSSProperties,
  defineComponent,
  ref,
  computed,
  watch,
} from 'vue';
import './style.scss';
import cls from 'classnames';
import { usePrefix } from '../../hooks/usePrefix';
import { useCursor } from '../../hooks/useCursor';
import { useScreen } from '../../hooks/useScreen';
import { useWorkbench } from '../../hooks/useWorkbench';
import { useHistory } from '../../hooks/useHistory';
import { IconWidget } from '../IconWidget';
import { Undo } from '../../icons/Undo';
import { Svg } from '../../icons/Svg';
import { Redo } from '../../icons/Redo';
import { Move } from '../../icons/Move';
import { Selection } from '../../icons/Selection';
import { CursorType, ScreenType } from '@season-designable/core';
import { PC } from '../../icons/PC';
import { Responsive } from '../../icons/Responsive';
import { Mobile } from '../../icons/Mobile';
import { Flip } from '../../icons/Flip';
import { Recover } from '../../icons/Recover';
import { Close } from '../../icons/Close';

export const DesignerToolsWidget = defineComponent({
  props: {
    className: String,
    style: Object as PropType<CSSProperties>,
    use: {
      type: Array as PropType<Array<'HISTORY' | 'CURSOR' | 'SCREEN_TYPE'>>,
      default: ['HISTORY', 'CURSOR', 'SCREEN_TYPE'],
    },
  },
  setup(props) {
    const screen = useScreen();
    const cursor = useCursor();
    const workbench = useWorkbench();
    const history = useHistory();
    const sizeRef = ref<{ width?: number | string; height?: number | string }>(
      {}
    );
    const prefix = usePrefix('designer-tools');

    const renderHistoryController = () => {
      if (!props.use?.includes('HISTORY')) return null;
      return (
        <div class="btn-group" style={{ marginRight: 20 }}>
          <button
            class="btn"
            disabled={!history?.allowUndo}
            onClick={() => {
              history?.undo();
            }}
          >
            <IconWidget>
              <Svg>
                <Undo />
              </Svg>
            </IconWidget>
          </button>
          <button
            class="btn"
            disabled={!history?.allowRedo}
            onClick={() => {
              history?.redo();
            }}
          >
            <IconWidget>
              <Svg>
                <Redo />
              </Svg>
            </IconWidget>
          </button>
        </div>
      );
    };

    const cursorType = ref<string>(CursorType.Normal);
    cursor?.type.subscribe({
      next: (v) => (cursorType.value = v),
    });

    const renderCursorController = () => {
      if (workbench?.type.value !== 'DESIGNABLE') return null;
      if (!props.use.includes('CURSOR')) return null;

      return (
        <div class="btn-group" style={{ marginRight: 20 }}>
          <button
            class="btn"
            disabled={cursorType.value === CursorType.Normal}
            onClick={() => {
              cursor?.setType(CursorType.Normal);
            }}
          >
            <IconWidget>
              <Svg>
                <Move />
              </Svg>
            </IconWidget>
          </button>
          <button
            class="btn"
            disabled={cursorType.value === CursorType.Selection}
            onClick={() => {
              cursor?.setType(CursorType.Selection);
            }}
          >
            <IconWidget>
              <Svg>
                <Selection />
              </Svg>
            </IconWidget>
          </button>
        </div>
      );
    };

    const screenType = ref(screen?.type.value);
    screen?.type.subscribe({
      next: (v) => (screenType.value = v),
    });

    const renderScreenTypeController = () => {
      if (!props.use.includes('SCREEN_TYPE')) return null;

      return (
        <div class="btn-group" style={{ marginRight: 20 }}>
          <button
            class="btn"
            disabled={screenType.value === ScreenType.PC}
            onClick={() => {
              screen?.setType(ScreenType.PC);
            }}
          >
            <IconWidget>
              <Svg viewBox="0 0 1224 1024">
                <PC />
              </Svg>
            </IconWidget>
          </button>
          <button
            class="btn"
            disabled={screenType.value === ScreenType.Mobile}
            onClick={() => {
              screen?.setType(ScreenType.Mobile);
            }}
          >
            <IconWidget>
              <Svg>
                <Mobile />
              </Svg>
            </IconWidget>
          </button>
          <button
            class="btn"
            disabled={screenType.value === ScreenType.Responsive}
            onClick={() => {
              screen?.setType(ScreenType.Responsive);
            }}
          >
            <IconWidget>
              <Svg>
                <Responsive />
              </Svg>
            </IconWidget>
          </button>
        </div>
      );
    };

    const renderResponsiveController = () => {
      if (!props.use.includes('SCREEN_TYPE')) return null;
      if (screenType.value !== ScreenType.Responsive) return null;

      return (
        <>
          <div class="input-number">
            <div class="input-number-input-wrap">
              <input
                autocomplete="off"
                class="input"
                v-model={sizeRef.value.width}
                onKeypress={() => {
                  screen?.setSize(screen.width.value, sizeRef.value.height);
                }}
              />
            </div>
          </div>
          <IconWidget style="padding: 0px 3px; color: rgb(153, 153, 153);">
            <Svg>
              <Close />
            </Svg>
          </IconWidget>
          <div class="input-number">
            <div class="input-number-input-wrap">
              <input
                autocomplete="off"
                class="input"
                v-model={sizeRef.value.height}
                onKeypress={() => {
                  screen?.setSize(sizeRef.value.height, screen.height.value);
                }}
              />
            </div>
          </div>

          {(screen?.width.value !== '100%' ||
            screen.height.value !== '100%') && (
            <button
              class="btn"
              onClick={() => {
                screen?.setType(ScreenType.Responsive);
              }}
            >
              <IconWidget>
                <Svg>
                  <Recover />
                </Svg>
              </IconWidget>
            </button>
          )}
        </>
      );
    };

    const screenFlip = ref(true);
    screen?.flip.subscribe({
      next: (v) => (screenFlip.value = v),
    });

    const renderMobileController = () => {
      if (!props.use.includes('SCREEN_TYPE')) return null;
      if (screenType.value !== ScreenType.Mobile) return;
      return (
        <button
          class="btn"
          onClick={() => {
            screen?.setFlip(!screenFlip.value);
          }}
        >
          <IconWidget
            style={{
              transition: 'all .15s ease-in',
              transform: screenFlip.value ? 'rotate(-90deg)' : '',
            }}
          >
            <Svg>
              <Flip />
            </Svg>
          </IconWidget>
        </button>
      );
    };

    return () => (
      <div style={props.style} class={cls(prefix, props.className)}>
        {renderHistoryController()}
        {renderCursorController()}
        {renderScreenTypeController()}
        {renderResponsiveController()}
        {renderMobileController()}
      </div>
    );
  },
});
