import { TreeNode } from '@season-designable/core';
import { PropType, computed, defineComponent, h, toRaw, watch } from 'vue';
import {
  DesignerComponentsSymbol,
  EngineContext,
  TreeNodeSymbol,
} from '../../context';
import { useBehaviorSubject } from '../../hooks/useBehaviorSubject';
import { useDesigner } from '../../hooks/useDesigner';
import { usePrefix } from '../../hooks/usePrefix';
import { useTree } from '../../hooks/useTree';
import { DesignerComponents } from '../../types';
import './style.scss';

export const TreeNodeWidget = defineComponent({
  props: {
    node: { type: Object as PropType<TreeNode> },
  },
  setup(props) {
    const designer = useDesigner(props.node?.designerProps?.effects);
    const prefix = usePrefix('node');
    const components = EngineContext[DesignerComponentsSymbol];
    const nodeProps = computed(
      () => useBehaviorSubject(props.node?.props).value
    );
    const children = useBehaviorSubject(props.node?.children);

    const renderChildren = () => {
      if (props.node?.designerProps?.selfRenderChildren) return [];
      return children.value?.map((child: TreeNode) => (
        <TreeNodeWidget key={child.id} node={child} />
      ));
    };

    if (!props.node) return () => null;
    if (props.node.hidden) return () => null;

    EngineContext[TreeNodeSymbol] = props.node;

    return () => {
      const componentName = props.node?.componentName || '';
      const Component = components?.[componentName].Component;

      if (props.node && Component) {
        const mergeProps = {
          name: props.node.id,
          ...toRaw(props.node.designerProps?.defaultProps),
          ...toRaw(nodeProps.value),
          ...props.node.designerProps?.getComponentProps?.(props.node),
        };

        if (props.node.isRoot) {
          return (
            <Component key={props.node.id} {...mergeProps}>
              {renderChildren}
            </Component>
          );
        }

        const dataId: Record<string, string> = {};
        if (designer) {
          dataId[designer?.props?.nodeIdAttrName] = props.node.id || '';
        }

        if (props.node.depth === 0) {
          delete mergeProps.style;
        }

        return (
          props.node && (
            <Component
              key={props.node.id}
              {...dataId}
              schema={mergeProps}
              class={prefix}
            >
              {renderChildren}
            </Component>
          )
        );
      }
    };
  },
});

export const ComponentTreeWidget = defineComponent({
  props: { components: Object as PropType<DesignerComponents> },
  setup(props) {
    const { components } = props;
    const designer = useDesigner();
    const tree = useTree();
    const treeProps = useBehaviorSubject(tree?.props);

    const prefix = usePrefix('component-tree');

    const dataId: Record<string, string> = {};

    if (designer && tree) {
      dataId[designer?.props?.nodeIdAttrName] = tree.id;
    }

    // onMounted(() => {
    // if (components) {
    //   GlobalRegistry.registerDesignerBehaviors(components);
    // }
    // });

    EngineContext[DesignerComponentsSymbol] = components;

    return () => (
      <div style={{ ...treeProps.value?.style }} class={prefix} {...dataId}>
        <TreeNodeWidget node={tree} />
      </div>
    );
  },
});
