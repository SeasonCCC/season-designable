import { PropType, defineComponent, renderSlot } from 'vue';
import { useBehaviorSubject } from '../../hooks';
import { usePrefix } from '../../hooks/usePrefix';
import { useTree } from '../../hooks/useTree';
import { BatchDrag } from '../../icons/BatchDrag';
import { Command } from '../../icons/Command';
import { Delete } from '../../icons/Delete';
import { DragLeftSource } from '../../icons/DragLeftSource';
import { Shift } from '../../icons/Shift';
import { Svg } from '../../icons/Svg';
import './style.scss';

export const EmptyWidget = defineComponent({
  props: {
    dragTipsDirection: String as PropType<'left' | 'right'>,
  },
  setup(props, { slots, attrs }) {
    const tree = useTree();
    const prefix = usePrefix('empty');
    const children = useBehaviorSubject(tree?.children);
    // const children$ = tree?.children;
    // const children = ref();
    // children$?.subscribe((val) => {
    //   children.value = val;
    // });

    const renderEmpty = () => {
      return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div class="animations">
            <Svg width="240px" height="240px">
              <DragLeftSource></DragLeftSource>
            </Svg>

            <Svg width="240px" height="240px">
              <BatchDrag></BatchDrag>
            </Svg>
          </div>
          <div class="hotkeys-list">
            <div>
              Selection{' '}
              <Svg>
                <Command />
              </Svg>{' '}
              + Click /{' '}
              <Svg>
                <Shift />
              </Svg>{' '}
              + Click /{' '}
              <Svg>
                <Command />
              </Svg>{' '}
              + A
            </div>
            <div>
              Copy{' '}
              <Svg>
                <Command />
              </Svg>{' '}
              + C / Paste{' '}
              <Svg>
                <Command />
              </Svg>{' '}
              + V
            </div>
            <div>
              Delete{' '}
              <Svg>
                <Delete />
              </Svg>
            </div>
          </div>
        </div>
      );
    };

    return () =>
      !children.value.length && (
        <div class={prefix}>
          {slots.default ? renderSlot(slots, 'default') : renderEmpty()}
        </div>
      );
  },
});
