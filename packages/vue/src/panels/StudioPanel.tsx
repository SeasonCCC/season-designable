import {
  FunctionalComponent,
  PropType,
  StyleValue,
  VNode,
  defineComponent,
  renderSlot
} from 'vue';
import { Layout } from '../containers/Layout';
import { usePosition } from '../hooks/usePosition';
import { usePrefix } from '../hooks/usePrefix';
import { DesignerLayoutProps } from '../types';

const StudioPanelInternal = defineComponent({
  props: {
    style: Object as PropType<StyleValue>,
    className: String,
    logo: Object as PropType<VNode | FunctionalComponent>,
    actions: Object as PropType<VNode | FunctionalComponent>,
    prefixCls: String,
    theme: String,
    position: String as PropType<DesignerLayoutProps['position']>,
  },
  setup(props, { slots, emit }) {
    const prefix = usePrefix('main-panel');
    const position = usePosition();

    let classNameBase = `root ${position}`;

    if (props.className) {
      classNameBase += ' props.className';
    }

    const { logo, actions } = slots;

    if (logo || actions) {
      return () => (
        <div class={`${prefix}-container ${classNameBase}`}>
          <div class={`${prefix}-header`}>
            <div class={`${prefix}-header-logo`}>
              {renderSlot(slots, 'logo')}
            </div>
            <div class={`${prefix}-header-actions`}>
              {' '}
              {renderSlot(slots, 'actions')}
            </div>
          </div>
          <div class={prefix}>{renderSlot(slots, 'default')}</div>
        </div>
      );
    }

    return () => (
      <div class={`${prefix} ${classNameBase}`}>
        {renderSlot(slots, 'default')}
      </div>
    );
  },
});

export const StudioPanel = defineComponent({
  props: {
    prefixCls: {
      type: String,
      default: 'dn-',
    },
    theme: {
      type: String as PropType<DesignerLayoutProps['theme']>,
      default: 'light',
    },

    position: {
      type: String as PropType<DesignerLayoutProps['position']>,
      default: 'fixed',
    },
  },
  setup(props, { slots, emit }) {
    return () => (
      <Layout {...props}>
        <StudioPanelInternal {...props}>
          {{
            default: () => renderSlot(slots, 'default'),
            logo: () => renderSlot(slots, 'logo'),
            actions: () => renderSlot(slots, 'actions'),
          }}
        </StudioPanelInternal>
      </Layout>
    );
  },
});
