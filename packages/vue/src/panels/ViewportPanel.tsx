import {
  CSSProperties,
  PropType,
  defineComponent,
  renderSlot
} from 'vue';
import { Simulator } from '../containers';
import { WorkspacePanelItem } from './WorkspacePanel';

export const ViewportPanel = defineComponent({
  props: {
    style: Object as PropType<CSSProperties>,
    flexable: Boolean,
  },
  setup(props, { slots }) {
    return () => (
      <WorkspacePanelItem {...props}>
        <Simulator>{renderSlot(slots, 'default')}</Simulator>
      </WorkspacePanelItem>
    );
  },
});
