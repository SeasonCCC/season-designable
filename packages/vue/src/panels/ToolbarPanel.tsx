import { CSSProperties, PropType, defineComponent, renderSlot } from 'vue';
import { WorkspacePanelItem } from './WorkspacePanel';

export const ToolbarPanel = defineComponent({
  props: {
    style: Object as PropType<CSSProperties>,
    flexable: Boolean,
  },
  setup(props, { slots }) {
    return () => (
      <WorkspacePanelItem
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: '4px',
          padding: '0 4px',
          ...props.style,
        }}
      >
        {renderSlot(slots, 'default')}
      </WorkspacePanelItem>
    );
  },
});
