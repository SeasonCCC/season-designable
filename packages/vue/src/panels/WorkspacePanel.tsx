import { CSSProperties, PropType, defineComponent, renderSlot } from 'vue';
import { usePrefix } from '../hooks/usePrefix';

export const WorkspacePanel = defineComponent({
  setup(props, { slots }) {
    const prefix = usePrefix('workspace-panel');
    return () => <div class={prefix}>{renderSlot(slots, 'default')}</div>;
  },
});

export const WorkspacePanelItem = defineComponent({
  props: {
    style: Object as PropType<CSSProperties>,
    flexable: Boolean,
  },
  setup(props, { slots }) {
    const prefix = usePrefix('workspace-panel-item');
    return () => (
      <div
        class={prefix}
        style={{
          ...props.style,
          flexGrow: props.flexable ? 1 : 0,
          flexShrink: props.flexable ? 1 : 0,
        }}
      >
        {renderSlot(slots, 'default')}
      </div>
    );
  },
});
