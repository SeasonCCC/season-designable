import { PropType, defineComponent, ref, watch } from 'vue';
import { useWorkbench } from '../hooks/useWorkbench';
import { useTree } from '../hooks/useTree';
import { ITreeNode, TreeNode, WorkbenchTypes } from '@season-designable/core';
import { requestIdle } from '@season-designable/shared';
import { Viewport } from '../containers/Viewport';
import { useBehaviorSubject } from '../hooks';

export const ViewPanel = defineComponent({
  props: {
    type: String as PropType<WorkbenchTypes>,
    scrollable: { type: Boolean, default: true },
    dragTipsDirection: String as PropType<'left' | 'right'>,
  },
  setup(props, { slots, attrs }) {
    const workbench = useWorkbench();
    const workbenchType = useBehaviorSubject(workbench?.type);
    const tree = useTree();
    const visible = ref(false);

    // workbench?.type.subscribe({
    //   next: (v) => {
    //     if (v === props.type) {
    //       requestIdle(() => {
    //         requestAnimationFrame(() => {
    //           visible.value = true;
    //         });
    //       });
    //     } else {
    //       visible.value = false;
    //     }
    //   },
    // });

    const render = () => {
      return slots.default?.(tree, (payload: ITreeNode) => {
        tree?.from(payload);
        tree?.takeSnapshot();
      });
      // return renderSlot(slots, 'default', tree, (payload: TreeNode) => {
      //   tree?.from(payload);
      //   tree?.takeSnapshot();
      // });
    };

    return () => {
      if (workbenchType.value !== props.type) return null;

      if (workbenchType.value === 'DESIGNABLE') {
        return (
          <Viewport dragTipsDirection={props.dragTipsDirection}>
            {render()}
          </Viewport>
        );
      } else {
        return (
          <div
            style={{
              overflow: props.scrollable ? 'overlay' : 'hidden',
              height: '100%',
              cursor: 'auto',
              userSelect: 'text',
            }}
          >
            {render()}
          </div>
        );
      }
    };
  },
});
