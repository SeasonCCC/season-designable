import cls from 'classnames';
import { isArray, isFunction, isString } from 'lodash-es';
import {
  PropType,
  RendererElement,
  RendererNode,
  VNode,
  computed,
  defineComponent,
  ref,
  renderSlot,
  watch,
} from 'vue';
import { usePrefix } from '../hooks/usePrefix';
import { IconWidget, TextWidget } from '../widgets';
import { Close } from '../icons/Close';
import { Svg } from '../icons/Svg';
import { PinFilled } from '../icons/PinFilled';
import { PinOutline } from '../icons/PinOutlined';

export interface CompositePanelProps {
  direction?: 'left' | 'right';
  showNavTitle?: boolean;
  defaultOpen?: boolean;
  defaultPinning?: boolean;
  defaultActiveKey?: number;
  activeKey?: number | string;
  onChange?: (activeKey: number | string) => void;
}

export interface CompositePanelItemProps extends Record<string, any> {
  key: string | number | symbol;
  children: VNode<
    RendererNode,
    RendererElement,
    {
      [key: string]: any;
    }
  >;
}

const parseItems = (children: VNode[]): CompositePanelItemProps[] =>
  children.map((item, index) => ({
    key: item.key ?? index,
    children: item,
    ...item.props,
  }));

const getDefaultKey = (children: VNode[]) => {
  const items = parseItems(children);
  return items[0].key;
};

const renderChildrenSlot = (node: VNode, slotKey = 'default') => {
  const children = node.children;
  if (children && !isArray(children) && !isString(children)) {
    let slot = children[slotKey];
    return isFunction(slot) ? slot() : <></>;
  } else {
    return <></>;
  }
};

export const CompositePanel = defineComponent({
  props: {
    direction: String as PropType<CompositePanelProps['direction']>,
    showNavTitle: Boolean,
    defaultOpen: Boolean,
    defaultPinning: Boolean,
    defaultActiveKey: Number,
    activeKey: [Number, String],
  },
  emits: {
    onChange(payload: { activeKey: number | string }) {
      if (payload.activeKey) {
        return true;
      } else {
        return false;
      }
    },
  },
  setup(props, { slots, emit }) {
    const { defaultActiveKey, defaultPinning, defaultOpen } = props;
    const prefix = usePrefix('composite-panel');
    const pinning = ref(defaultPinning);

    const children = slots['default']?.();

    if (children) {
      const activeKey = ref(defaultActiveKey || getDefaultKey(children));
      const visible = ref<boolean>(defaultOpen || true);

      const items = parseItems(children);

      const currentItem = computed(() =>
        items.find(
          (item, index) =>
            item.key === activeKey.value || index === activeKey.value
        )
      );

      watch(
        () => props.activeKey,
        () => {
          if (props.activeKey !== null && props.activeKey !== undefined) {
            if (props.activeKey !== activeKey.value) {
              activeKey.value = props.activeKey;
            }
          }
        },
        { immediate: true }
      );

      const renderContent = computed(() => {
        const content = currentItem.value?.children;
        if (!content || !visible.value) return;

        return (
          <div
            class={cls(prefix + '-tabs-content', {
              pinning: pinning.value,
            })}
          >
            <div class={`${prefix}-tabs-header`}>
              <div class={`${prefix}-tabs-header-title`}>
                <TextWidget>{currentItem.value?.title}</TextWidget>
              </div>
              <div class={`${prefix}-tabs-header-actions`}>
                <div class={`${prefix}-tabs-header-extra`}>
                  {currentItem.value?.extra}
                </div>

                {!pinning.value && (
                  <IconWidget
                    class={`${prefix}-tabs-header-pin`}
                    onClick={() => {
                      pinning.value = !pinning.value;
                    }}
                  >
                    <Svg>
                      <PinOutline />
                    </Svg>
                  </IconWidget>
                )}

                {pinning.value && (
                  <IconWidget
                    class={`${prefix}-tabs-header-pin-filled`}
                    onClick={() => {
                      pinning.value = !pinning.value;
                    }}
                  >
                    <Svg>
                      <PinFilled />
                    </Svg>
                  </IconWidget>
                )}

                <IconWidget
                  class={`${prefix}-tabs-header-close`}
                  onClick={() => {
                    visible.value = false;
                  }}
                >
                  <Svg>
                    <Close />
                  </Svg>
                </IconWidget>
              </div>
            </div>
            <div class={`${prefix}-tabs-body`}>
              {renderChildrenSlot(content)}
            </div>
          </div>
        );
      });

      return () => (
        <div
          class={cls(prefix, {
            [`direction-${props.direction}`]: !!props.direction,
          })}
        >
          <div class={`${prefix}-tabs`}>
            {items.map((item, index) => {
              const shape = item.shape ?? 'tab';
              const Comp = shape === 'link' ? 'a' : 'div';

              return (
                <Comp
                  class={cls(prefix + '-tabs-pane', {
                    active: activeKey.value === item.key,
                  })}
                  key={index}
                  href={item.href}
                  onClick={(e: any) => {
                    if (shape === 'tab') {
                      if (activeKey.value === item.key) {
                        visible.value = !visible.value;
                      } else {
                        visible.value = true;
                      }

                      if (!item.activeKey || !item.onChange)
                        activeKey.value = item.key;
                    }
                    item.onClick?.(e);
                    item.onChange?.(item.key);
                  }}
                >
                  {renderChildrenSlot(item.children, 'icon')}
                </Comp>
              );
            })}
          </div>
          {renderContent.value}
        </div>
      );
    }
  },
});

export const CompositePanelItem = defineComponent({
  name: 'CompositePanel.Item',
  props: ['shape', 'title', 'icon', 'href', 'extra', 'onClick'],
  setup(props, { slots }) {
    return () => ({
      default: () => renderSlot(slots, 'default'),
      icon: () => renderSlot(slots, 'icon'),
    });
  },
});
