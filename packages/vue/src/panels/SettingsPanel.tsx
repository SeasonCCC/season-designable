import { PropType, defineComponent, ref, renderSlot } from 'vue';
import { useWorkbench } from '../hooks/useWorkbench';
import cls from 'classnames';
import { useTree } from '../hooks/useTree';
import { ITreeNode, TreeNode, WorkbenchTypes } from '@season-designable/core';
import { requestIdle } from '@season-designable/shared';
import { Viewport } from '../containers/Viewport';
import { usePrefix } from '../hooks';
import { IconWidget, TextWidget } from '../widgets';
import { Svg } from '../icons/Svg';
import { PinFilled } from '../icons/PinFilled';
import { PinOutline } from '../icons/PinOutlined';
import { Close } from '../icons/Close';
import { useBehaviorSubject } from '../hooks/useBehaviorSubject';

export const SettingsPanel = defineComponent({
  props: {
    title: String,
    extra: String,
  },
  setup(props, { slots, attrs }) {
    const prefix = usePrefix('settings-panel');
    const workbench = useWorkbench();
    const tree = useTree();
    const innerVisible = ref(true);
    const pinning = ref(false);
    const visible = ref(true);
    const type = useBehaviorSubject(workbench?.type);

    return () => {
      if (visible.value && type.value === 'DESIGNABLE') {
        return (
          <div class={cls(prefix, { pinning: pinning.value })}>
            <div class={prefix + '-header'}>
              <div class={prefix + '-header-title'}>
                <TextWidget>{props.title}</TextWidget>
              </div>
              <div class={prefix + '-header-actions'}>
                <div class={prefix + '-header-extra'}>{props.extra}</div>
                {!pinning.value && (
                  <IconWidget
                    class={`${prefix}-header-pin`}
                    onClick={() => {
                      pinning.value = !pinning.value;
                    }}
                  >
                    <Svg>
                      <PinOutline />
                    </Svg>
                  </IconWidget>
                )}

                {pinning.value && (
                  <IconWidget
                    class={`${prefix}-header-pin-filled`}
                    onClick={() => {
                      pinning.value = !pinning.value;
                    }}
                  >
                    <Svg>
                      <PinFilled />
                    </Svg>
                  </IconWidget>
                )}

                <IconWidget
                  class={`${prefix}-header-close`}
                  onClick={() => {
                    visible.value = false;
                  }}
                >
                  <Svg>
                    <Close />
                  </Svg>
                </IconWidget>
              </div>
            </div>
            <div class={prefix + '-body'}>
              {innerVisible && renderSlot(slots, 'default')}
            </div>
          </div>
        );
      } else {
        return null;
      }
    };
  },
});
