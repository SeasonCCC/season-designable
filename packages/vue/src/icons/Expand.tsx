import { defineComponent } from 'vue';

export const Expand = defineComponent({
  setup() {
    return () => (
      <>
        <path
          fill="currentColor"
          d="m17.5 4.75l-7.5 7.5l-7.5-7.5L1 6.25l9 9l9-9z"
        />
      </>
    );
  },
});
