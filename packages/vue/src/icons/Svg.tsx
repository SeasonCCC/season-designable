import { defineComponent, renderSlot } from 'vue';

export const Svg = defineComponent({
  props: {
    width: { type: String, default: '1em' },
    height: { type: String, default: '1em' },
    viewBox: { type: String, default: '0 0 1024 1024' },
    fill: { type: String, default: 'currentColor' },
  },
  setup(props, { slots }) {
    return () => (
      <svg xmlns="http://www.w3.org/2000/svg" {...props}>
        {renderSlot(slots, 'default')}
      </svg>
    );
  },
});
