import { GlobalRegistry } from '@season-designable/core';
import icons from './icons';
import panels from './panels';
import global from './global';
import operations from './operations';

GlobalRegistry.setDesignerLocales(icons, panels, global, operations);
