import { Engine, TreeNode } from '@season-designable/core';
import {
  WorkspaceContext,
  DesignerLayoutContext,
  DesignerComponents,
} from './types';
import { IconProviderProps } from './widgets';

export const DesignerComponentsSymbol = Symbol('DesignerComponentsSymbol');

export const DesignerLayoutSymbol = Symbol('DesignerLayoutSymbol');

export const TreeNodeSymbol = Symbol('TreeNodeSymbol');

export const WorkspaceSymbol = Symbol('WorkspaceSymbol');

export const DesignerEngineSymbol = Symbol('DesignerEngineSymbol');

export const IconSymbol = Symbol('IconSymbol');

type EngineContextType = {
  [DesignerComponentsSymbol]?: DesignerComponents;
  [DesignerEngineSymbol]?: Engine;
  [DesignerLayoutSymbol]?: DesignerLayoutContext;
  [WorkspaceSymbol]?: WorkspaceContext;
  [IconSymbol]?: IconProviderProps;
  [TreeNodeSymbol]?: TreeNode;
};

export const EngineContext: EngineContextType = {};
