import { BehaviorSubject } from 'rxjs';
import { ref, Ref } from 'vue';

export const useBehaviorSubject = <T>(
  behSubject?: BehaviorSubject<T>,
  subscribe?: (val: T) => void
): Ref<T> => {
  const refValue = ref();

  if (behSubject) {
    behSubject.subscribe((val) => {
      if (subscribe) {
        subscribe(val);
      }
      refValue.value = val;
    });
  }

  return refValue;
};
