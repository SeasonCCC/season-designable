import {
  DesignerComponentsSymbol,
  EngineContext,
} from '../context';

export const useComponents = () => EngineContext[DesignerComponentsSymbol];
