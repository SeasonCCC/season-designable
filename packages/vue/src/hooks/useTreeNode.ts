import { EngineContext, TreeNodeSymbol } from '../context';

export const useTreeNode = () => EngineContext[TreeNodeSymbol];
