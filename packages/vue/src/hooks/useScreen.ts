import { useDesigner } from './useDesigner';

export const useScreen = () => {
  const designer = useDesigner();
  return designer?.screen;
};
