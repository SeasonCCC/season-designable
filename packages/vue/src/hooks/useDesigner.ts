import { Engine } from '@season-designable/core';
import { globalThisPolyfill } from '@season-designable/shared';
import { get, isFunction } from 'lodash-es';
import { onBeforeUnmount, shallowReactive } from 'vue';
import { DesignerEngineSymbol, EngineContext } from '../context';

export const useDesigner = (effects?: (engine: Engine) => void) => {
  let designer =
    get(globalThisPolyfill, '__DESIGNABLE_ENGINE__') ||
    EngineContext[DesignerEngineSymbol];

  // let unRef: any =
  //   isFunction(effects) && designer ? effects(designer) : undefined;

  // onBeforeUnmount(() => {
  //   unRef?.();
  // });

  if (designer) {
    return designer;
  }
};
