import { GlobalRegistry, DesignerRegistry } from '@season-designable/core';
import { globalThisPolyfill } from '@season-designable/shared';

export const useRegistry = (): DesignerRegistry =>
  Reflect.get(globalThisPolyfill, '__DESIGNER_REGISTRY__') || GlobalRegistry;
