import { useDesigner } from './useDesigner';
import { globalThisPolyfill } from '@season-designable/shared';
import { EngineContext, WorkspaceSymbol } from '../context';
import { get } from 'lodash-es';
import { shallowReactive } from 'vue';

export const useWorkspace = (id?: string) => {
  const designer = useDesigner();
  if (designer) {
    const workspaceId = id || EngineContext[WorkspaceSymbol]?.id;

    if (workspaceId) {
      const workspace = designer.workbench.findWorkspaceById(workspaceId);
      if (workspace) {
        return shallowReactive(workspace);
      }
    }

    if (get(globalThisPolyfill, '__DESIGNABLE_WORKSPACE__')) {
      return get(globalThisPolyfill, '__DESIGNABLE_WORKSPACE__');
    }

    const currentWorkspace = designer.workbench.currentWorkspace;

    if (currentWorkspace) {
      return shallowReactive(currentWorkspace);
    }
  }
};
