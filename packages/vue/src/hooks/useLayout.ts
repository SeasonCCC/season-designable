import { DesignerLayoutSymbol, EngineContext } from '../context';
import { globalThisPolyfill } from '@season-designable/shared';
import { get } from 'lodash-es';

export const useLayout = () => {
  const layout =
    get(globalThisPolyfill, '__DESIGNABLE_LAYOUT__') ||
    EngineContext[DesignerLayoutSymbol];
  return layout;
};
