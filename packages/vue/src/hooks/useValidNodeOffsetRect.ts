import { TreeNode } from '@season-designable/core';
import { useDesigner } from './useDesigner';
import { useViewport } from './useViewport';

export const useValidNodeOffsetRect = (node: TreeNode) => {
  const engine = useDesigner();
  const viewport = useViewport();
  const rectRef = viewport?.getValidNodeOffsetRect(node);
  const element = viewport?.findElementById(node?.id);

  return rectRef;
};
