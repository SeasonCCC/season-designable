import { isObject, isString } from 'lodash-es';
import { CSSProperties } from 'vue';

const css2obj = (css: string) => {
  const r = /(?<=^|;)\s*([^:]+)\s*:\s*([^;]+)\s*/g;
  let o: CSSProperties = {};
  css.replace(r, (m, p, v) => (o[p] = v));
  return o;
};

export const useStyle = (style: unknown) => {
  if (isObject(style)) {
    return style;
  } else if (isString(style)) {
    return css2obj(style);
  }
};
