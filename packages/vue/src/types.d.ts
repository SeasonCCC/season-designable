export interface DesignerLayoutProps {
  prefixCls?: string;
  theme?: 'dark' | 'light' | (string & {});
  variables?: Record<string, string>;
  position?: 'fixed' | 'absolute' | 'relative';
}
export interface DesignerProps extends DesignerLayoutProps {
  engine: Engine;
}

export interface WorkspaceContext {
  id: string;
  title?: string;
  description?: string;
}

export interface DesignerLayoutContext {
  theme?: 'dark' | 'light' | string;
  prefixCls: string;
  position: 'fixed' | 'absolute' | 'relative';
}

export type DesignerComponents = {
  [key: string]: {
    Component?: DefineComponent<any>;
    Resource?: IResource[];
    Behavior?: IBehavior[];
  };
};
