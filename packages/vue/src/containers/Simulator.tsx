import { ScreenType } from '@season-designable/core';
import { defineComponent, renderSlot } from 'vue';
import { useScreen } from '../hooks/useScreen';
import {
  MobileSimulator,
  PCSimulator,
  ResponsiveSimulator,
} from '../simulators';

export const Simulator = defineComponent({
  setup(props, { slots, attrs }) {
    const screen = useScreen();
    if (screen?.type.value === ScreenType.PC) {
      return () => (
        <PCSimulator {...props} {...attrs}>
          {renderSlot(slots, 'default')}
        </PCSimulator>
      );
    }

    if (screen?.type.value === ScreenType.Mobile) {
      return () => (
        <MobileSimulator {...props} {...attrs}>
          {renderSlot(slots, 'default')}
        </MobileSimulator>
      );
    }

    if (screen?.type.value === ScreenType.Responsive) {
      return () => (
        <ResponsiveSimulator {...props} {...attrs}>
          {renderSlot(slots, 'default')}
        </ResponsiveSimulator>
      );
    }

    return () => (
      <PCSimulator {...props} {...attrs}>
        {renderSlot(slots, 'default')}
      </PCSimulator>
    );
  },
});
