import {
  defineComponent,
  watch,
  onMounted,
  PropType,
  ref,
  renderSlot,
} from 'vue';
import { DesignerLayoutProps } from '../types';
import { EngineContext, DesignerLayoutSymbol } from '../context';

export const Layout = defineComponent({
  name: 'Layout',
  props: {
    prefixCls: {
      type: String,
      default: 'dn-',
    },
    theme: {
      type: String as PropType<DesignerLayoutProps['theme']>,
      default: 'light',
    },
    variables: {
      type: Object as PropType<DesignerLayoutProps['variables']>,
      default: {},
    },
    position: {
      type: String as PropType<DesignerLayoutProps['position']>,
      default: 'fixed',
    },
  },
  setup(props, { slots, emit }) {
    const containerRef = ref<HTMLDivElement | null>(null);

    watch(containerRef, () => {
      if (containerRef.value && props.variables) {
        Object.entries(props.variables).forEach(([value, key]) => {
          containerRef.value?.style?.setProperty(`--${key}`, value)
        })
      }
    })

    const layoutContext = EngineContext[DesignerLayoutSymbol];

    if (layoutContext) {
      return () => renderSlot(slots, 'default')
    }

    EngineContext[DesignerLayoutSymbol] = {
      theme: props.theme,
      prefixCls: props.prefixCls,
      position: props.position || 'relative'
    }

    return () => (
      <div
        ref={containerRef}
        class={{
          [`${props.prefixCls}app`]: true,
          [`${props.prefixCls}${props.theme}`]: props.theme,
        }}
      >
        {renderSlot(slots, 'default')}
      </div>
    );
  },
});
