import {
  PropType,
  defineComponent,
  onBeforeUnmount,
  onMounted,
  onUnmounted,
  ref,
  renderSlot,
} from 'vue';
import { usePrefix } from '../hooks/usePrefix';
import { useViewport } from '../hooks/useViewport';
import { Viewport as ViewportType } from '@season-designable/core';
import { useStyle } from '../shared/style';
import { EmptyWidget } from '../widgets/EmptyWidget';
import { AuxWidget } from '../widgets/AuxToolWidget';
import { globalThisPolyfill, requestIdle } from '@season-designable/shared';

export const Viewport = defineComponent({
  props: {
    dragTipsDirection: String as PropType<'left' | 'right'>,
  },
  setup(props, { slots, attrs }) {
    const prefix = usePrefix('viewport');
    const viewport = useViewport();
    const loaded = ref(false);
    const isFrameRef = ref(false);
    const viewportRef = ref<ViewportType>();
    const elementRef = ref<HTMLDivElement | null>(null);
    const { style } = attrs;

    onMounted(() => {
      const frameElement = elementRef.value?.querySelector('iframe');

      if (frameElement) {
      } else {
        if (elementRef.value) {
          viewport?.onMount(elementRef.value, globalThisPolyfill);

          requestIdle(() => {
            isFrameRef.value = false;
            loaded.value = true;
          });
        }
      }

      viewportRef.value = viewport;
    });

    onUnmounted(() => {
      viewport?.onUnmount();
    });

    return () => (
      <div
        {...props}
        ref={elementRef}
        class={prefix}
        style={{
          opacity: !loaded.value ? 0 : 1,
          overflow: isFrameRef.value ? 'hidden' : 'overlay',
          ...useStyle(style),
        }}
      >
        {renderSlot(slots, 'default')}
        <AuxWidget></AuxWidget>
        <EmptyWidget></EmptyWidget>
      </div>
    );
  },
});
