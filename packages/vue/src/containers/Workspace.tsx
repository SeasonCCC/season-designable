import { computed, defineComponent, ref, renderSlot } from 'vue';
import { EngineContext, WorkspaceSymbol } from '../context';
import { useDesigner } from '../hooks/useDesigner';

export const Workspace = defineComponent({
  name: 'Workspace',
  props: {
    id: String,
    title: String,
    description: String,
  },
  setup(props, { slots, emit }) {
    const oldId = ref<string>();
    const designer = useDesigner();

    const workspace = computed(() => {
      if (!designer) return;
      if (oldId.value && oldId.value !== props.id) {
        const old = designer.workbench.findWorkspaceById(oldId.value);
        if (old) old.viewport.detachEvents();
      }

      const workspace = {
        id: props.id || 'index',
        title: props.title || '',
        description: props.description || '',
      };

      designer.workbench.ensureWorkspace(workspace);
      oldId.value = workspace.id;
      return workspace;
    });

    if (workspace.value) {
      EngineContext[WorkspaceSymbol] = workspace.value;
    }

    return () => renderSlot(slots, 'default');
  },
});
