import { Engine } from '@season-designable/core';
import { defineComponent, PropType, renderSlot } from 'vue';
import { useWorkbench } from '../hooks/useWorkbench';
import { DesignerLayoutProps } from '../types';
import { Workspace } from './Workspace';

export const Workbench = defineComponent({
  name: 'Workbench',
  props: {
    engine: {
      type: Object as PropType<Engine>,
    },
    prefixCls: {
      type: String,
      default: 'dn-',
    },
    theme: {
      type: String as PropType<DesignerLayoutProps['theme']>,
      default: 'light',
    },
    variables: {
      type: Object as PropType<DesignerLayoutProps['variables']>,
      default: {},
    },
    position: {
      type: String as PropType<DesignerLayoutProps['position']>,
      default: 'fixed',
    },
  },
  setup(props, { slots, emit }) {
    const workbench = useWorkbench();

    return () => (
      <Workspace id={workbench?.currentWorkspace?.id}>
        {renderSlot(slots, 'default')}
      </Workspace>
    );
  },
});
