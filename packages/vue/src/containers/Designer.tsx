import { Engine } from '@season-designable/core';
import {
  defineComponent,
  onMounted,
  PropType,
  ref,
  renderSlot,
  watch,
  watchEffect,
} from 'vue';
import { DesignerEngineSymbol, EngineContext } from '../context';
import { useDesigner } from '../hooks/useDesigner';
import { DesignerLayoutProps } from '../types';
import { GhostWidget } from '../widgets/GhostWidget';
import { Layout } from './Layout';

export const Designer = defineComponent({
  name: 'Designer',
  props: {
    engine: {
      type: Object as PropType<Engine>,
    },
    prefixCls: {
      type: String,
      default: 'dn-',
    },
    theme: {
      type: String as PropType<DesignerLayoutProps['theme']>,
      default: 'light',
    },
    variables: {
      type: Object as PropType<DesignerLayoutProps['variables']>,
      default: {},
    },
    position: {
      type: String as PropType<DesignerLayoutProps['position']>,
      default: 'fixed',
    },
  },
  setup(props, { slots, emit }) {
    const engine = useDesigner();
    const refInstance = ref<Engine | null>(null);

    if (props.engine) {
      EngineContext[DesignerEngineSymbol] = props.engine;
    }

    if (engine) {
      throw new Error('There can only be one Designable Engine Context');
    }

    watchEffect(() => {
      if (props.engine) {
        props.engine.mount();
      }
    });

    const { prefixCls, theme, variables, position } = props;

    return () => (
      <Layout {...{ prefixCls, theme, variables, position }}>
        {renderSlot(slots, 'default')}
        <GhostWidget />
      </Layout>
    );
  },
});
