import { PropType, defineComponent, renderSlot } from 'vue';
import cls from 'classnames';
import './style.scss';
import { usePrefix } from '../../hooks/usePrefix';

export const PCSimulator = defineComponent({
  props: {
    className: Object as PropType<Record<string, string>>,
  },
  setup(props, { slots, attrs }) {
    const prefix = usePrefix('pc-simulator');

    return () => (
      <div {...attrs} class={cls(prefix, props.className)}>
        {renderSlot(slots, 'default')}
      </div>
    );
  },
});
