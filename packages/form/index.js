'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./dist/form.cjs.prod.js')
} else {
  module.exports = require('./dist/form.cjs.js')
}
