import { Form } from './models';
import { FormProps } from './types';

export const createForm = <T extends object = any>(options?: FormProps<T>) => {
  const form = new Form(options);
  return form;
};
