import { getUuid } from '@season-designable/shared';
import { assign, isObject, merge } from 'lodash-es';
import { BehaviorSubject, combineLatest, isEmpty } from 'rxjs';
import { FieldFactoryProps, FormMergeStrategy, FormProps } from '../types';

export class Form<ValueType extends object = any> {
  displayName = 'Form';
  id: string;
  initialized$ = new BehaviorSubject(false);
  validating$ = new BehaviorSubject(false);
  submitting$ = new BehaviorSubject(false);
  loading$ = new BehaviorSubject(false);
  modified$ = new BehaviorSubject(false);
  mounted$ = new BehaviorSubject(false);
  unmounted$ = new BehaviorSubject(false);
  pattern$ = new BehaviorSubject('');
  display$ = new BehaviorSubject('');
  values: Partial<ValueType>;
  initialValues: Partial<ValueType>;
  props: FormProps<ValueType>;
  // heart: Heart;
  // graph: Graph;
  // fields: IFormFields = {};
  // requests: IFormRequests = {};
  indexes: Record<string, string> = {};
  disposers: (() => void)[] = [];

  constructor(props: FormProps<ValueType> = {}) {
    this.id = getUuid();

    this.props = { ...props };
    this.pattern$.next(this.props.pattern || 'editable');
    this.display$.next(this.props.display || 'visible');

    this.values = props.values || {};
    this.initialValues = props.initialValues || {};

    this.makeReactive();
    this.onInit();
  }

  protected makeReactive() {
    const subjects: BehaviorSubject<any>[] = Object.values(this).filter(
      (value) => {
        return value instanceof BehaviorSubject;
      }
    );

    const merged = combineLatest(subjects);
    // merged.subscribe((x) => {

    // });
  }

  // get valid() {
  //   return !this.invalid.value;
  // }

  // get invalid() {
  //   return this.errors.length > 0;
  // }

  onInit = () => {
    this.initialized$.next(true);
  };

  onMount = () => {
    this.mounted$.next(true);
  };

  setValues = (values: any, strategy: FormMergeStrategy = 'merge') => {
    if (!isObject(values)) return;
    if (strategy === 'merge') {
      merge(this.values, values);
    } else if (strategy === 'assign') {
      assign(this.values, values);
    } else if (strategy === 'overwrite') {
      this.values = values;
    }
  };

  createObjectField<Decorator, Component>(
    props: FieldFactoryProps<Decorator, Component>
  ) {
    // console.log(props);
  }
}
