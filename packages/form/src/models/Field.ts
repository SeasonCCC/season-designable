import { FieldProps, FormPathPattern } from '../types';
import { BaseField } from './BaseField';
import { Form } from './Form';

export class Field<
  Decorator = any,
  Component = any,
  TextType = any,
  ValueType = any
> extends BaseField {
  displayName = 'Field';
  props: FieldProps<Decorator, Component, TextType, ValueType>;

  constructor(
    address: FormPathPattern,
    props: FieldProps<Decorator, Component, TextType, ValueType>,
    form: Form,
    designable: boolean
  ) {
    super();
    this.props = props;
  }
}
