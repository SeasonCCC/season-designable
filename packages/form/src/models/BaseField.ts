import { FormPath } from '@season-designable/path';
import { DisplayTypes, PatternTypes } from '../types';
import { Form } from './Form';

export class BaseField<Decorator = any, Component = any, TextType = any> {
  title: TextType;
  description: TextType;
  selfDisplay: DisplayTypes;
  selfPattern: PatternTypes;
  initialized: boolean;
  mounted: boolean;
  unmounted: boolean;
  content: any;
  data: any;
  decoratorType: Decorator;
  decoratorProps: Record<string, any>;
  componentType: Component;
  componentProps: Record<string, any>;

  designable: boolean;
  address: FormPath;
  path: FormPath;
  form: Form;
}
