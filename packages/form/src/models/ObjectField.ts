import { Field } from './Field';

export class ObjectField<Decorator = any, Component = any> extends Field<
  Decorator,
  Component,
  any,
  Record<string, any>
> {}
