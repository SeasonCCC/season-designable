import { Form } from './models';
import { ArrayField } from './models/ArrayField';
import { Field } from './models/Field';
import { ObjectField } from './models/ObjectField';
import { VoidField } from './models/VoidField';

export type NonFunctionPropertyNames<P> = {
  [K in keyof P]: P[K] extends (...args: any) => any ? never : K;
}[keyof P];

export type FormMergeStrategy = 'overwrite' | 'merge' | 'assign';

export type PatternTypes = 'editable' | 'readOnly' | 'disabled' | 'readPretty';

export type DisplayTypes = 'none' | 'hidden' | 'visible';

export interface FormProps<T extends object = any> {
  values?: Partial<T>;
  initialValues?: Partial<T>;
  pattern?: PatternTypes;
  display?: DisplayTypes;
  hidden?: boolean;
  visible?: boolean;
  editable?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  readPretty?: boolean;
  validateFirst?: boolean;
  designable?: boolean;
  effects?: (form: Form<T>) => void;
}

export type FieldValidator = any;

export type FieldComponent<Component = any, ComponentProps = any> =
  | [Component]
  | [Component, ComponentProps]
  | boolean
  | any[];

export type FieldDecorator<Decorator = any, ComponentProps = any> =
  | [Decorator]
  | [Decorator, ComponentProps]
  | boolean
  | any[];

export type FieldDataSource = {
  label?: any;
  value?: any;
  title?: any;
  key?: any;
  text?: any;
  children?: FieldDataSource;
  [key: string]: any;
}[];

export type FieldReaction = (field: Field) => void;

export interface FieldFactoryProps<
  Decorator = any,
  Component = any,
  TextType = any,
  ValueType = any
> extends FieldProps<Decorator, Component, TextType, ValueType> {
  name: FormPathPattern;
  basePath?: FormPathPattern;
}

export interface FieldProps<
  Decorator = any,
  Component = any,
  TextType = any,
  ValueType = any
> {
  name: FormPathPattern;
  basePath?: FormPathPattern;
  title?: TextType;
  description?: TextType;
  value?: ValueType;
  initialValue?: ValueType;
  required?: boolean;
  display?: DisplayTypes;
  pattern?: PatternTypes;
  hidden?: boolean;
  visible?: boolean;
  editable?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  readPretty?: boolean;
  dataSource?: FieldDataSource;
  validateFirst?: boolean;
  validator?: FieldValidator;
  decorator?: FieldDecorator<Decorator>;
  component?: FieldComponent<Component>;
  reactions?: FieldReaction[] | FieldReaction;
  content?: any;
  data?: any;
}

export type GeneralField = Field | VoidField | ArrayField | ObjectField;

type OmitState<P> = Omit<
  P,
  | 'selfDisplay'
  | 'selfPattern'
  | 'originValues'
  | 'originInitialValues'
  | 'id'
  | 'address'
  | 'path'
  | 'lifecycles'
  | 'disposers'
  | 'requests'
  | 'fields'
  | 'graph'
  | 'heart'
  | 'indexes'
  | 'props'
  | 'displayName'
  | 'setState'
  | 'getState'
  | 'getFormGraph'
  | 'setFormGraph'
  | 'setFormState'
  | 'getFormState'
>;

export type FieldState = Partial<
  Pick<
    Field,
    NonFunctionPropertyNames<OmitState<Field<any, any, string, string>>>
  >
>;

export type VoidFieldState = Partial<
  Pick<
    VoidField,
    NonFunctionPropertyNames<OmitState<VoidField<any, any, string>>>
  >
>;

export type GeneralFieldState = FieldState & VoidFieldState;

export type FormPathPattern =
  | string
  | number
  | Array<string | number>
  // | FormPath
  | RegExp;
// | (((address: Array<string | number>) => boolean) & {
//     path: FormPath;
//   });
