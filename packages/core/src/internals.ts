import { globalThisPolyfill, isPlainObj } from '@season-designable/shared';
import { each, get, isObject, set } from 'lodash-es';

export const lowerSnake = (str: string) => {
  return String(str).replace(/\s+/g, '_').toLocaleLowerCase();
};

export const mergeLocales = (target: any, source: any) => {
  if (isPlainObj(target) && isPlainObj(source)) {
    each(source, (value, key) => {
      const token = lowerSnake(key);
      const messages = mergeLocales(
        get(target, key) || get(target, token),
        value
      );
      set(target, token, messages);
    });
    return target;
  } else if (isPlainObj(source)) {
    const result: Record<string, any> = {};
    each(source, (value, key) => {
      const messages = mergeLocales(undefined, value);
      result[lowerSnake(key)] = messages;
    });
    return result;
  }
  return source;
};

export const getBrowserLanguage = () => {
  if (!globalThisPolyfill.navigator) {
    return 'en';
  }
  return (
    get(globalThisPolyfill.navigator, 'browserlanguage') ||
    globalThisPolyfill.navigator?.language ||
    'en'
  );
};
