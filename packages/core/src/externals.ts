import { Engine, EngineProps, TreeNode } from './models';
import { DEFAULT_DRIVERS, DEFAULT_EFFECTS, DEFAULT_SHORTCUTS } from './presets';
import { every, isArray, isString } from 'lodash-es';
import {
  Behavior,
  BehaviorCreator,
  BehaviorHost,
  DesignerLocales,
  Resource,
  ResourceCreator,
  ResourceHost,
} from './types';
import { mergeLocales } from './internals';

export const isBehaviorHost = (val: any): val is BehaviorHost =>
  val?.Behavior && isBehaviorList(val.Behavior);

export const isBehaviorList = (val: any): val is Behavior[] =>
  isArray(val) && every(val, isBehavior);

export const isBehavior = (val: any): val is Behavior =>
  val.name || val.selector;

export const isResourceHost = (val: any): val is ResourceHost =>
  val?.Resource && isResourceList(val.Resource);

export const isResourceList = (val: any): val is Resource[] =>
  Array.isArray(val) && every(val, isResource);

export const isResource = (val: any): val is Resource =>
  val?.node && !!val.node.isSourceNode && val.node instanceof TreeNode;

export const createDesigner = (props: Partial<EngineProps<Engine>> = {}) => {
  const drivers = props.drivers || [];
  const effects = props.effects || [];
  const shortcuts = props.shortcuts || [];

  return new Engine({
    ...props,
    effects: [...effects, ...DEFAULT_EFFECTS],
    drivers: [...drivers, ...DEFAULT_DRIVERS],
    shortcuts: [...shortcuts, ...DEFAULT_SHORTCUTS],
  });
};

export const createBehavior = (
  ...behaviors: Array<BehaviorCreator | BehaviorCreator[]>
): Behavior[] =>
  behaviors.reduce<Behavior[]>((buf, behavior) => {
    if (isArray(behavior)) {
      return buf.concat(createBehavior(...behavior));
    }

    let { selector } = behavior || {};
    if (!selector) return buf;

    if (isString(selector)) {
      let name = selector;
      selector = (node) => node.componentName === name;
    }

    return buf.concat({ ...behavior, ...{ selector } });
  }, []);

export const createResource = (...sources: ResourceCreator[]): Resource[] =>
  sources.reduce<Resource[]>(
    (buf, source) =>
      buf.concat({
        ...source,
        node: new TreeNode({
          componentName: '$$ResourceNode$$',
          isSourceNode: true,
          children: source.elements || [],
        }),
      }),
    []
  );

export const createLocales = (...packages: DesignerLocales[]) => {
  return packages.reduce((buf, pkg) => mergeLocales(buf, pkg), {});
};
