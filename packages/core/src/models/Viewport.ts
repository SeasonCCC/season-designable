import {
  IPoint,
  IRect,
  Rect,
  calcBoundingRect,
  calcElementLayout,
  cancelIdle,
  globalThisPolyfill,
  isHTMLElement,
  isPointInRect,
  requestIdle,
} from '@season-designable/shared';
import { Engine } from './Engine';
import { TreeNode } from './TreeNode';
import { Workspace } from './Workspace';
import { BehaviorSubject } from 'rxjs';

export interface ViewportProps {
  engine: Engine;
  workspace: Workspace;
  viewportElement?: HTMLElement;
  contentWindow?: Window;
  nodeIdAttrName: string;
  moveSensitive?: boolean;
  moveInsertionType?: ViewportMoveInsertion;
}

export interface ViewportData {
  scrollX?: number;
  scrollY?: number;
  width?: number;
  height?: number;
}

export type ViewportMoveInsertion = 'all' | 'inline' | 'block';

export class Viewport {
  workspace: Workspace;

  engine: Engine;

  viewportElement = new BehaviorSubject<HTMLElement | null>(null);

  contentWindow = new BehaviorSubject<Window | null>(null);

  dragStartSnapshot!: ViewportData;

  scrollX = new BehaviorSubject(0);

  scrollY = new BehaviorSubject(0);

  width = new BehaviorSubject(0);

  height = new BehaviorSubject(0);

  mounted = false;

  attachRequest!: number;

  nodeIdAttrName: string;

  moveSensitive: boolean;

  moveInsertionType: ViewportMoveInsertion;

  nodeElementsStore: Record<string, Element[]> = {};

  constructor(props: ViewportProps) {
    this.workspace = props.workspace;
    this.engine = props.engine;
    this.moveSensitive = props.moveSensitive || false;
    this.moveInsertionType = props.moveInsertionType || 'all';
    // this.viewportElement = props.viewportElement || document.body;
    // this.viewportElement = props.viewportElement || null;
    // this.contentWindow = props.contentWindow || null;
    this.viewportElement.next(props.viewportElement || null);
    this.contentWindow.next(props.contentWindow || null);
    this.nodeIdAttrName = props.nodeIdAttrName;

    this.digestViewport();
    this.attachEvents();
  }

  get rect() {
    const viewportElement = this.viewportElement;
    if (viewportElement.value)
      return viewportElement.value.getBoundingClientRect();
  }

  get innerRect() {
    const rect = this.rect;
    return new Rect(0, 0, rect!.width, rect!.height);
  }

  get isMaster() {
    return this.contentWindow.value === globalThisPolyfill;
  }

  get isIframe() {
    return !!this.contentWindow.value?.frameElement && !this.isMaster;
  }

  get offsetX() {
    const rect = this.rect;
    if (!rect) return 0;
    return rect.x;
  }

  get offsetY() {
    const rect = this.rect;
    if (!rect) return 0;
    return rect.y;
  }

  get scale() {
    if (!this.viewportElement.value) return 1;
    const clientRect = this.viewportElement.value.getBoundingClientRect();
    const offsetWidth = this.viewportElement.value.offsetWidth;
    if (!clientRect.width || !offsetWidth) return 1;
    return Math.round(clientRect.width / offsetWidth);
  }

  get viewportRoot() {
    return this.isIframe
      ? this.contentWindow.value?.document?.body
      : this.viewportElement.value;
  }

  attachEvents() {
    const engine = this.engine;
    cancelIdle(this.attachRequest);
    this.attachRequest = requestIdle(() => {
      if (!engine) return;

      if (!this.contentWindow.value) {
        return;
      }

      if (this.isIframe) {
        this.workspace.attachEvents(
          this.contentWindow.value,
          this.contentWindow.value
        );
      } else if (isHTMLElement(this.viewportElement.value)) {
        this.workspace.attachEvents(
          this.viewportElement.value,
          this.contentWindow.value
        );
      }
    });
  }

  cacheElements() {
    this.nodeElementsStore = {};

    if (!this.viewportRoot) {
      return;
    }

    this.viewportRoot
      .querySelectorAll(`*[${this.nodeIdAttrName}]`)
      .forEach((element) => {
        const id = element.getAttribute(this.nodeIdAttrName);
        if (id) {
          this.nodeElementsStore[id] = this.nodeElementsStore[id] || [];
          this.nodeElementsStore[id].push(element);
        }
      });
  }

  clearCache() {
    this.nodeElementsStore = {};
  }

  containsElement(element: HTMLElement | Element | EventTarget) {
    if (!this.viewportElement.value) {
      return;
    }

    let root: Element = this.viewportElement.value;
    if (root === element) return true;
    return root?.contains(element as any);
  }

  detachEvents() {
    if (this.isIframe) {
      if (!this.contentWindow.value || !this.viewportElement.value) {
        return;
      }

      this.workspace.detachEvents(this.contentWindow.value);
      this.workspace.detachEvents(this.viewportElement.value);
    } else if (this.viewportElement.value) {
      this.workspace.detachEvents(this.viewportElement.value);
    }
  }

  onMount(element: HTMLElement, contentWindow: Window) {
    this.mounted = true;
    // this.viewportElement = element;
    // this.contentWindow = contentWindow;

    this.viewportElement.next(element);
    this.contentWindow.next(contentWindow);
    this.attachEvents();
    this.digestViewport();
  }

  onUnmount() {
    this.mounted = false;
    this.detachEvents();
  }

  digestViewport() {
    const currentData = this.getCurrentData();

    if (currentData) {
      const { width, height, scrollX, scrollY } = currentData;
      width && this.width.next(width);
      height && this.height.next(height);
      scrollX && this.scrollX.next(scrollX);
      scrollY && this.scrollY.next(scrollY);
    }
  }

  findElementById(id: string): Element | undefined {
    if (!id) return;
    if (!this.viewportRoot) {
      return;
    }

    if (this.nodeElementsStore[id]) return this.nodeElementsStore[id][0];
    return (
      <HTMLElement>(
        this.viewportRoot.querySelector(`*[${this.nodeIdAttrName}='${id}']`)
      ) || undefined
    );
  }

  findElementsById(id: string): Element[] {
    if (!id) return [];
    if (!this.viewportRoot) {
      return [];
    }

    if (this.nodeElementsStore[id]) return this.nodeElementsStore[id];

    return Array.from(
      this.viewportRoot?.querySelectorAll(`*[${this.nodeIdAttrName}='${id}']`)
    );
  }

  getCurrentData() {
    const data: ViewportData = {};
    if (this.isIframe) {
      if (!this.contentWindow.value) {
        return;
      }

      data.height = this.contentWindow.value.innerHeight;
      data.width = this.contentWindow.value.innerWidth;
      data.scrollX = this.contentWindow.value.scrollX;
      data.scrollY = this.contentWindow.value.scrollY;
    } else if (this.viewportElement.value) {
      data.height = this.viewportElement.value.clientHeight;
      data.width = this.viewportElement.value.clientWidth;
      data.scrollX = this.viewportElement.value.scrollLeft;
      data.scrollY = this.viewportElement.value.scrollTop;
    }

    return data;
  }

  getValidNodeElement(node: TreeNode) {
    const getNodeElement = (node: TreeNode): Element | null => {
      if (!node) {
        return null;
      }

      const el = this.findElementById(node.id);

      if (el) {
        return el;
      } else {
        return getNodeElement(node.parent);
      }
    };

    return getNodeElement(node);
  }

  getElementRect(element: HTMLElement | Element) {
    // if (!element) {
    //   return null;
    // }

    const rect = element.getBoundingClientRect();
    const offsetWidth = isHTMLElement(element)
      ? element['offsetWidth']
      : rect.width;
    const offsetHeight = isHTMLElement(element)
      ? element['offsetHeight']
      : rect.height;

    return new Rect(
      rect.x,
      rect.y,
      this.scale !== 1 ? offsetWidth : rect.width,
      this.scale !== 1 ? offsetHeight : rect.height
    );
  }

  getElementRectById(id: string) {
    const elements = this.findElementsById(id);

    const rect = calcBoundingRect(
      elements.map((element) => this.getElementRect(element))
    );

    if (rect) {
      if (this.isIframe) {
        return new Rect(
          rect.x + this.offsetX,
          rect.y + this.offsetY,
          rect.width,
          rect.height
        );
      } else {
        return new Rect(rect.x, rect.y, rect.width, rect.height);
      }
    }
  }

  getChildrenRect(node: TreeNode) {
    if (!node?.children.value?.length) return;

    return calcBoundingRect(
      node.children.value.reduce<Rect[]>((buf, child) => {
        const rect = this.getValidNodeRect(child);
        if (rect) {
          return [...buf, rect];
        }

        return buf;
      }, [])
    );
  }

  getChildrenOffsetRect(node: TreeNode): Rect | undefined {
    if (!node?.children.value?.length) return;

    return calcBoundingRect(
      node.children.value.reduce<Rect[]>((buf, child) => {
        const rect = this.getValidNodeOffsetRect(child);
        if (rect) {
          return [...buf, rect];
        }

        return buf;
      }, [])
    );
  }

  getValidNodeRect(node: TreeNode): Rect | undefined {
    if (!node) return;
    const rect = this.getElementRectById(node.id);
    if (node && node === node.root && node.isInOperation) {
      if (!rect) return this.rect;
      return calcBoundingRect([this.rect as IRect, rect]);
    }

    if (rect) {
      return rect;
    } else {
      return this.getChildrenRect(node);
    }
  }

  getElementOffsetRect(element?: HTMLElement | Element) {}

  getElementOffsetRectById(id: string) {
    const elements = this.findElementsById(id);
    if (!elements.length) return;
    const rect = calcBoundingRect(
      elements.map((element) => this.getElementRect(element))
    );

    if (rect) {
      if (this.isIframe) {
        if (!this.contentWindow.value) {
          return;
        }

        return new Rect(
          rect.x + this.contentWindow.value.scrollX,
          rect.y + this.contentWindow.value.scrollY,
          rect.width,
          rect.height
        );
      } else {
        if (!this.viewportElement.value) {
          return;
        }

        return new Rect(
          (rect.x - this.offsetX + this.viewportElement.value.scrollLeft) /
            this.scale,
          (rect.y - this.offsetY + this.viewportElement.value.scrollTop) /
            this.scale,
          rect.width,
          rect.height
        );
      }
    }
  }

  getValidNodeOffsetRect(node: TreeNode) {
    if (!node) return;
    const rect = this.getElementOffsetRectById(node.id);

    if (node && node === node.root && node.isInOperation) {
      if (!rect) return this.innerRect;
      return calcBoundingRect([this.innerRect, rect]);
    }

    if (rect) {
      return rect;
    } else {
      return this.getChildrenOffsetRect(node);
    }
  }

  getValidNodeLayout(node: TreeNode) {
    if (!node) return 'vertical';
    if (node.parent?.designerProps?.inlineChildrenLayout) return 'horizontal';

    const element = this.findElementById(node.id);
    if (!element) {
      return 'vertical';
    } else {
      return calcElementLayout(element);
    }
  }

  isPointInViewport(point: IPoint, sensitive?: boolean) {
    if (!this.rect) return false;

    const element = document.elementFromPoint(point.x, point.y);
    if (element && !this.containsElement(element)) {
      return false;
    }

    return isPointInRect(point, this.rect, sensitive);
  }

  matchViewport(
    target: HTMLElement | Element | Window | Document | EventTarget
  ) {
    if (this.isIframe) {
      return (
        target === this.viewportElement.value ||
        target === this.contentWindow.value ||
        target === this.contentWindow.value?.document
      );
    } else {
      return target === this.viewportElement.value;
    }
  }
}
