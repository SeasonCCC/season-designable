import { BehaviorSubject } from 'rxjs';
import { Engine } from './Engine';

export enum ScreenType {
  PC = 'PC',
  Responsive = 'Responsive',
  Mobile = 'Mobile',
  Sketch = 'Sketch',
}

export enum ScreenStatus {
  Normal = 'Normal',
  Resizing = 'Resizing',
  Zooming = 'Zooming',
}

export class Screen {
  scale = 1;
  width = new BehaviorSubject<number | string>('100%');
  height = new BehaviorSubject<number | string>('100%');
  engine: Engine;
  background = '';
  flip = new BehaviorSubject<Boolean>(false);
  status = ScreenStatus.Normal;
  type: BehaviorSubject<ScreenType>;

  constructor(engine: Engine) {
    this.engine = engine;
    this.type = new BehaviorSubject<ScreenType>(engine.props.defaultScreenType);
  }

  setStatus(status: ScreenStatus) {
    this.status = status;
  }

  setType(type: ScreenType) {
    this.type.next(type);
  }

  setScale(scale: number) {
    this.scale = scale;
  }

  setSize(width?: number | string, height?: number | string) {
    if (width) {
      this.width.next(width);
    }
    if (height) {
      this.height.next(height);
    }
  }

  resetSize() {
    this.width.next('100%');
    this.height.next('100%');
  }

  setBackground(background: string) {
    this.background = background;
  }

  setFlip(flip: boolean) {
    this.flip.next(flip);
  }
}
