import { BehaviorSubject } from 'rxjs';
import { HoverNodeEvent } from '../events/mutation/HoverNodeEvent';
import { Operation } from './Operation';
import { TreeNode } from './TreeNode';

export interface HoverProps {
  operation: Operation;
}

export class Hover {
  node = new BehaviorSubject<TreeNode | null>(null);
  operation: Operation;

  constructor(props: HoverProps) {
    this.operation = props.operation;
  }

  setHover(node?: TreeNode) {
    if (node) {
      this.node.next(node);
    } else {
      this.node.next(null);
    }
  }

  clear() {
    this.node.next(null);
  }

  trigger() {
    if (this.operation && this.node.value) {
      return this.operation.dispatch(
        new HoverNodeEvent({
          target: this.operation.tree,
          source: this.node.value,
        })
      );
    }
  }
}
