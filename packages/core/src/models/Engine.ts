import {
  Event,
  EventProps,
  getUuid,
  globalThisPolyfill,
} from '@season-designable/shared';
import { BehaviorSubject } from 'rxjs';
import { Keyboard, Shortcut } from '../models';
import { Cursor } from './Cursor';
import { Screen, ScreenType } from './Screen';
import { ITreeNode, TreeNode } from './TreeNode';
import { Workbench } from './Workbench';

export type EngineProps<T = Event> = EventProps<T> & {
  shortcuts: Shortcut[];
  sourceIdAttrName: string; //拖拽源Id的dom属性名
  nodeIdAttrName: string; //节点Id的dom属性名
  contentEditableAttrName: string; //原地编辑属性名
  contentEditableNodeIdAttrName: string; //原地编辑指定Node Id属性名
  clickStopPropagationAttrName: string; //点击阻止冒泡属性
  outlineNodeIdAttrName: string; //大纲树节点ID的dom属性名
  nodeSelectionIdAttrName: string; //节点工具栏属性名
  nodeDragHandlerAttrName: string; //节点拖拽手柄属性名
  screenResizeHandlerAttrName: string;
  nodeResizeHandlerAttrName: string; //节点尺寸拖拽手柄属性名
  nodeTranslateAttrName: string; // 节点自由布局的属性名
  defaultComponentTree?: TreeNode; //默认组件树
  defaultScreenType: ScreenType;
  rootComponentName: string;
};

export class Engine extends Event {
  id: string;

  props: EngineProps<Engine>;

  cursor: Cursor;

  workbench: Workbench;

  keyboard: Keyboard;

  screen: Screen;

  movingNodes$ = new BehaviorSubject<TreeNode[]>([]);

  constructor(props: Partial<EngineProps<Engine>>) {
    super(props);
    this.id = getUuid();
    this.props = { ...Engine.defaultProps, ...props };

    this.workbench = new Workbench(this);
    this.screen = new Screen(this);
    this.cursor = new Cursor(this);
    this.keyboard = new Keyboard(this);
  }

  setCurrentTree(tree: ITreeNode) {
    if (this.workbench.currentWorkspace) {
      this.workbench.currentWorkspace.operation.tree.from(tree);
    }
  }

  getCurrentTree() {
    return this.workbench.currentWorkspace?.operation.tree;
  }

  getAllSelectedNodes() {
    return this.workbench.workspaces.reduce<TreeNode[]>(
      (buf, workspace) => [
        ...buf,
        ...workspace.operation.selection.selectedNodes,
      ],
      []
    );
  }

  findNodeById(id: string) {
    return TreeNode.findById(id);
  }

  findMovingNodes() {
    this.workbench.workspaces.forEach((workspace) => {
      workspace.operation.moveHelper.dragNodes.subscribe((val) => {
        this.movingNodes$.next(val);
      });
    });

    return this.movingNodes$;
  }

  createNode(node: ITreeNode, parent?: TreeNode) {
    return new TreeNode(node, parent);
  }

  mount() {
    this.attachEvents(globalThisPolyfill);
  }

  unmount() {
    this.detachEvents();
  }

  static defaultProps = {
    shortcuts: [],
    effects: [],
    drivers: [],
    rootComponentName: 'Root',
    sourceIdAttrName: 'data-designer-source-id',
    nodeIdAttrName: 'data-designer-node-id',
    contentEditableAttrName: 'data-content-editable',
    contentEditableNodeIdAttrName: 'data-content-editable-node-id',
    clickStopPropagationAttrName: 'data-click-stop-propagation',
    nodeSelectionIdAttrName: 'data-designer-node-helpers-id',
    nodeDragHandlerAttrName: 'data-designer-node-drag-handler',
    screenResizeHandlerAttrName: 'data-designer-screen-resize-handler',
    nodeResizeHandlerAttrName: 'data-designer-node-resize-handler',
    outlineNodeIdAttrName: 'data-designer-outline-node-id',
    nodeTranslateAttrName: 'data-designer-node-translate-handler',
    defaultScreenType: ScreenType.PC,
  };
}
