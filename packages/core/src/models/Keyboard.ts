import { KeyCode } from '@season-designable/shared';
import { get } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { AbstractKeyboardEvent } from '../events/keyboard/AbstractKeyboardEvent';
import { EngineContext } from '../types';
import { Engine } from './Engine';
import { Shortcut } from './Shortcut';

const Modifiers = {
  metaKey: KeyCode.Meta,
  shiftKey: KeyCode.Shift,
  ctrlKey: KeyCode.Control,
  altKey: KeyCode.Alt,
};

export class Keyboard {
  engine: Engine;
  shortcuts: Shortcut[] = [];
  sequence = new BehaviorSubject<KeyCode[]>([]);
  keyDown = new BehaviorSubject<KeyCode | null>(null);
  modifiers = {};
  requestTimer?: NodeJS.Timeout;

  constructor(engine: Engine) {
    this.engine = engine;
    this.shortcuts = engine.props.shortcuts || [];
  }

  matchCodes(context: EngineContext) {
    for (let i = 0; i < this.shortcuts.length; i++) {
      const shortcut = this.shortcuts[i];
      if (shortcut.match(this.sequence.value, context)) {
        return true;
      }
    }

    return false;
  }

  preventCodes() {
    return this.shortcuts.some((shortcut) =>
      shortcut.preventCodes(this.sequence.value)
    );
  }

  includes(key: KeyCode) {
    return this.sequence.value.some((code) => Shortcut.matchCode(code, key));
  }

  excludes(key: KeyCode) {
    return this.sequence.value.filter((code) => !Shortcut.matchCode(code, key));
  }

  addKeyCode(key: KeyCode) {
    if (!this.includes(key)) {
      this.sequence.next([...this.sequence.value, key]);
    }
  }

  removeKeyCode(key: KeyCode) {
    if (this.includes(key)) {
      this.sequence.next(this.excludes(key));
    }
  }

  isModifier(key: KeyCode) {
    return Object.values(Modifiers).some((modifier) =>
      Shortcut.matchCode(modifier, key)
    );
  }

  isKeyDown(code: KeyCode) {
    return this.keyDown.value === code;
  }

  handleModifiers(event: AbstractKeyboardEvent) {
    Object.entries(Modifiers).forEach(([key, code]) => {
      if (get(event, key)) {
        if (!this.sequence.value.includes(code)) {
          this.sequence.next([code, ...this.sequence.value]);
        }
      }
    });
  }

  handleKeyboard(event: AbstractKeyboardEvent, context?: EngineContext) {
    if (event.data) {
      if (event.eventType === 'keydown') {
        this.keyDown.next(event.data);
        this.addKeyCode(event.data);
        this.handleModifiers(event);

        if (context && this.matchCodes(context)) {
          this.sequence.next([]);
        }

        this.requestClean(4000);

        if (this.preventCodes()) {
          event.preventDefault();
          event.stopPropagation();
        }
      } else {
        if (this.isModifier(event.data)) {
          this.sequence.next([]);
        }
        this.keyDown.next(null);
      }
    }
  }

  requestClean(duration = 320) {
    clearTimeout(this.requestTimer);
    this.requestTimer = setTimeout(() => {
      this.keyDown.next(null);
      this.sequence.next([]);
      clearTimeout(this.requestTimer);
    }, duration);
  }
}
