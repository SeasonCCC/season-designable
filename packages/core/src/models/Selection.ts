import { has, isArray, isString } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { SelectNodeEvent } from '../events/mutation/SelectNodeEvent';
import { UnSelectNodeEvent } from '../events/mutation/UnSelectNodeEvent';
import { Operation } from './Operation';
import { TreeNode } from './TreeNode';

export interface ISelection {
  selected?: string[];
  operation?: Operation;
}

export class Selection {
  operation!: Operation;
  selected = new BehaviorSubject<string[]>([]);
  indexes: Record<string, boolean> = {};

  constructor(props: ISelection) {
    if (props.selected) {
      this.selected.next(props.selected);
      // this.selected = props.selected;
    }

    if (props.operation) {
      this.operation = props.operation;
    }
  }

  get selectedNodes() {
    return this.selected.value.reduce<TreeNode[]>((nodes, id) => {
      let node = this.operation.tree.findById(id);
      if (node) {
        return [...nodes, node];
      }
      return nodes;
    }, []);
  }

  get first() {
    if (this.selected && this.selected.value.length)
      return this.selected.value[0];
  }

  get last() {
    if (this.selected && this.selected.value.length)
      return this.selected.value[this.selected.value.length - 1];
  }

  get length() {
    return this.selected.value.length;
  }

  batchSafeSelect(ids: string[] | TreeNode[]) {
    if (!ids?.length) return;
    this.batchSelect(ids);
  }

  batchSelect(ids: string[] | TreeNode[]) {
    this.selected.next(this.mapIds(ids));
    this.indexes = this.selected.value.reduce<Record<string, boolean>>(
      (buf, id) => ({
        ...buf,
        [id]: true,
      }),
      {}
    );
  }

  mapIds(ids: string[] | TreeNode[]): string[] {
    return isArray(ids)
      ? ids.map((node: any) => (isString(node) ? node : node?.id))
      : [];
  }

  trigger(type = SelectNodeEvent) {
    return this.operation.dispatch(
      new type({
        target: this.operation.tree,
        source: this.selectedNodes,
      })
    );
  }

  select(id: string | TreeNode) {
    if (isString(id)) {
      if (
        this.selected.value.length === 1 &&
        this.selected.value.includes(id)
      ) {
        this.trigger(SelectNodeEvent);
        return;
      }
      this.selected.next([id]);
      this.indexes = { [id]: true };
      this.trigger(SelectNodeEvent);
    } else {
      this.select(id.id);
    }
  }

  add(...ids: string[] | TreeNode[]) {
    this.mapIds(ids).forEach((id) => {
      if (!this.selected.value.includes(id)) {
        this.selected.next([...this.selected.value, id]);
        this.indexes[id] = true;
      }
    });

    this.trigger(SelectNodeEvent);
  }

  crossAddTo(id: string | TreeNode) {
    let selectedNode = isString(id) ? this.operation.tree.findById(id) : id;
    if (!selectedNode) {
      return;
    }

    if (!this.has(selectedNode)) {
      const closestSelectNode = this.selectedNodes.reduce(
        (targetNode, node) => {
          return node.distanceTo(node) < targetNode.distanceTo(node)
            ? node
            : targetNode;
        },
        this.selectedNodes[0]
      );

      if (closestSelectNode) {
        const selectedNodeList = selectedNode.crossSiblings(closestSelectNode);

        this.add(...selectedNodeList);
      }
    } else {
      this.remove(selectedNode);
    }
  }

  remove(...ids: string[] | TreeNode[]) {
    this.mapIds(ids).forEach((id) => {
      this.selected.next(this.selected.value.filter((item) => item !== id));
      delete this.indexes[id];
    });

    this.trigger(UnSelectNodeEvent);
  }

  has(...ids: string[] | TreeNode[]) {
    return this.mapIds(ids).some((id) => has(this.indexes, id));
  }
}
