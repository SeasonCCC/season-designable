import {
  IPoint,
  Rect,
  calcDistanceOfPointToRect,
  calcDistancePointToEdge,
  isNearAfter,
  isPointInRect,
} from '@season-designable/shared';
import { Operation } from './Operation';
import { TreeNode } from './TreeNode';
import { Viewport } from './Viewport';
import { CursorDragType } from './Cursor';
import { DragNodeEvent } from '../events/mutation/DragNodeEvent';
import { BehaviorSubject } from 'rxjs';
import { DropNodeEvent } from '../events';

export enum ClosestPosition {
  Before = 'BEFORE',
  ForbidBefore = 'FORBID_BEFORE',
  After = 'After',
  ForbidAfter = 'FORBID_AFTER',
  Upper = 'UPPER',
  ForbidUpper = 'FORBID_UPPER',
  Under = 'UNDER',
  ForbidUnder = 'FORBID_UNDER',
  Inner = 'INNER',
  ForbidInner = 'FORBID_INNER',
  InnerAfter = 'INNER_AFTER',
  ForbidInnerAfter = 'FORBID_INNER_AFTER',
  InnerBefore = 'INNER_BEFORE',
  ForbidInnerBefore = 'FORBID_INNER_BEFORE',
  Forbid = 'FORBID',
}

export interface MoveHelperProps {
  operation: Operation;
}

export interface MoveHelperDragStartProps {
  dragNodes: TreeNode[];
}

export interface MoveHelperDragDropProps {
  dropNode: TreeNode;
}
export interface MoveHelperDragMoveProps {
  touchNode: TreeNode | null;
  point: IPoint;
}

export class MoveHelper {
  operation: Operation;

  rootNode: TreeNode;

  dragNodes = new BehaviorSubject<TreeNode[]>([]);

  touchNode = new BehaviorSubject<TreeNode | null>(null);

  closestNode = new BehaviorSubject<TreeNode | null>(null);

  activeViewport: Viewport | null = null;

  viewportClosestRect = new BehaviorSubject<Rect | null>(null);

  outlineClosestRect = new BehaviorSubject<Rect | null>(null);

  viewportClosestOffsetRect = new BehaviorSubject<Rect | null>(null);

  outlineClosestOffsetRect = new BehaviorSubject<Rect | null>(null);

  viewportClosestDirection = new BehaviorSubject<ClosestPosition | null>(null);

  outlineClosestDirection = new BehaviorSubject<ClosestPosition | null>(null);

  dragging = new BehaviorSubject(false);

  constructor(props: MoveHelperProps) {
    this.operation = props.operation;
    this.rootNode = this.operation.tree;
  }

  get cursor() {
    return this.operation.engine.cursor;
  }

  get viewport() {
    return this.operation.workspace.viewport;
  }

  get outline() {
    return this.operation.workspace.outline;
  }

  get hasDragNodes() {
    return this.dragNodes.value.length > 0;
  }

  get closestDirection() {
    if (this.activeViewport === this.outline) {
      return this.outlineClosestDirection;
    }
    return this.viewportClosestDirection;
  }

  calcClosestNode(point: IPoint, viewport: Viewport) {
    if (this.touchNode.value) {
      const touchNodeRect = viewport.getValidNodeRect(this.touchNode.value);

      if (!touchNodeRect) return null;

      if (this.touchNode.value.children.value.length) {
        const touchDistance = calcDistancePointToEdge(point, touchNodeRect);
        let minDistance = touchDistance;

        let minDistanceNode = this.touchNode.value;

        this.touchNode.value.children.value.forEach((node) => {
          const rect = viewport.getElementRectById(node.id);
          if (!rect) return;

          const distance = isPointInRect(point, rect, viewport.moveSensitive)
            ? 0
            : calcDistanceOfPointToRect(point, rect);

          if (distance <= minDistance) {
            minDistance = distance;
            minDistanceNode = node;
          }
        });

        return minDistanceNode;
      } else {
        return this.touchNode.value;
      }
    }

    return this.operation.tree;
  }

  calcClosestRect(viewport: Viewport, closestDirection: ClosestPosition) {
    if (!this.closestNode.value || !closestDirection) return;
    const closestRect = viewport.getValidNodeRect(this.closestNode.value);
    if (
      closestDirection === ClosestPosition.InnerAfter ||
      closestDirection === ClosestPosition.InnerBefore
    ) {
      return viewport.getChildrenRect(this.closestNode.value);
    } else {
      return closestRect;
    }
  }

  calcClosestOffsetRect(viewport: Viewport, closestDirection: ClosestPosition) {
    if (!this.closestNode.value || !closestDirection) return;
    const closestRect = viewport.getValidNodeOffsetRect(this.closestNode.value);
    if (
      closestDirection === ClosestPosition.InnerAfter ||
      closestDirection === ClosestPosition.InnerBefore
    ) {
      return viewport.getChildrenOffsetRect(this.closestNode.value);
    } else {
      return closestRect;
    }
  }

  calcClosestPosition(point: IPoint, viewport: Viewport) {
    const closestNode = this.closestNode.value;

    if (!closestNode || !viewport.isPointInViewport(point)) {
      return ClosestPosition.Forbid;
    }

    const closestRect = viewport.getValidNodeRect(closestNode);
    const isInline = this.getClosestLayout(viewport) === 'horizontal';
    if (!closestRect) {
      return null;
    }

    const isAfter = isNearAfter(
      point,
      closestRect,
      viewport.moveInsertionType === 'block' ? false : isInline
    );

    const getValidParent = (node: TreeNode): TreeNode | undefined => {
      if (!node) return;
      if (node.parent?.allowSibling(this.dragNodes.value)) return node.parent;
      return getValidParent(node.parent);
    };

    if (isPointInRect(point, closestRect, viewport.moveSensitive)) {
      if (!closestNode.allowAppend(this.dragNodes.value)) {
        if (!closestNode.allowSibling(this.dragNodes.value)) {
          const parentClosestNode = getValidParent(closestNode);

          if (parentClosestNode) {
            this.closestNode.next(parentClosestNode);
          }

          if (isInline) {
            if (parentClosestNode) {
              if (isAfter) {
                return ClosestPosition.After;
              }
              return ClosestPosition.Before;
            }

            if (isAfter) {
              return ClosestPosition.ForbidAfter;
            }
            return ClosestPosition.ForbidBefore;
          } else {
            if (parentClosestNode) {
              if (isAfter) {
                return ClosestPosition.Under;
              }
              return ClosestPosition.Upper;
            }

            if (isAfter) {
              return ClosestPosition.ForbidUnder;
            }
            return ClosestPosition.ForbidUpper;
          }
        } else {
          if (isInline) {
            return isAfter ? ClosestPosition.After : ClosestPosition.Before;
          } else {
            return isAfter ? ClosestPosition.Under : ClosestPosition.Upper;
          }
        }
      }

      if (closestNode.contains(...this.dragNodes.value)) {
        if (isAfter) {
          return ClosestPosition.InnerAfter;
        }
        return ClosestPosition.InnerBefore;
      } else {
        return ClosestPosition.Inner;
      }
    } else if (closestNode.isRoot) {
      return isAfter ? ClosestPosition.InnerAfter : ClosestPosition.InnerBefore;
    } else {
      if (!closestNode.allowSibling(this.dragNodes.value)) {
      }
      if (isInline) {
        return isAfter ? ClosestPosition.After : ClosestPosition.Before;
      } else {
        return isAfter ? ClosestPosition.Under : ClosestPosition.Upper;
      }
    }
  }

  dragStart(props: MoveHelperDragStartProps) {
    const nodes = TreeNode.filterDraggable(props.dragNodes);
    if (nodes.length) {
      this.dragNodes.next(nodes);
      this.trigger(
        new DragNodeEvent({
          target: this.operation.tree,
          source: this.dragNodes.value,
        })
      );
      this.viewport.cacheElements();
      this.cursor.setDragType(CursorDragType.Move);
      this.dragging.next(true);
    }
  }

  dragMove(props: MoveHelperDragMoveProps) {
    if (!this.dragging.value) return;

    if (this.outline.isPointInViewport(props.point, false)) {
      this.activeViewport = this.outline;
      this.touchNode.next(props.touchNode);
      this.closestNode.next(this.calcClosestNode(props.point, this.outline));
    } else if (this.viewport.isPointInViewport(props.point, false)) {
      this.activeViewport = this.viewport;
      this.touchNode.next(props.touchNode);
      this.closestNode.next(this.calcClosestNode(props.point, this.viewport));
    }

    if (!this.activeViewport) return;

    if (this.activeViewport === this.outline) {
      this.outlineClosestDirection.next(
        this.calcClosestPosition(props.point, this.outline)
      );
      this.viewportClosestDirection.next(this.outlineClosestDirection.value);
    } else {
      this.viewportClosestDirection.next(
        this.calcClosestPosition(props.point, this.viewport)
      );
      this.outlineClosestDirection.next(this.viewportClosestDirection.value);
    }

    if (this.outline.mounted) {
      if (!this.outlineClosestDirection.value) return;

      this.outlineClosestRect.next(
        this.calcClosestRect(
          this.outline,
          this.outlineClosestDirection.value
        ) || null
      );

      this.outlineClosestOffsetRect.next(
        this.calcClosestOffsetRect(
          this.outline,
          this.outlineClosestDirection.value
        ) || null
      );
    }

    if (this.viewport.mounted) {
      if (!this.viewportClosestDirection.value) return;

      this.viewportClosestRect.next(
        this.calcClosestRect(
          this.viewport,
          this.viewportClosestDirection.value
        ) || null
      );

      this.viewportClosestOffsetRect.next(
        this.calcClosestOffsetRect(
          this.viewport,
          this.viewportClosestDirection.value
        ) || null
      );
    }
  }

  dragEnd() {
    this.dragging.next(false);
    this.dragNodes.next([]);
    this.touchNode.next(null);
    this.closestNode.next(null);
    this.activeViewport = null;
    this.outlineClosestDirection.next(null);
    this.outlineClosestOffsetRect.next(null);
    this.outlineClosestRect.next(null);
    this.viewportClosestDirection.next(null);
    this.viewportClosestOffsetRect.next(null);
    this.viewportClosestRect.next(null);
    this.viewport.clearCache();
  }

  dragDrop(props: MoveHelperDragDropProps) {
    this.trigger(
      new DropNodeEvent({
        target: this.operation.tree,
        source: props.dropNode,
      })
    );
  }

  getClosestLayout(viewport: Viewport) {
    if (this.closestNode && this.closestNode.value) {
      return viewport.getValidNodeLayout(this.closestNode.value);
    }
  }

  trigger(event: any) {
    if (this.operation) {
      return this.operation.dispatch(event);
    }
  }
}
