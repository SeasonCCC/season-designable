import { globalThisPolyfill } from '@season-designable/shared';
import { isNumber } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { Engine } from './Engine';

export enum CursorStatus {
  Normal = 'NORMAL',
  DragStart = 'DRAG_START',
  Dragging = 'DRAGGING',
  DragStop = 'DRAG_STOP',
}

export enum CursorDragType {
  Move = 'MOVE',
  Resize = 'RESIZE',
  Rotate = 'ROTATE',
  Scale = 'SCALE',
  Translate = 'TRANSLATE',
  Round = 'ROUND',
}

export enum CursorType {
  Normal = 'NORMAL',
  Selection = 'SELECTION',
  Sketch = 'SKETCH',
}

export type CursorPosition = {
  pageX?: number;

  pageY?: number;

  clientX?: number;

  clientY?: number;

  topPageX?: number;

  topPageY?: number;

  topClientX?: number;

  topClientY?: number;
};

type CursorPositionKey = keyof CursorPosition;

export type CursorProps = {
  status?: CursorStatus;

  position?: CursorPosition;

  dragStartPosition?: CursorPosition;

  dragEndPosition?: CursorPosition;

  view?: Window;
};

const DEFAULT_POSITION = {
  pageX: 0,
  pageY: 0,
  clientX: 0,
  clientY: 0,
  topPageX: 0,
  topPageY: 0,
  topClientX: 0,
  topClientY: 0,
};

const setCursorStyle = (contentWindow: Window | null, style: string) => {
  const currentRoot = document.getElementsByTagName('html')[0];
  const root = contentWindow?.document.getElementsByTagName('html')[0];
  if (root && root.style.cursor !== style) {
    root.style.cursor = style;
  }

  if (currentRoot && currentRoot.style.cursor !== style) {
    currentRoot.style.cursor = style;
  }
};

const calcPositionDelta = (end: CursorPosition, start: CursorPosition) => {
  const positionDelta: CursorPosition = {};

  Object.keys(end).forEach((key) => {
    const endPos = end[<CursorPositionKey>key];
    const startPos = start[<CursorPositionKey>key];
    if (isNumber(endPos) && isNumber(startPos)) {
      positionDelta[<CursorPositionKey>key] = endPos - startPos;
    } else {
      positionDelta[<CursorPositionKey>key] = endPos;
    }
    return {};
  });

  return positionDelta;
};

export class Cursor {
  engine: Engine;

  type = new BehaviorSubject<CursorType | string>(CursorType.Normal);

  dragType = new BehaviorSubject<CursorDragType | string>(CursorDragType.Move);

  status = new BehaviorSubject<CursorStatus>(CursorStatus.Normal);

  position = new BehaviorSubject<CursorPosition>(DEFAULT_POSITION);

  dragStartPosition = new BehaviorSubject<CursorPosition>(DEFAULT_POSITION);

  dragEndPosition = new BehaviorSubject<CursorPosition>(DEFAULT_POSITION);

  dragAtomDelta = new BehaviorSubject<CursorPosition>(DEFAULT_POSITION);

  dragStartToCurrentDelta = new BehaviorSubject<CursorPosition>(
    DEFAULT_POSITION
  );

  dragStartToEndDelta = new BehaviorSubject<CursorPosition>(DEFAULT_POSITION);

  view: Window = globalThisPolyfill;

  constructor(engine: Engine) {
    this.engine = engine;
  }

  get speed() {
    const { clientX, clientY } = this.dragAtomDelta.value;
    if (clientX && clientY) {
      return Math.sqrt(Math.pow(clientX, 2) + Math.pow(clientY, 2));
    } else {
      return 0;
    }
  }

  setStatus(status: CursorStatus) {
    this.status.next(status);
  }

  setType(type: CursorType | string) {
    this.type.next(type);
  }

  setDragType(type: CursorDragType | string) {
    this.dragType.next(type);
  }

  setStyle(style: string) {
    this.engine.workbench.workspaces.forEach((workspace) => {
      setCursorStyle(workspace.viewport.contentWindow.value, style);
    });
  }

  setPosition(position?: CursorPosition) {
    if (position) {
      this.dragAtomDelta.next(calcPositionDelta(this.position.value, position));
      this.position.next({ ...position });

      if (this.status.value === CursorStatus.Dragging) {
        this.dragStartToCurrentDelta.next(
          calcPositionDelta(this.position.value, this.dragStartPosition.value)
        );
      }
    }
  }

  setDragStartPosition(position?: CursorPosition) {
    if (position) {
      this.dragStartPosition.next({ ...position });
    } else {
      this.dragStartPosition.next(DEFAULT_POSITION);
      this.dragStartToCurrentDelta.next(DEFAULT_POSITION);
    }
  }

  setDragEndPosition(position?: CursorPosition) {
    if (position) {
      this.dragEndPosition.next({ ...position });
      this.dragStartToEndDelta.next(
        calcPositionDelta(
          this.dragStartPosition.value,
          this.dragEndPosition.value
        )
      );
    } else {
      this.dragEndPosition.next(DEFAULT_POSITION);
      this.dragStartToEndDelta.next(DEFAULT_POSITION);
    }
  }
}
