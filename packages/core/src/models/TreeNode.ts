import { getUuid } from '@season-designable/shared';
import { clone, isFunction } from 'lodash-es';
import { mergeLocales } from '../internals';
import { GlobalRegistry } from '../register';
import {
  DesignerControllerProps,
  DesignerLocales,
  DesignerProps,
} from '../types';
import { Operation } from './Operation';
import {
  PrependNodeEvent,
  CloneNodeEvent,
  FromNodeEvent,
  AppendNodeEvent,
} from '../events';
import { BehaviorSubject } from 'rxjs';
import { RemoveNodeEvent } from '../events/mutation/RemoveNodeEvent';
import { InsertAfterEvent } from '../events/mutation/InsertAfterEvent';
import { InsertBeforeEvent } from '../events/mutation/InsertBeforeEvent';

export interface ITreeNode {
  componentName?: string;
  sourceName?: string;
  operation?: Operation;
  hidden?: boolean;
  isSourceNode?: boolean;
  id?: string;
  props?: Record<string | number | symbol, any>;
  children?: ITreeNode[];
}

export type NodeFinder = {
  (node: TreeNode): boolean;
};

const TreeNodes = new Map<string, TreeNode>();

const CommonDesignerPropsMap = new Map<string, DesignerControllerProps>();

const removeNode = (node: TreeNode) => {
  if (node.parent) {
    node.parent.children.next(
      node.parent.children.value.filter((child) => child !== node)
    );
    // node.parent.children = node.parent.children.filter(
    //   (child) => child !== node
    // );
  }
};

/**
 * 重置 node 的 parent
 * @param nodes {@link TreeNode[]}
 * @param parent {@link TreeNode}
 * @returns 重置后的 nodes {@link TreeNode[]}
 */
const resetNodesParent = (nodes: TreeNode[], parent: TreeNode) => {
  const resetDepth = (node: TreeNode) => {
    node.depth = node.parent ? node.parent.depth + 1 : 0;
    node.children.value.forEach(resetDepth);
  };

  const shallowReset = (node: TreeNode) => {
    node.parent = parent;
    node.root = parent.root;
    resetDepth(node);
  };

  const deepReset = (node: TreeNode) => {
    shallowReset(node);
    resetNodesParent(node.children.value, node);
  };

  return nodes.map((node) => {
    if (node === parent) return node;

    if (!parent.isSourceNode) {
      if (node.isSourceNode) {
        node = node.clone(parent);
        resetDepth(node);
      } else if (!node.isRoot && node.isInOperation) {
        node.operation?.selection.remove(node);
        removeNode(node);
        shallowReset(node);
      } else {
        deepReset(node);
      }
    } else {
      deepReset(node);
    }
    if (!TreeNodes.has(node.id)) {
      TreeNodes.set(node.id, node);
      CommonDesignerPropsMap.set(node.componentName, node.designerProps);
    }
    return node;
  });
};

// const resetParent = (node: TreeNode, parent: TreeNode) => {
//   return resetNodesParent([node], parent)[0];
// };

const resolveDesignerProps = (
  node: TreeNode,
  props: DesignerControllerProps
) => {
  if (isFunction(props)) return props(node);
  return props;
};

export class TreeNode {
  parent!: TreeNode;

  root!: TreeNode;

  rootOperation!: Operation;

  id!: string;

  depth = 0;

  hidden = false;

  componentName = 'NO_NAME_COMPONENT';

  sourceName = '';

  // props: ITreeNode['props'] = {};
  props = new BehaviorSubject<ITreeNode['props']>({});

  children = new BehaviorSubject<TreeNode[]>([]);

  isSelfSourceNode!: boolean;

  constructor(node?: ITreeNode | TreeNode, parent?: TreeNode) {
    if (node instanceof TreeNode) {
      return node;
    }

    this.id = node?.id || getUuid();

    if (parent) {
      this.parent = parent;
      this.depth = parent.depth + 1;
      this.root = parent.root;
    } else {
      this.root = this;
    }

    TreeNodes.set(this.id, this);

    if (node) {
      if (node.operation) {
        this.rootOperation = node.operation;
      }

      this.isSelfSourceNode = node.isSourceNode || false;
      this.from(node);
    }
  }

  get designerProps(): DesignerProps {
    const behaviors = GlobalRegistry.getDesignerBehaviors(this);
    const designerProps: DesignerProps = behaviors.reduce((buf, pattern) => {
      if (!pattern.designerProps) return buf;
      Object.assign(buf, resolveDesignerProps(this, pattern.designerProps));
      return buf;
    }, {});

    return designerProps;
  }

  get designerLocales(): DesignerLocales {
    const behaviors = GlobalRegistry.getDesignerBehaviors(this);

    const designerLocales: DesignerLocales = behaviors.reduce(
      (buf, pattern) => {
        if (!pattern.designerLocales) return buf;
        mergeLocales(buf, pattern.designerLocales);
        return buf;
      },
      {}
    );

    return designerLocales;
  }

  get previous(): TreeNode | null {
    if (this.parent === this || !this.parent) return null;
    return this.parent.children.value[this.index - 1];
  }

  get next(): TreeNode | null {
    if (this.parent === this || !this.parent) return null;
    return this.parent.children.value[this.index + 1];
  }

  get siblings() {
    if (this.parent) {
      return this.parent.children.value.filter((node) => node !== this);
    }
    return [];
  }

  get index() {
    if (this.parent === this || !this.parent) return 0;
    return this.parent.children.value.indexOf(this);
  }

  get descendants(): TreeNode[] {
    return this.children.value.reduce<TreeNode[]>(
      (buf, node) => buf.concat(node).concat(node.descendants),
      []
    );
  }

  get isRoot() {
    return this === this.root;
  }

  get isInOperation() {
    return !!this.operation;
  }

  get lastChild() {
    return this.children.value[this.children.value.length - 1];
  }

  get firstChild() {
    return this.children.value[0];
  }

  get isSourceNode() {
    return this.root.isSelfSourceNode;
  }

  get operation() {
    return this.root?.rootOperation;
  }

  get viewport() {
    return this.operation?.workspace?.viewport;
  }

  get outline() {
    return this.operation?.workspace?.outline;
  }

  get moveLayout() {
    return this.viewport?.getValidNodeLayout(this);
  }

  prepend(...nodes: TreeNode[]) {
    if (nodes.some((node) => node.contains(this))) return [];
    const originSourceParents = nodes.map((node) => node.parent);
    const newNodes = resetNodesParent(
      nodes.filter((node) => node !== this),
      this
    );

    if (!newNodes.length) return [];

    return this.triggerMutation(
      new PrependNodeEvent({
        originSourceParents,
        target: this,
        source: newNodes,
      }),
      () => {
        this.children.next([...this.children.value, ...newNodes]);
        // this.children = [...this.children, ...newNodes];
        return newNodes;
      },
      []
    );
  }

  append(...nodes: TreeNode[]) {
    if (nodes.some((node) => node.contains(this))) return [];
    const newNodes = resetNodesParent(
      nodes.filter((node) => node !== this),
      this
    );

    if (!newNodes.length) return [];

    const originSourceParents = nodes.map((node) => node.parent);

    return this.triggerMutation(
      new AppendNodeEvent({
        originSourceParents,
        target: this,
        source: newNodes,
      }),
      () => {
        this.children.next([...this.children.value, ...newNodes]);
        return newNodes;
      },
      []
    );
  }

  insertAfter(...nodes: TreeNode[]) {
    const parent = this.parent;
    if (nodes.some((node) => node.contains(this))) return [];

    const newNodes = resetNodesParent(
      nodes.filter((node) => node !== this),
      parent
    );

    if (!newNodes.length) return [];
    const originSourceParents = nodes.map((node) => node.parent);

    return this.triggerMutation(
      new InsertAfterEvent({
        originSourceParents,
        target: this,
        source: newNodes,
      }),
      () => {
        const nodeList = parent.children.value.reduce<TreeNode[]>(
          (buf, children) => {
            if (children === this) {
              return [...buf, children, ...newNodes];
            } else {
              return [...buf, children];
            }
          },
          []
        );

        parent.children.next(nodeList);

        return newNodes;
      },
      []
    );
  }

  insertBefore(...nodes: TreeNode[]) {
    const parent = this.parent;
    if (nodes.some((node) => node.contains(this))) return [];

    const newNodes = resetNodesParent(
      nodes.filter((node) => node !== this),
      parent
    );

    if (!newNodes.length) return [];
    const originSourceParents = nodes.map((node) => node.parent);

    return this.triggerMutation(
      new InsertBeforeEvent({
        originSourceParents,
        target: this,
        source: newNodes,
      }),
      () => {
        parent.children.next(
          parent.children.value.reduce<TreeNode[]>((buf, children) => {
            if (children === this) {
              return [...buf, ...newNodes, children];
            } else {
              return [...buf, children];
            }
          }, [])
        );

        return newNodes;
      },
      []
    );
  }

  getElement(area: 'viewport' | 'outline' = 'viewport') {
    return this[area]?.findElementById(this.id);
  }

  getValidElement(area: 'viewport' | 'outline' = 'viewport') {
    return this[area]?.getValidNodeElement(this);
  }

  getElementRect(area: 'viewport' | 'outline' = 'viewport') {
    let element = this.getElement(area);
    if (!element) {
      return;
    }
    return this[area]?.getElementRect(element);
  }

  getValidElementRect(area: 'viewport' | 'outline' = 'viewport') {
    return this[area]?.getValidNodeRect(this);
  }

  getElementOffsetRect(area: 'viewport' | 'outline' = 'viewport') {
    return this[area]?.getElementOffsetRect(this.getElement(area));
  }

  getValidElementOffsetRect(area: 'viewport' | 'outline' = 'viewport') {
    return this[area]?.getValidNodeOffsetRect(this);
  }

  getPrevious(step = 1) {
    return this.parent.children.value[this.index - step];
  }

  getAfter(step = 1) {
    return this.parent.children.value[this.index + step];
  }

  getSibling(index = 0) {
    return this.parent.children.value[index];
  }

  getParents(node?: TreeNode): TreeNode[] {
    const _node = node || this;
    return _node?.parent
      ? [_node.parent].concat(this.getParents(_node.parent))
      : [];
  }

  getParentByDepth(depth = 0): TreeNode {
    let parent = this.parent;
    if (parent?.depth === depth) {
      return parent;
    } else {
      return parent?.getParentByDepth(depth);
    }
  }

  getMessage(token: string) {
    return GlobalRegistry.getDesignerMessage(token, this.designerLocales);
  }

  isMyAncestor(node: TreeNode) {
    if (node === this || this.parent === node) return false;
    return node.contains(this);
  }

  isMyParent(node: TreeNode) {
    return this.parent === node;
  }

  isMyParents(node: TreeNode) {
    if (node === this) return false;
    return this.isMyParent(node) || this.isMyAncestor(node);
  }

  isMyChild(node: TreeNode) {
    return node.isMyParent(this);
  }

  isMyChildren(node: TreeNode) {
    return node.isMyParents(this);
  }

  from(node: ITreeNode) {
    if (!node) return;

    return this.triggerMutation(
      new FromNodeEvent({
        target: this,
        source: node,
      }),
      () => {
        if (node.id && node.id !== this.id) {
          TreeNodes.delete(this.id);
          TreeNodes.set(node.id, this);
          this.id = node.id;
        }

        if (node.componentName) {
          this.componentName = node.componentName;
        }

        this.props.next(node.props ?? {});

        if (node.hidden) {
          this.hidden = node.hidden;
        }

        if (node.children) {
          this.children.next(
            node.children.map((n) => new TreeNode(n, this)) || []
          );
        }
      }
    );
  }

  serialize(): ITreeNode {
    return {
      id: this.id,
      componentName: this.componentName,
      sourceName: this.sourceName,
      props: this.props.value,
      hidden: this.hidden,
      children: this.children.value.map((treeNode) => treeNode.serialize()),
    };
  }

  takeSnapshot(type?: string) {
    this.operation?.snapshot(type);
  }

  triggerMutation<T>(event: any, callback?: () => T, defaults?: T): T | null {
    if (this.operation) {
      this.takeSnapshot(event.constructor.name);
      const result = this.operation.dispatch(event, callback) || defaults;
      return result || null;
    } else if (isFunction(callback)) {
      return callback();
    }

    return null;
  }

  find(finder: NodeFinder): TreeNode | null {
    if (finder(this)) {
      return this;
    } else {
      let result = null;
      this.children.value.forEach((node) => {
        if (finder(node)) {
          result = node;
          return null;
        }
      });

      return result;
    }
  }

  findAll(finder: NodeFinder): TreeNode[] {
    const results = [];
    if (finder(this)) {
      results.push(this);
    }
    this.children.value.forEach((node) => {
      if (finder(node)) {
        results.push(node);
      }
    });
    return results;
  }

  findById(id: string) {
    if (id === '') {
      return null;
    }

    if (this.id === id) return this;

    if (this.children.value.length > 0) {
      return TreeNodes.get(id) || null;
    }

    return null;
  }

  contains(...nodeList: TreeNode[]) {
    return nodeList.every((node) => {
      if (
        this === node ||
        this === node.parent ||
        this === node.getParentByDepth(this.depth)
      ) {
        return true;
      }

      return false;
    });
  }

  distanceTo(node: TreeNode) {
    if (this.root !== node.root) {
      return Infinity;
    }

    if (this.parent !== node.parent) {
      return Infinity;
    }

    return Math.abs(this.index - node.index);
  }

  crossSiblings(node: TreeNode): TreeNode[] {
    if (this.parent !== node.parent) {
      return [];
    }

    const maxIndex = Math.max(node.index, this.index);
    const minIndex = Math.min(node.index, this.index);
    // const results: TreeNode[] = [];

    // for (let i = minIndex + 1; i < maxIndex; i++) {
    //   results.push(this.parent.children.value[i]);
    // }

    return this.parent.children.value.slice(minIndex, maxIndex + 1);
  }

  allowSibling(nodes: TreeNode[]) {
    if (this.designerProps.allowAppend?.(this, nodes) === false) {
      return false;
    }
    return this.parent?.allowAppend(nodes);
  }

  allowDrop(parent: TreeNode) {
    if (!isFunction(this.designerProps.allowDrop)) return true;
    return this.designerProps.allowDrop(parent);
  }

  allowAppend(nodes: TreeNode[]) {
    if (!this.designerProps.droppable) return false;
    if (this.designerProps.allowAppend?.(this, nodes) === false) return false;
    if (nodes.some((node) => !node.allowDrop(this))) return false;
    if (this.isRoot) return true;
    return true;
  }

  allowClone() {
    if (this === this.root) return false;
    return this.designerProps.cloneable ?? true;
  }

  allowDrag() {
    if (this === this.root && !this.isSourceNode) return false;
    return this.designerProps.draggable ?? true;
  }

  allowDelete() {
    if (this === this.root) return false;
    return this.designerProps.deletable ?? true;
  }

  clone(parent?: TreeNode) {
    const newNode = new TreeNode(
      {
        id: getUuid(),
        componentName: this.componentName,
        sourceName: this.sourceName,
        props: this.props.value,
      },
      parent || this.parent
    );

    newNode.children.next(
      resetNodesParent(
        this.children.value.map<TreeNode>((child) => child.clone(newNode)),
        newNode
      )
    );

    return (
      this.triggerMutation(
        new CloneNodeEvent({
          target: this,
          source: newNode,
        }),
        () => newNode
      ) || newNode
    );
  }

  remove() {
    return this.triggerMutation(
      new RemoveNodeEvent({
        target: this,
        source: [],
      }),
      () => {
        removeNode(this);
        TreeNodes.delete(this.id);
      }
    );
  }

  static sort(nodes: TreeNode[] = []) {
    return nodes.sort((node1, node2) => {
      if (node1.depth !== node2.depth) {
        return 0;
      }

      // 对需要 nodes 排序，保持 node 在 children 中的排序
      return node1.index > node2.index ? 1 : -1;
    });
  }

  static clone(nodes: TreeNode[]) {
    const groups: Map<string, TreeNode[]> = new Map();
    const lastGroupNode: Map<string, TreeNode> = new Map();

    const filterNestedNode = TreeNode.sort(nodes).filter(
      (node) => !nodes.some((parent) => node.isMyParents(parent))
    );

    filterNestedNode.forEach((node) => {
      if (node === node.root) return;
      if (!node.allowClone()) return;
      if (!node?.operation) return;

      let nodeList = groups.get(node.parent.id);
      if (!nodeList) {
        groups.set(node.parent.id, [node]);
      } else {
        groups.set(node.parent.id, [...nodeList, node]);
      }

      let lastNode = lastGroupNode.get(node.parent.id);
      if (lastNode) {
        if (node.index > lastNode.index) {
          lastGroupNode.set(node.parent.id, node);
        }
      } else {
        lastGroupNode.set(node.parent.id, node);
      }
    });

    const parents = new Map<TreeNode, TreeNode[]>();

    groups.forEach((nodes, parentId) => {
      let insertPoint = lastGroupNode.get(parentId) || null;

      nodes.forEach((node) => {
        const cloneNode = node.clone();

        if (
          node.operation.selection.has(node) &&
          insertPoint?.parent.allowAppend([cloneNode])
        ) {
          insertPoint.insertAfter(cloneNode);
          insertPoint = insertPoint.next;
        } else if (node.operation.selection.length === 1) {
          const targetNode = node.operation?.tree.findById(
            node.operation.selection.first || ''
          );

          if (targetNode) {
            let cloneNodes = parents.get(targetNode) || [];

            if (targetNode.allowAppend([cloneNode])) {
              cloneNodes.push(cloneNode);
            }

            parents.set(targetNode, cloneNodes);
          }
        }
      });
    });

    parents.forEach((nodes, target) => {
      if (!nodes.length) return;
      target.append(...nodes);
    });
  }

  static findById(id: string) {
    return TreeNodes.get(id);
  }

  static filterDraggable(nodes: TreeNode[] = []) {
    return nodes.reduce<TreeNode[]>((buf, node) => {
      if (!node.allowDrag()) return buf;

      if (isFunction(node.designerProps.getDragNodes)) {
        return buf.concat(node.designerProps.getDragNodes(node));
      }

      if (node.componentName === '$$ResourceNode$$') {
        return [...buf, ...node.children.value];
      }

      return [...buf, node];
    }, []);
  }

  static filterDroppable(nodes: TreeNode[] = [], parent: TreeNode) {
    return nodes.reduce<TreeNode[]>((buf, node) => {
      if (!node.allowDrop(parent)) return buf;

      if (isFunction(node.designerProps.getDropNodes)) {
        const cloned = node.isSourceNode ? node.clone(node.parent) : node;
        const transformed = node.designerProps.getDropNodes(cloned, parent);
        return transformed ? buf.concat(transformed) : buf;
      }

      if (node.componentName === '$$ResourceNode$$') {
        return [...buf, ...node.children.value];
      }

      return [...buf, node];
    }, []);
  }

  static remove(nodes: TreeNode[] = []) {
    nodes.forEach((node) => {
      if (node.allowDelete()) {
        const previous = node.previous;
        const next = node.next;
        node.remove();

        if (previous) {
          node.operation.selection.select(previous);
        } else if (next) {
          node.operation.selection.select(next);
        } else {
          node.operation.selection.select(node.parent);
        }

        node.operation.hover.clear();
      }
    });
  }
}
