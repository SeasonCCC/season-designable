import { EventContainer, getUuid } from '@season-designable/shared';
import { Engine } from './Engine';
import { Viewport } from './Viewport';
import { IOperation, Operation } from './Operation';
import { History } from './History';
import { EngineContext } from '../types';
import { CustomEvent } from '@season-designable/shared';
import { HistoryPushEvent } from '../events/history/HistoryPushEvent';
import { HistoryRedoEvent } from '../events/history/HistoryRedoEvent';
import { HistoryUndoEvent } from '../events/history/HistoryUndoEvent';
import { HistoryGotoEvent } from '../events/history/HistoryGotoEvent';

export type ViewportMatcher = {
  contentWindow?: Window;
  viewportElement?: HTMLElement;
};

export type IWorkspace = {
  id?: string;
  title?: string;
  description?: string;
  operation: IOperation;
};

export interface WorkspaceProps {
  id?: string;
  title?: string;
  description?: string;
  contentWindow?: Window;
  viewportElement?: HTMLElement;
}

export class Workspace {
  id: string;

  title: string;

  description: string;

  engine: Engine;

  viewport: Viewport;

  outline: Viewport;

  operation: Operation;

  history: History<this>;

  props: WorkspaceProps;

  constructor(engine: Engine, props: WorkspaceProps) {
    this.engine = engine;
    this.props = props;
    this.id = props.id || getUuid();
    this.title = props.title || '';
    this.description = props.description || '';
    this.viewport = new Viewport({
      engine: this.engine,
      workspace: this,
      viewportElement: props.viewportElement,
      contentWindow: props.contentWindow,
      nodeIdAttrName: this.engine.props.nodeIdAttrName,
      moveSensitive: true,
      moveInsertionType: 'all',
    });
    this.outline = new Viewport({
      engine: this.engine,
      workspace: this,
      viewportElement: props.viewportElement,
      contentWindow: props.contentWindow,
      nodeIdAttrName: this.engine.props.outlineNodeIdAttrName,
      moveSensitive: false,
      moveInsertionType: 'block',
    });
    this.operation = new Operation(this);
    this.history = new History(this, {
      onPush: (item) => {
        this.operation.dispatch(new HistoryPushEvent(item));
      },
      onRedo: (item) => {
        this.operation.hover.clear();
        this.operation.dispatch(new HistoryRedoEvent(item));
      },
      onUndo: (item) => {
        this.operation.hover.clear();
        this.operation.dispatch(new HistoryUndoEvent(item));
      },
      onGoto: (item) => {
        this.operation.hover.clear();
        this.operation.dispatch(new HistoryGotoEvent(item));
      },
    });
  }

  getEventContext(): EngineContext {
    return {
      workbench: this.engine.workbench,
      workspace: this,
      engine: this.engine,
      viewport: this.viewport,
    };
  }

  dispatch(event: CustomEvent) {
    return this.engine.dispatch(event, this.getEventContext());
  }

  attachEvents(container: EventContainer, contentWindow: Window) {
    this.engine.attachEvents(container, contentWindow, this.getEventContext());
  }

  detachEvents(container: EventContainer) {
    this.engine.detachEvents(container);
  }

  serialize(): IWorkspace {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      operation: this.operation.serialize(),
    };
  }

  from(workspace?: IWorkspace) {
    if (!workspace) return;
    if (workspace.operation) {
      this.operation.from(workspace.operation);
    }

    if (workspace.id) {
      this.id = workspace.id;
    }

    if (workspace.title) {
      this.title = workspace.title;
    }

    if (workspace.description) {
      this.description = workspace.description;
    }
  }
}
