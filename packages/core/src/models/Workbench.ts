import { BehaviorSubject } from 'rxjs';
import { AddWorkspaceEvent } from '../events/workbench/AddWorkspaceEvent';
import { WorkbenchTypes } from '../types';
import { Engine } from './Engine';
import { Workspace, WorkspaceProps } from './Workspace';

export class Workbench {
  workspaces: Workspace[];

  currentWorkspace: Workspace | null;

  activeWorkspace: Workspace | null;

  engine: Engine;

  type = new BehaviorSubject<WorkbenchTypes>('DESIGNABLE');

  constructor(engine: Engine) {
    this.engine = engine;
    this.workspaces = [];
    this.currentWorkspace = null;
    this.activeWorkspace = null;
  }

  setWorkbenchType(type: WorkbenchTypes) {
    this.type.next(type);
  }

  setActiveWorkspace(workspace: Workspace) {
    this.activeWorkspace = workspace;
    return workspace;
  }

  findWorkspaceById(id?: string) {
    return this.workspaces.find((workspace) => workspace.id === id);
  }

  addWorkspace(props: WorkspaceProps) {
    const finded = this.findWorkspaceById(props.id);
    if (!finded) {
      this.currentWorkspace = new Workspace(this.engine, props);
      this.workspaces.push(this.currentWorkspace);
      this.engine.dispatch(new AddWorkspaceEvent(this.currentWorkspace));
      return this.currentWorkspace;
    }

    return finded;
  }

  ensureWorkspace(props: WorkspaceProps) {
    const workspace = this.findWorkspaceById(props.id);
    if (workspace) return workspace;
    this.addWorkspace(props);
    return this.currentWorkspace;
  }
}
