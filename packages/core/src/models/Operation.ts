import { Engine } from './Engine';
import { Hover } from './Hover';
import { Selection } from './Selection';
import { MoveHelper } from './MoveHelper';
import { TransformHelper } from './TransformHelper';
import { Workspace } from './Workspace';
import { ITreeNode, TreeNode } from './TreeNode';
import { isFunction } from 'lodash-es';
import {
  CustomEvent,
  cancelIdle,
  requestIdle,
} from '@season-designable/shared';

export interface IOperation {
  tree?: ITreeNode;
  selected?: string[];
}

export class Operation {
  workspace: Workspace;

  engine: Engine;

  tree: TreeNode;

  selection: Selection;

  hover: Hover;

  transformHelper: TransformHelper;

  moveHelper: MoveHelper;

  requests = {
    snapshot: 0,
  };

  constructor(workspace: Workspace) {
    this.engine = workspace.engine;
    this.workspace = workspace;
    if (this.engine.props.defaultComponentTree) {
      this.tree = new TreeNode({
        componentName: this.engine.props.rootComponentName,
        ...this.engine.props.defaultComponentTree.serialize(),
        operation: this,
      });
    } else {
      this.tree = new TreeNode({
        componentName: this.engine.props.rootComponentName,
        operation: this,
      });
    }
    // this.tree = new TreeNode({
    //   componentName: this.engine.props.rootComponentName,
    //   ...this.engine.props.defaultComponentTree,
    //   operation: this,
    // });

    this.hover = new Hover({
      operation: this,
    });

    this.selection = new Selection({
      operation: this,
    });

    this.moveHelper = new MoveHelper({
      operation: this,
    });

    this.transformHelper = new TransformHelper({
      operation: this,
    });

    this.selection.select(this.tree);
  }

  dispatch(event: CustomEvent, callback?: () => void) {
    // if (!this.workspace.dispatch(event)) return null;
    this.workspace.dispatch(event);
    if (isFunction(callback)) return callback();
    return null;
  }

  from(operation?: IOperation) {
    if (!operation) return;
    if (operation.tree) {
      // console.log(operation.tree.children);
      this.tree.from(operation.tree);
    }
    if (operation.selected) {
      this.selection.selected.next(operation.selected);
      // this.selection.selected = operation.selected;
    }
  }

  serialize(): IOperation {
    return {
      tree: this.tree.serialize(),
      selected: [this.tree.id],
    };
  }

  snapshot(type?: string) {
    cancelIdle(this.requests.snapshot);
    if (
      !this.workspace ||
      !this.workspace.history ||
      this.workspace.history.locking
    ) {
      return;
    }

    this.requests.snapshot = requestIdle(() => {
      this.workspace.history.push(type);
    });
  }
}
