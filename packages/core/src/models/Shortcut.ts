import { KeyCode } from '@season-designable/shared';
import { EngineContext } from '../types';
import { isArray, isFunction } from 'lodash-es';

export interface IShortcutProps {
  codes?: KeyCode[] | KeyCode[][];
  matcher?: (codes: KeyCode[]) => boolean;
  handler?: (context: EngineContext) => void;
}

export class Shortcut {
  codes: KeyCode[][];
  handler: ((context: EngineContext) => void) | null;
  matcher: ((codes: KeyCode[]) => boolean) | null;

  constructor(props: IShortcutProps) {
    this.codes = this.parseCode(props.codes);
    this.handler = props.handler || null;
    this.matcher = props.matcher || null;
  }

  parseCode(codes?: (KeyCode | KeyCode[])[]) {
    if (!codes) return [];

    return codes.reduce<KeyCode[][]>((buf, code) => {
      if (isArray(code)) {
        return [...buf, code];
      } else {
        return [...buf, [code]];
      }
    }, []);
  }

  preventCodes(codes: KeyCode[]) {
    if (!this.codes.length) {
      return false;
    }

    for (let i = 0; i < this.codes.length; i++) {
      const sequence = this.codes[i];

      for (let j = 0; j < sequence.length; j++) {
        if (codes[j] && !Shortcut.matchCode(sequence[j], codes[j])) {
          return false;
        }
      }
    }

    return true;
  }

  matched(matched: boolean, context: EngineContext) {
    if (matched && isFunction(this.handler)) {
      this.handler(context);
      return matched;
    }
    return matched;
  }

  match(codes: KeyCode[], context: EngineContext) {
    return this.codes.some((sequence) => {
      const sortedSelf = Shortcut.sortCodes(sequence);
      const sortedTarget = Shortcut.sortCodes(codes);

      if (isFunction(this.matcher)) {
        return this.matched(this.matcher(sortedTarget), context);
      }

      if (sortedTarget.length !== sortedSelf.length) return false;

      for (let i = 0; i < sortedSelf.length; i++) {
        if (!Shortcut.matchCode(sortedTarget[i], sortedSelf[i])) {
          return false;
        }
      }

      return this.matched(true, context);
    });
  }

  static matchCode = (code1: KeyCode, code2: KeyCode) => {
    return code1.toLocaleLowerCase() === code2.toLocaleLowerCase();
  };

  static sortCodes = (codes: KeyCode[]) => {
    return codes.sort((code1, code2) =>
      code1.toLocaleLowerCase() > code2.toLocaleLowerCase() ? 1 : -1
    );
    // return codes.map((code) => code.toLocaleLowerCase()).sort();
  };
}
