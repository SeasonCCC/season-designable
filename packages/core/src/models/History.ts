import { BehaviorSubject } from 'rxjs';

export type HistoryProps<T> = {
  onPush?: (item: T) => void;
  onRedo?: (item: T) => void;
  onUndo?: (item: T) => void;
  onGoto?: (item: T) => void;
};

export type HistoryItem<T> = {
  data: T;
  type?: string;
  timestamp: number;
};

export type Serializable = {
  from(json: any): void; //导入数据
  serialize(): any; //序列化模型，用于历史记录保存
};

export class History<T extends Serializable = any> {
  context: Serializable;
  props?: HistoryProps<HistoryItem<T>>;
  current = new BehaviorSubject(0);
  history = new BehaviorSubject<HistoryItem<T>[]>([]);
  updateTimer = null;
  maxSize = 100;
  locking = false;

  constructor(context: T, props?: HistoryProps<HistoryItem<T>>) {
    this.context = context;
    this.props = props;
    this.push();
  }

  list() {
    return this.history;
  }

  push(type?: string) {
    if (this.locking) return;
    if (this.current.value < this.history.value.length - 1) {
      this.history.next(this.history.value.slice(0, this.current.value + 1));
    }

    const item = {
      data: this.context.serialize(),
      timestamp: Date.now(),
      type,
    };

    this.current.next(this.history.value.length);
    this.history.next([...this.history.value, item]);
    const overSizeCount = this.history.value.length - this.maxSize;
    if (overSizeCount > 0) {
      this.history.next(this.history.value.slice(0, overSizeCount));
      // this.history.splice(0, overSizeCount);
      this.current.next(overSizeCount - 1);
    }

    if (this.props?.onPush) {
      this.props.onPush(item);
    }
  }

  get allowUndo() {
    return this.history.value.length > 0 && this.current.value - 1 >= 0;
  }

  get allowRedo() {
    return this.history.value.length > this.current.value + 1;
  }

  redo() {
    if (this.allowRedo) {
      const item = this.history.value[this.current.value + 1];
      this.locking = true;
      this.context.from(item.data);
      this.locking = false;
      this.current.next(this.current.value + 1);

      if (this.props?.onRedo) {
        this.props.onRedo(item);
      }
    }
  }

  undo() {
    if (this.allowUndo) {
      const item = this.history.value[this.current.value - 1];
      this.locking = true;
      this.context.from(item.data);
      this.locking = false;
      this.current.next(this.current.value - 1);

      if (this.props?.onUndo) {
        this.props.onUndo(item);
      }
    }
  }

  goTo(index: number) {
    const item = this.history.value[index];
    if (item) {
      this.locking = true;
      this.context.from(item.data);
      this.locking = false;
      this.current.next(index);

      if (this.props?.onGoto) {
        this.props.onGoto(item);
      }
    }
  }

  clear() {
    this.history.next([]);
    this.current.next(0);
  }
}
