import { Operation } from './Operation';
import { TreeNode } from './TreeNode';

export interface TransformHelperProps {
  operation: Operation;
}

export type TransformHelperType =
  | 'translate'
  | 'resize'
  | 'rotate'
  | 'scale'
  | 'round';

export type ResizeDirection =
  | 'left-top'
  | 'left-center'
  | 'left-bottom'
  | 'center-top'
  | 'center-bottom'
  | 'right-top'
  | 'right-bottom'
  | 'right-center'
  | (string & {});

export interface TransformHelperDragStartProps {
  type: TransformHelperType;
  direction?: ResizeDirection;
  dragNodes: TreeNode[];
}

export class TransformHelper {
  constructor(props: TransformHelperProps) {}
}
