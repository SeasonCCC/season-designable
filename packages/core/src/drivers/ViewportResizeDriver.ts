import { EventDriver, globalThisPolyfill } from '@season-designable/shared';
import { Engine } from '..';
import { ViewportResizeEvent } from '../events/viewport/ViewportResizeEvent';

export class ViewportResizeDriver extends EventDriver<Engine> {
  request: number = 0;

  resizeObserver: ResizeObserver | null = null;

  onResize = (e: any) => {
    if (e.preventDefault) e.preventDefault();
    // this.request = requestAnimationFrame(() => {
    // cancelAnimationFrame(this.request);
    this.dispatch(
      new ViewportResizeEvent({
        scrollX: this.contentWindow.scrollX,
        scrollY: this.contentWindow.scrollY,
        width: this.contentWindow.innerWidth,
        height: this.contentWindow.innerHeight,
        innerHeight: this.contentWindow.innerHeight,
        innerWidth: this.contentWindow.innerWidth,
        view: this.contentWindow,
        target: e.target || this.container,
      })
    );
    // });
  };

  attach() {
    if (this.contentWindow && this.contentWindow !== globalThisPolyfill) {
      this.addEventListener('resize', this.onResize);
    } else {
      if (
        this.container !== document &&
        this.container instanceof HTMLElement
      ) {
        this.resizeObserver = new ResizeObserver(this.onResize);
        this.resizeObserver.observe(this.container);
      }
    }
  }

  detach() {
    if (this.contentWindow && this.contentWindow !== globalThisPolyfill) {
      this.removeEventListener('resize', this.onResize);
    } else if (this.resizeObserver) {
      if (
        this.container !== document &&
        this.container instanceof HTMLElement
      ) {
        this.resizeObserver.unobserve(this.container);
        this.resizeObserver.disconnect();
      }
    }
  }
}
