import { EventDriver } from '@season-designable/shared';
import { Engine, MouseMoveEvent } from '..';

export class MouseMoveDriver extends EventDriver<Engine> {
  request: number = 0;
  onMouseMove = (e: MouseEvent) => {
    // e.stopPropagation();
    this.request = requestAnimationFrame(() => {
      cancelAnimationFrame(this.request);
      this.dispatch(
        new MouseMoveEvent({
          clientX: e.clientX,
          clientY: e.clientY,
          pageX: e.pageX,
          pageY: e.pageY,
          target: e.target!,
          view: e.view!,
        })
      );
    });
  };

  attach() {
    this.addEventListener('mousemove', this.onMouseMove, {
      mode: 'onlyOne',
    });
  }

  detach() {
    this.removeEventListener('mouseover', this.onMouseMove, {
      mode: 'onlyOne',
    });
  }
}
