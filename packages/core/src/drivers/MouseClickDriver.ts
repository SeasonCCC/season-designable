import { EventDriver } from '@season-designable/shared';
import { Engine, MouseClickEvent, MouseDoubleClickEvent } from '..';

export class MouseClickDriver extends EventDriver<Engine> {
  onMouseClick = (e: MouseEvent) => {
    e.stopPropagation();
    const target = e.target;

    if (target instanceof HTMLElement) {
      if (
        target.closest(`*[${this.engine.props.clickStopPropagationAttrName}]`)
      ) {
        return;
      }

      this.dispatch(
        new MouseClickEvent({
          clientX: e.clientX,
          clientY: e.clientY,
          pageX: e.pageX,
          pageY: e.pageY,
          target: e.target!,
          view: e.view!,
        })
      );
    }
  };

  onMouseDoubleClick = (e: MouseEvent) => {
    const target = e.target;

    if (target instanceof HTMLElement) {
      if (
        target.closest(`*[${this.engine.props.clickStopPropagationAttrName}]`)
      ) {
        return;
      }

      this.dispatch(
        new MouseDoubleClickEvent({
          clientX: e.clientX,
          clientY: e.clientY,
          pageX: e.pageX,
          pageY: e.pageY,
          target: e.target!,
          view: e.view!,
        })
      );
    }
  };

  attach() {
    this.addEventListener('click', this.onMouseClick, {
      mode: 'onlyChild',
    });
    this.addEventListener('dblclick', this.onMouseDoubleClick, {
      mode: 'onlyChild',
    });
  }

  detach() {
    this.removeEventListener('click', this.onMouseClick, {
      mode: 'onlyChild',
    });
    this.removeEventListener('dblclick', this.onMouseDoubleClick, {
      mode: 'onlyChild',
    });
  }
}
