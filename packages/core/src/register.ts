import { each, get, merge } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { isBehaviorHost, isBehaviorList } from './externals';
import { getBrowserLanguage, lowerSnake, mergeLocales } from './internals';
import { TreeNode } from './models';
import {
  Behavior,
  BehaviorLike,
  DesignerBehaviorStore,
  DesignerBehaviors,
  DesignerIcons,
  DesignerIconsStore,
  DesignerLanguageStore,
  DesignerLocaleStore,
  DesignerLocales,
} from './types';

const designerBehaviorsStore = new BehaviorSubject<DesignerBehaviorStore>([]);

const designerIconsStore = new BehaviorSubject<DesignerIconsStore>({});

const designerLocalesStore = new BehaviorSubject<DesignerLocaleStore>({});

const designerLanguageStore = new BehaviorSubject<DesignerLanguageStore>(
  getBrowserLanguage()
);

const reSortBehaviors = (target: Behavior[], sources: DesignerBehaviors) => {
  const findTargetBehavior = (behavior: Behavior) => target.includes(behavior);

  const findSourceBehavior = (name: string) => {
    let source: Behavior | undefined;

    each(sources, (value) => {
      const { Behavior } = value;
      const target = Behavior?.find((item) => item.name === name);
      if (target) source = target;
    });

    return source;
  };

  let result: Behavior[] = target;
  each(sources, (item) => {
    if (!item) return;
    if (!isBehaviorHost(item)) return;
    const { Behavior } = item;

    if (Behavior) {
      Behavior.forEach((behavior) => {
        if (findTargetBehavior(behavior)) return;
        result.push(behavior);

        behavior.extends?.forEach((extend) => {
          const behaviorExt = findSourceBehavior(extend);
          if (!behaviorExt) {
            throw new Error(
              `No ${extend} behavior that ${behavior.name} depends on`
            );
          }

          if (!findTargetBehavior(behaviorExt)) {
            result.unshift(behaviorExt);
          }
        });
      });
    }
  });

  return result;
};

export const getISOCode = (language: string) => {
  let lang = lowerSnake(language);

  if (designerLocalesStore.value[lang]) {
    return lang;
  }

  let isoCode = designerLanguageStore.value;

  each(designerLocalesStore.value, (_, key: string) => {
    if (key.includes(lang) || key.includes(isoCode)) {
      isoCode = key;
      return;
    }
  });

  return isoCode;
};

const desginerGlobalRegistry = {
  setDesignerLanguage(lang: string) {
    designerLanguageStore.next(lang);
  },

  setDesignerBehaviors(behaviors: BehaviorLike[]) {
    designerBehaviorsStore.next(
      behaviors.reduce<Behavior[]>((buf, behavior) => {
        if (isBehaviorHost(behavior)) {
          return [...buf, ...(behavior.Behavior || [])];
        } else if (isBehaviorList(behavior)) {
          return [...buf, ...behavior];
        }
        return buf;
      }, [])
    );
  },

  setDesignerIcons(icons: DesignerIcons) {
    designerIconsStore.next(merge(designerIconsStore.value, icons));
  },

  setDesignerLocales(...localePkg: DesignerLocales[]) {
    designerLocalesStore.next(
      localePkg.reduce((sub, locales) => {
        return mergeLocales(sub, locales);
      }, designerLocalesStore.value)
    );
  },

  getDesignerLanguage: () => getISOCode(designerLanguageStore.value),

  getDesignerLanguageStore: () => designerLanguageStore,

  getDesignerMessage: (token: string, locales?: DesignerLocales) => {
    const lang = getISOCode(designerLanguageStore.value);
    const locale = locales ? locales[lang] : designerLocalesStore.value[lang];

    if (!locale) {
      each(designerLocalesStore.value, (item) => {
        const message = get(item, lowerSnake(token));

        if (message) return message;
      });

      return;
    }

    // if (locales) {
    // console.log(locales, token, get(locale, lowerSnake(token), ''));
    // }
    // console.log(get(locale, lowerSnake(token)));

    // return '';

    return get(locale, lowerSnake(token));
  },

  getDesignerBehaviors: (node: TreeNode) => {
    return designerBehaviorsStore.value.filter((pattern) =>
      pattern.selector(node)
    );
  },

  getDesignerIcon: (name: string) => {
    return designerIconsStore.value[name];
  },

  registerDesignerBehaviors(...behaviors: DesignerBehaviors[]) {
    let result: Behavior[] = [];

    behaviors.forEach((behavior) => {
      result = reSortBehaviors(result, behavior);
    });

    if (result.length) {
      designerBehaviorsStore.next(result);
    }
  },
};

export type DesignerRegistry = typeof desginerGlobalRegistry;

export const GlobalRegistry: DesignerRegistry = desginerGlobalRegistry;
