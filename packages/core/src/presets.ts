import {
  DragDropDriver,
  KeyboardDriver,
  MouseClickDriver,
  MouseMoveDriver,
  ViewportResizeDriver,
} from './drivers';
import {
  useDragDropEffect,
  useCursorEffect,
  useKeyboardEffect,
  useSelectionEffect,
  useViewportEffect,
  useWorkspaceEffect,
} from './effects';
import {
  CopyNodes,
  DeleteNodes,
  PasteNodes,
  SelectNodes,
  SelectOrderNodes,
  SelectAllNodes,
  SelectNextNode,
  SelectPrevNode,
} from './shortcuts';

export const DEFAULT_EFFECTS = [
  useDragDropEffect,
  useCursorEffect,
  useSelectionEffect,
  useKeyboardEffect,
  useViewportEffect,
  useWorkspaceEffect,
];

export const DEFAULT_DRIVERS = [
  DragDropDriver,
  MouseMoveDriver,
  MouseClickDriver,
  KeyboardDriver,
  ViewportResizeDriver,
];

export const DEFAULT_SHORTCUTS = [
  DeleteNodes,
  CopyNodes,
  PasteNodes,
  SelectNodes,
  SelectOrderNodes,
  SelectAllNodes,
  SelectNextNode,
  SelectPrevNode,
];
