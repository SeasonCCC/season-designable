import { AppendNodeEvent, Engine } from '..';

export const useWorkspaceEffect = (engine: Engine) => {
  engine.subscribeTo(AppendNodeEvent, (event) => {
    if (event.context?.workbench) {
      engine.workbench.setActiveWorkspace(event.context.workspace);
    }
  });
};
