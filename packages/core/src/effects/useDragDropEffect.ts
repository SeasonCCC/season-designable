import { Point } from '@season-designable/shared';
import {
  ClosestPosition,
  CursorDragType,
  CursorType,
  Engine,
  TreeNode,
} from '../models';
import { DragMoveEvent, DragStartEvent, DragStopEvent } from '../events';

export const useDragDropEffect = (engine: Engine) => {
  engine.subscribeTo(DragStartEvent, (event) => {
    if (engine.cursor.type.value !== CursorType.Normal) return;
    const target = event.data.target;

    if (
      target instanceof HTMLElement &&
      event.data.topClientX &&
      event.data.topClientY
    ) {
      const el = target?.closest(`
         *[${engine.props.nodeIdAttrName}],
         *[${engine.props.sourceIdAttrName}],
         *[${engine.props.outlineNodeIdAttrName}]
        `);

      const handler = target?.closest(
        `*[${engine.props.nodeDragHandlerAttrName}]`
      );

      const helper = handler?.closest(
        `*[${engine.props.nodeSelectionIdAttrName}]`
      );

      if (!el?.getAttribute && !handler) return;

      const sourceId = el?.getAttribute(engine.props.sourceIdAttrName);
      const outlineId = el?.getAttribute(engine.props.outlineNodeIdAttrName);
      const handlerId = helper?.getAttribute(
        engine.props.nodeSelectionIdAttrName
      );
      const nodeId = el?.getAttribute(engine.props.nodeIdAttrName);

      engine.workbench.workspaces.forEach((currentWorkspace) => {
        const operation = currentWorkspace.operation;
        const moveHelper = operation.moveHelper;
        if (nodeId || outlineId || handlerId) {
          const node = engine.findNodeById(
            outlineId || nodeId || handlerId || ''
          );

          if (node) {
            if (!node.allowDrag()) return;
            if (node.isRoot) return;

            const validSelected = engine
              .getAllSelectedNodes()
              .filter((node) => node.allowDrag());

            if (validSelected.some((selectNode) => selectNode === node)) {
              moveHelper.dragStart({ dragNodes: TreeNode.sort(validSelected) });
            } else {
              moveHelper.dragStart({ dragNodes: [node] });
            }
          }
        } else if (sourceId) {
          const sourceNode = engine.findNodeById(sourceId);
          if (sourceNode) {
            moveHelper.dragStart({ dragNodes: [sourceNode] });
          }
        }
      });

      engine.cursor.setStyle('move');
    }
  });

  engine.subscribeTo(DragMoveEvent, (event) => {
    if (engine.cursor.type.value !== CursorType.Normal) return;
    if (engine.cursor.dragType.value !== CursorDragType.Move) return;
    const target = event.data.target;

    if (
      target instanceof HTMLElement &&
      event.data.topClientX &&
      event.data.topClientY
    ) {
      const el = target?.closest(`
            *[${engine.props.nodeIdAttrName}],
            *[${engine.props.outlineNodeIdAttrName}]
          `);

      const point = new Point(event.data.topClientX, event.data.topClientY);
      const nodeId = el?.getAttribute(engine.props.nodeIdAttrName);
      const outlineId = el?.getAttribute(engine.props.outlineNodeIdAttrName);

      engine.workbench.workspaces.forEach((currentWorkspace) => {
        const operation = currentWorkspace.operation;
        const moveHelper = operation.moveHelper;
        const dragNodes = moveHelper.dragNodes;

        const tree = operation.tree;
        if (!dragNodes.value.length) return;

        const touchNode = tree.findById(outlineId || nodeId || '');

        moveHelper.dragMove({
          point,
          touchNode,
        });
      });
    }
  });

  engine.subscribeTo(DragStopEvent, (event) => {
    if (engine.cursor.type.value !== CursorType.Normal) return;
    if (engine.cursor.dragType.value !== CursorDragType.Move) return;

    engine.workbench.workspaces.forEach((currentWorkspace) => {
      const operation = currentWorkspace.operation;
      const moveHelper = operation.moveHelper;
      const dragNodes = moveHelper.dragNodes;
      const closestNode = moveHelper.closestNode;
      const closestDirection = moveHelper.closestDirection;
      const selection = operation.selection;

      if (!dragNodes.value.length) return;

      if (
        dragNodes.value.length &&
        closestNode.value &&
        closestDirection.value
      ) {
        if (
          closestDirection.value === ClosestPosition.After ||
          closestDirection.value === ClosestPosition.Under
        ) {
          if (closestNode.value.allowSibling(dragNodes.value)) {
            selection.batchSafeSelect(
              closestNode.value.insertAfter(
                ...TreeNode.filterDroppable(dragNodes.value, closestNode.value)
              ) || []
            );
          }
        } else if (
          closestDirection.value === ClosestPosition.Before ||
          closestDirection.value === ClosestPosition.Upper
        ) {
          if (closestNode.value.allowSibling(dragNodes.value)) {
            selection.batchSafeSelect(
              closestNode.value.insertBefore(
                ...TreeNode.filterDroppable(dragNodes.value, closestNode.value)
              ) || []
            );
          }
        } else if (
          closestDirection.value === ClosestPosition.Inner ||
          closestDirection.value === ClosestPosition.InnerAfter
        ) {
          if (closestNode.value.allowAppend(dragNodes.value)) {
            selection.batchSafeSelect(
              closestNode.value.append(
                ...TreeNode.filterDroppable(dragNodes.value, closestNode.value)
              ) || []
            );

            moveHelper.dragDrop({ dropNode: closestNode.value });
          }
        } else if (closestDirection.value === ClosestPosition.InnerBefore) {
        }
      }

      moveHelper.dragEnd();
    });

    engine.cursor.setStyle('');
  });
};
