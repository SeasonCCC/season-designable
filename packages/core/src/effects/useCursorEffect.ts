import { requestIdle } from '@season-designable/shared';
import { CursorStatus, Engine } from '..';
import {
  DragMoveEvent,
  DragStartEvent,
  DragStopEvent,
  MouseMoveEvent,
} from '../events/cursor';

export const useCursorEffect = (engine: Engine) => {
  engine.subscribeTo(DragStartEvent, (event) => {
    engine.cursor.setStatus(CursorStatus.DragStart);
    engine.cursor.setDragStartPosition(event.data);
  });

  engine.subscribeTo(DragMoveEvent, (event) => {
    engine.cursor.setStatus(CursorStatus.Dragging);
    engine.cursor.setPosition(event.data);
  });

  engine.subscribeTo(DragStopEvent, (event) => {
    engine.cursor.setStatus(CursorStatus.DragStop);
    engine.cursor.setDragEndPosition(event.data);
    engine.cursor.setDragStartPosition();
    requestIdle(() => {
      engine.cursor.setStatus(CursorStatus.Normal);
    });
  });

  engine.subscribeTo(MouseMoveEvent, (event) => {
    engine.cursor.setStatus(
      engine.cursor.status.value === CursorStatus.Dragging ||
        engine.cursor.status.value === CursorStatus.DragStart
        ? engine.cursor.status.value
        : CursorStatus.Normal
    );

    if (engine.cursor.status.value === CursorStatus.Dragging) return;
    engine.cursor.setPosition(event.data);
  });

  engine.subscribeTo(MouseMoveEvent, (event) => {
    const currentWorkspace = event?.context?.workspace;
    if (!currentWorkspace) return;
    const operation = currentWorkspace.operation;

    if (engine.cursor.status.value !== CursorStatus.Normal) {
      operation.hover.clear();
      return;
    }

    const target = event.data.target;
    if (target instanceof HTMLElement) {
      const el = target?.closest?.(`
        *[${engine.props.nodeIdAttrName}],
        *[${engine.props.outlineNodeIdAttrName}]
      `);

      if (!el || !el.getAttribute) {
        return;
      }

      const nodeId = el.getAttribute(engine.props.nodeIdAttrName);
      const outlineNodeId = el.getAttribute(engine.props.outlineNodeIdAttrName);
      const node = operation.tree.findById(nodeId || outlineNodeId || '');

      if (node) {
        operation.hover.setHover(node);
      } else {
        operation.hover.clear();
      }
    }
  });
};
