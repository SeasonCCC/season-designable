import { KeyCode } from '@season-designable/shared';
import { CursorStatus, Engine } from '..';
import { MouseClickEvent } from '../events/cursor';

export const useSelectionEffect = (engine: Engine) => {
  engine.subscribeTo(MouseClickEvent, (event) => {
    if (engine.cursor.status.value !== CursorStatus.Normal) return;
    const target = event.data.target;

    if (target instanceof HTMLElement) {
      const el = target.closest(
        `*[${engine.props.nodeIdAttrName}],*[${engine.props.outlineNodeIdAttrName}]`
      );

      const isHelpers = target.closest(
        `*[${engine.props.nodeSelectionIdAttrName}]`
      );

      const currentWorkspace =
        event.context?.workspace ?? engine.workbench.activeWorkspace;

      if (!currentWorkspace) return;

      if (!el) return;

      if (!el?.getAttribute) {
      }

      const nodeId = el.getAttribute(engine.props.nodeIdAttrName);
      const structNodeId = el.getAttribute(engine.props.outlineNodeIdAttrName);
      const operation = currentWorkspace.operation;
      const selection = operation.selection;
      const tree = operation.tree;
      const node = tree.findById(nodeId || structNodeId || '');

      if (node) {
        if (
          engine.keyboard.isKeyDown(KeyCode.Meta) ||
          engine.keyboard.isKeyDown(KeyCode.Control)
        ) {
          if (selection.has(node)) {
            selection.remove(node);
          } else {
            selection.add(node);
          }
        } else if (engine.keyboard.isKeyDown(KeyCode.Shift)) {
          if (selection.has(node)) {
            selection.remove(node);
          } else {
            selection.crossAddTo(node);
          }
        } else {
          selection.select(node);
        }
      } else {
        selection.select(tree);
      }
    }
  });
};
