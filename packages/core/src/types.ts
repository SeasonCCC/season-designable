import {
  Engine,
  ITreeNode,
  TreeNode,
  Viewport,
  Workbench,
  Workspace,
} from './models';

export interface DesignerMiniLocales {
  [ISOCode: string]: string;
}

export type WorkbenchTypes =
  | 'DESIGNABLE'
  | 'PREVIEW'
  | 'JSONTREE'
  | 'MARKUP'
  | (string & {});

export interface BehaviorCreator {
  name: string;
  extends?: string[];
  selector: string | ((node: TreeNode) => boolean);
  designerProps?: DesignerControllerProps;
  designerLocales?: DesignerLocales;
}

export interface Behavior extends BehaviorCreator {
  selector: (node: TreeNode) => boolean;
}

export interface BehaviorHost {
  Behavior?: Behavior[];
}

export interface DesignerBehaviors {
  [key: string]: BehaviorHost;
}

export type BehaviorLike = Behavior[] | BehaviorHost;

export interface ResourceCreator {
  title?: string | DesignerMiniLocales;
  description?: string | DesignerMiniLocales;
  icon?: any;
  thumb?: string;
  span?: number;
  elements?: ITreeNode[];
}

export type DesignerIcons = Record<string, any>;

export type DesignerIconsStore = DesignerIcons;

export type DesignerLocaleStore = DesignerLocales;

export type DesignerBehaviorStore = Behavior[];

export type DesignerLanguageStore = string;

export interface Resource extends Omit<ResourceCreator, 'elements'> {
  node?: TreeNode;
}

export interface ResourceHost {
  Resource?: Resource[];
}

export type ResourceLike = Resource[] | ResourceHost;

export type Resizable = {
  width?: (
    node: TreeNode,
    element: Element
  ) => {
    plus: () => void;
    minus: () => void;
  };
  height?: (
    node: TreeNode,
    element: Element
  ) => {
    plus: () => void;
    minus: () => void;
  };
};

export type Translate = {
  x: (
    node: TreeNode,
    element: HTMLElement,
    diffX: string | number
  ) => {
    translate: () => void;
  };
  y: (
    node: TreeNode,
    element: HTMLElement,
    diffY: string | number
  ) => {
    translate: () => void;
  };
};

export interface DesignerLocales {
  [ISOCode: string]: {
    [key: string]: any;
  };
}

export type EngineContext = {
  workspace: Workspace;
  workbench: Workbench;
  engine: Engine;
  viewport: Viewport;
};

export interface DesignerProps {
  package?: string; //npm包名
  registry?: string; //web npm注册平台地址
  version?: string; //semver版本号
  path?: string; //组件模块路径
  title?: string; //标题
  description?: string; //描述
  icon?: string; //icon
  droppable?: boolean; //是否可作为拖拽容器，默认为true
  draggable?: boolean; //是否可拖拽，默认为true
  deletable?: boolean; //是否可删除，默认为true
  cloneable?: boolean; //是否可拷贝，默认为true
  resizable?: Resizable;
  translatable?: Translate; // 自由布局
  inlineChildrenLayout?: boolean; //子节点内联，用于指定复杂布局容器，强制内联
  selfRenderChildren?: boolean; //是否自己渲染子节点
  propsSchema?: Record<string, any>; //Formily JSON Schema
  defaultProps?: any; //默认属性
  getDragNodes?: (node: TreeNode) => TreeNode | TreeNode[]; //拦截转换Drag节点
  getDropNodes?: (node: TreeNode, parent: TreeNode) => TreeNode | TreeNode[]; //拦截转换Drop节点
  getComponentProps?: (node: TreeNode) => any; //拦截属性
  allowAppend?: (target: TreeNode, sources?: TreeNode[]) => boolean;
  allowSiblings?: (target: TreeNode, sources?: TreeNode[]) => boolean;
  allowDrop?: (target: TreeNode) => boolean;
  [key: string]: any;
}

export type DesignerControllerProps =
  | DesignerProps
  | ((node: TreeNode) => DesignerProps);
