import { KeyCode } from '@season-designable/shared';
import { TreeNode, Shortcut } from '../models';

export const DeleteNodes = new Shortcut({
  codes: [[KeyCode.Backspace], [KeyCode.Delete]],
  handler(context) {
    const operation = context?.workspace.operation;
    if (operation) {
      TreeNode.remove(operation.selection.selectedNodes);
    }
  },
});

let ClipboardNodes: TreeNode[] = [];

export const CopyNodes = new Shortcut({
  codes: [
    [KeyCode.Meta, KeyCode.C],
    [KeyCode.Control, KeyCode.C],
  ],
  handler(context) {
    const operation = context?.workspace.operation;
    if (operation) {
      ClipboardNodes = operation.selection.selectedNodes;
    }
  },
});

export const PasteNodes = new Shortcut({
  codes: [
    [KeyCode.Meta, KeyCode.V],
    [KeyCode.Control, KeyCode.V],
  ],
  handler(context) {
    const operation = context?.workspace.operation;
    if (operation) {
      TreeNode.clone(ClipboardNodes);
    }
  },
});
