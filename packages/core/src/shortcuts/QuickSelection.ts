import { KeyCode } from '@season-designable/shared';
import { TreeNode, Shortcut } from '../models';

const findBottomLastChild = (node: TreeNode): TreeNode => {
  if (node.children.value.length === 0) {
    return node;
  }

  return findBottomLastChild(node.lastChild);
};

const findTopParentNext = (node: TreeNode): TreeNode => {
  if (!node.parent) {
    return node;
  }

  if (node.parent.next) {
    return node.parent.next;
  }

  return findTopParentNext(node.parent);
};

export const SelectPrevNode = new Shortcut({
  codes: [
    [KeyCode.PageUp],
    [KeyCode.ArrowUp],
    [KeyCode.Up],
    [KeyCode.Left],
    [KeyCode.ArrowLeft],
  ],
  handler(context) {
    const operation = context.workspace.operation;
    const tree = operation.tree;
    const selection = operation.selection;

    if (!selection.first) {
      return;
    }

    const selectedTopNode = tree.findById(selection.first);
    if (!selectedTopNode) {
      return;
    }

    if (selectedTopNode.previous) {
      selection.select(findBottomLastChild(selectedTopNode.previous));
    } else if (selectedTopNode.parent) {
      selection.select(selectedTopNode.parent);
    } else {
      selection.select(findBottomLastChild(selectedTopNode));
    }
  },
});

export const SelectNextNode = new Shortcut({
  codes: [
    [KeyCode.PageDown],
    [KeyCode.ArrowDown],
    [KeyCode.Down],
    [KeyCode.Right],
    [KeyCode.ArrowRight],
  ],
  handler(context) {
    const operation = context.workspace.operation;
    const tree = operation.tree;
    const selection = operation.selection;

    if (!selection.last) {
      return;
    }

    const selectedBottomNode = tree.findById(selection.last);
    if (!selectedBottomNode) {
      return;
    }

    const nextNode = selectedBottomNode.firstChild || selectedBottomNode.next;

    if (nextNode) {
      selection.select(nextNode);
    } else {
      selection.select(findTopParentNext(selectedBottomNode));
    }
  },
});
