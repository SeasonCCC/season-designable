import { EngineContext } from '../../types';

export abstract class AbstractHistoryEvent {
  data: any;
  context?: EngineContext;
  constructor(data: any) {
    this.data = data;
  }
}
