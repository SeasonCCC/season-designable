import { AbstractHistoryEvent } from './AbstractHistoryEvent';
import { CustomEvent } from '@season-designable/shared';

export class HistoryGotoEvent
  extends AbstractHistoryEvent
  implements CustomEvent {}
