import { AbstractHistoryEvent } from './AbstractHistoryEvent';
import { CustomEvent } from '@season-designable/shared';

export class HistoryUndoEvent
  extends AbstractHistoryEvent
  implements CustomEvent {}
