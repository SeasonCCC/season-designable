import { CustomEvent } from '@season-designable/shared';
import { AbstractCursorEvent } from './AbstractCursorEvent';

export class DragStartEvent extends AbstractCursorEvent implements CustomEvent {
  // type = DragStartEvent.name;
}
