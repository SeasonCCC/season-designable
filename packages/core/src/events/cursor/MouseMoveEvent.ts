import { CustomEvent } from '@season-designable/shared';
import { AbstractCursorEvent } from './AbstractCursorEvent';

export class MouseMoveEvent
  extends AbstractCursorEvent
  implements CustomEvent {}
