import { globalThisPolyfill } from '@season-designable/shared';
import { EngineContext } from '../../types';

export interface CursorEventOriginData {
  clientX: number;
  clientY: number;
  pageX: number;
  pageY: number;
  target: EventTarget;
  view: Window;
}

export interface CursorEventData extends CursorEventOriginData {
  topClientX?: number;
  topClientY?: number;
  topPageX?: number;
  topPageY?: number;
}

export abstract class AbstractCursorEvent {
  data: CursorEventData;
  context?: EngineContext;

  constructor(data: CursorEventOriginData) {
    this.data = data || {
      clientX: 0,
      clientY: 0,
      pageX: 0,
      pageY: 0,
      target: null,
      view: globalThisPolyfill,
    };
    this.transformCoordinates();
  }

  transformCoordinates() {
    const { frameElement } = this.data?.view || {};
    if (frameElement && this.data.view !== globalThisPolyfill) {
    } else {
      this.data.topClientX = this.data.clientX;
      this.data.topClientY = this.data.clientY;
      this.data.topPageX = this.data.pageX;
      this.data.topPageY = this.data.pageY;
    }
  }
}
