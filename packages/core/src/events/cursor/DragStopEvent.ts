import { CustomEvent } from '@season-designable/shared';
import { AbstractCursorEvent } from './AbstractCursorEvent';

export class DragStopEvent extends AbstractCursorEvent implements CustomEvent {
  // static type = Symbol('DragStopEvent');

  // constructor(data: CursorEventOriginData) {
  //   super(data);
  // }
}
