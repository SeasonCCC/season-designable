import { CustomEvent } from '@season-designable/shared';
import { AbstractCursorEvent } from './AbstractCursorEvent';

export class DragMoveEvent extends AbstractCursorEvent implements CustomEvent {
  // static type = Symbol('DragMoveEvent');
  // constructor(data: CursorEventOriginData) {
  //   super(data);
  // }
}
