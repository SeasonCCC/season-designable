import { CustomEvent } from '@season-designable/shared';
import { AbstractCursorEvent } from './AbstractCursorEvent';

export class MouseClickEvent
  extends AbstractCursorEvent
  implements CustomEvent {}

export class MouseDoubleClickEvent
  extends AbstractCursorEvent
  implements CustomEvent {}
