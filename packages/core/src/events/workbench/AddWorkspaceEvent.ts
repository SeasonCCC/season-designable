import { CustomEvent } from '@season-designable/shared';
import { AbstractWorkbenchEvent } from './AbstractWorkbenchEvent';

export const ADD_WORKSPACE_EVENT = Symbol('AddWorkspaceEvent');

export class AddWorkspaceEvent
  extends AbstractWorkbenchEvent
  implements CustomEvent
{
  type = ADD_WORKSPACE_EVENT;
}
