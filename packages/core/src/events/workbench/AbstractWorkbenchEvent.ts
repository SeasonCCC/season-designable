import { Workspace } from '../../models';
import { EngineContext } from '../../types';

export abstract class AbstractWorkbenchEvent {
  data: Workspace;
  context!: EngineContext;
  constructor(data: Workspace) {
    this.data = data;
  }
}
