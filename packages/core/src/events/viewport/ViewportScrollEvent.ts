import { CustomEvent } from '@season-designable/shared';
import { AbstractViewportEvent } from './AbstractViewportEvent';

export class ViewportScrollEvent
  extends AbstractViewportEvent
  implements CustomEvent {}
