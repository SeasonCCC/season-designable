import { globalThisPolyfill } from '@season-designable/shared';
import { EngineContext } from '../../types';

export interface ViewportEventData {
  scrollX: number;
  scrollY: number;
  width: number;
  height: number;
  view: Window;
  innerWidth: number;
  innerHeight: number;
  target: EventTarget;
}

export abstract class AbstractViewportEvent {
  data: ViewportEventData;
  context!: EngineContext;

  constructor(data: ViewportEventData) {
    this.data = data || {
      scrollX: globalThisPolyfill.scrollX,
      scrollY: globalThisPolyfill.scrollY,
      width: globalThisPolyfill.innerWidth,
      height: globalThisPolyfill.innerHeight,
      innerWidth: globalThisPolyfill.innerWidth,
      innerHeight: globalThisPolyfill.innerHeight,
      view: globalThisPolyfill,
      target: globalThisPolyfill,
    };
  }
}
