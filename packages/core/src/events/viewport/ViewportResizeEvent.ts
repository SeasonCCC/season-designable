import { CustomEvent } from '@season-designable/shared';
import { AbstractViewportEvent } from './AbstractViewportEvent';

export class ViewportResizeEvent
  extends AbstractViewportEvent
  implements CustomEvent {}
