import { CustomEvent } from '@season-designable/shared';
import { AbstractKeyboardEvent } from './AbstractKeyboardEvent';

export class KeyDownEvent
  extends AbstractKeyboardEvent
  implements CustomEvent {}
