import { CustomEvent } from '@season-designable/shared';
import { AbstractKeyboardEvent } from './AbstractKeyboardEvent';

export class KeyUpEvent extends AbstractKeyboardEvent implements CustomEvent {}
