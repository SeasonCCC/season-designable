import { KeyCode, getKeyCodeFromEvent } from '@season-designable/shared';
import { EngineContext } from '../../types';

export abstract class AbstractKeyboardEvent {
  data: KeyCode | null;
  context?: EngineContext;
  originEvent: KeyboardEvent;

  constructor(e: KeyboardEvent) {
    this.data = getKeyCodeFromEvent(e) || null;
    this.originEvent = e;
  }

  get eventType() {
    return this.originEvent.type;
  }

  get ctrlKey() {
    return this.originEvent.ctrlKey;
  }

  get shiftKey() {
    return this.originEvent.shiftKey;
  }

  get metaKey() {
    return this.originEvent.metaKey;
  }

  get altkey() {
    return this.originEvent.altKey;
  }

  preventDefault() {
    if (this.originEvent.preventDefault) {
      this.originEvent.preventDefault();
    }
  }

  stopPropagation() {
    if (this.originEvent.stopPropagation) {
      this.originEvent.stopPropagation();
    }
  }
}
