import { CustomEvent } from '@season-designable/shared';
import { AbstractMutationNodeEvent } from './AbstractMutationNodeEvent';

export class SelectNodeEvent
  extends AbstractMutationNodeEvent
  implements CustomEvent {}
