import { CustomEvent } from '@season-designable/shared';
import { AbstractMutationNodeEvent } from './AbstractMutationNodeEvent';


export class DragNodeEvent
  extends AbstractMutationNodeEvent
  implements CustomEvent
{
}
