import { ITreeNode, TreeNode } from '@season-designable/core';
import { CustomEvent } from '@season-designable/shared';
import { EngineContext } from '../../types';

export interface FromNodeEventData {
  //事件发生的数据源
  source: ITreeNode;
  //事件发生的目标对象
  target: TreeNode;
}

export class FromNodeEvent implements CustomEvent {
  data: FromNodeEventData;
  context!: EngineContext;

  constructor(data: FromNodeEventData) {
    this.data = data;
  }
}
