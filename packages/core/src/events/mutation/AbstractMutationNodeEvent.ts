import { TreeNode } from '../../models';
import { EngineContext } from '../../types';

export interface MutationNodeEventData {
  //事件发生的数据源
  source: TreeNode | TreeNode[];
  //事件发生的目标对象
  target: TreeNode | TreeNode[];
  // 事件发生的来源对象
  originSourceParents?: TreeNode | TreeNode[];
  //扩展数据
  extra?: any;
}

export abstract class AbstractMutationNodeEvent {
  data: MutationNodeEventData;
  context!: EngineContext;

  constructor(data: MutationNodeEventData) {
    this.data = data;
  }
}
