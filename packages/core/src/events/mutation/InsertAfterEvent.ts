import { CustomEvent } from '@season-designable/shared';
import { AbstractMutationNodeEvent } from './AbstractMutationNodeEvent';

export class InsertAfterEvent
  extends AbstractMutationNodeEvent
  implements CustomEvent {}
