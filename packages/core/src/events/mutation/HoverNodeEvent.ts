import { CustomEvent } from '@season-designable/shared';
import { AbstractMutationNodeEvent } from './AbstractMutationNodeEvent';

export class HoverNodeEvent
  extends AbstractMutationNodeEvent
  implements CustomEvent {}
